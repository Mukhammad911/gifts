@extends('client.partials.layouts')
@section('content')

<main id="faq" class="inner">
    <div class="container">
        <div class="row">
            <div class="col-md-8 center-block">
                <div class="info-404 text-center">
                    <h2 class="primary-color inner-bottom-xs">404</h2>
                    <p class="lead">Извините но такой страницы не существует</p>
                    <div class="sub-form-row inner-top-xs inner-bottom-xs">
                        <form role="form" action="{{url('/search')}}" method="get">
                            <input placeholder="Поищите наши продукты" autocomplete="off">
                            <button class="le-button">Поиск</button>
                        </form>
                    </div>
                    <div class="text-center">
                        <a href="{{url('/')}}" class="btn-lg huge"><i class="fa fa-home"></i> Перейти на главную страницу</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection