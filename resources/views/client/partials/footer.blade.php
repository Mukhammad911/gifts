<footer id="footer" class="color-bg">
    <div class="container">
        <div class="row no-margin widgets-row">
            <div class="col-xs-12  col-sm-4 no-margin-left">
                <div class="widget">
                    <h2 style="margin-left: 10px"><a href="{{url('/feature')}}">СКОРО В ПРОДАЖЕ!</a></h2>
                    <div class="body">
                        <ul>
                            <div class="row">
                                @foreach(App\Products::where('feature', '!=', 1)->inRandomOrder()->paginate(4) as $feature)
                                    <li>

                                        <div class="col-xs-4 col-sm-9">
                                            <a href="{{url('product/show/'.$feature->id)}}">{{$feature->name}}</a>.
                                            <div class="price">
                                                <div class="price-prev">

                                                </div>
                                                <div class="price-current">{{$feature->price}} TJS</div>
                                            </div>
                                        </div>

                                        <div class="col-xs-2 col-sm-3 no-margin">
                                            <a href="{{url('product/show/'.$feature->id)}}" class="thumb-holder">
                                                <img alt="{{$feature->name}}" src="{{asset('images/blank.gif')}}"
                                                     data-echo="{{asset('images/products/'.$feature->img)}}"
                                                     title="{{$feature->name}}"/>
                                            </a>
                                        </div>

                                    </li>
                                @endforeach
                            </div>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-4 ">
                <div class="widget">
                    <h2 style="margin-left: 10px"><a href="{{url('/discount')}}">Акции</a></h2>
                    <div class="body">
                        <ul>
                            <div class="row">
                                @foreach(App\Products::whereNotNull('discount')->inRandomOrder()->paginate(4) as $dis)
                                    <li>

                                        <div class="col-xs-4 col-sm-9">
                                            <a href="{{url('product/show/'.$dis->id)}}">{{$dis->name}}</a>
                                            <div class="price">
                                                <div class="price-prev">

                                                </div>
                                                <div class="price-current">{{$dis->price}} TJS</div>
                                            </div>
                                        </div>

                                        <div class="col-xs-2 col-sm-3 no-margin">
                                            <a href="{{url('product/show/'.$dis->id)}}" class="thumb-holder">
                                                <img alt="{{$dis->name}}" src="{{asset('images/blank.gif')}}"
                                                     data-echo="{{asset('images/products/'.$dis->img)}}"
                                                     title="{{$dis->name}}"/>
                                            </a>
                                        </div>

                                    </li>
                                @endforeach
                            </div>

                        </ul>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-4 ">
                <div class="widget">
                    <h2 style="margin-left: 10px"><a href="{{url('/bestsellers')}}">Топ продаж</a></h2>
                    <div class="body">
                        <ul>
                            <div class="row">
                                @foreach(App\Products::where('bestseller', '!=', 1)->inRandomOrder()->paginate(4) as $top)

                                    <li>

                                        <div class="col-xs-4 col-sm-9">
                                            <a href="{{url('product/show/'.$top->id)}}">{{$top->name}}</a>
                                            <div class="price">
                                                <div class="price-prev">

                                                </div>
                                                <div class="price-current">{{$top->price}} TJS</div>
                                            </div>
                                        </div>

                                        <div class="col-xs-2 col-sm-3 no-margin">
                                            <a href="{{url('product/show/'.$top->id)}}" class="thumb-holder">
                                                <img alt="{{$top->name}}" src="{{asset('images/blank.gif')}}"
                                                     data-echo="{{asset('images/products/'.$top->img)}}"
                                                     title="{{$top->name}}"/>
                                            </a>
                                        </div>

                                    </li>

                                @endforeach
                            </div>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="sub-form-row">
        <div class="container">
            <div class="col-xs-12 col-sm-8 col-sm-offset-2 no-padding">
                <form role="form" action="{{url('/add/newsletter/')}}" method="POST">
                    {{ csrf_field() }}
                    <input type="email" placeholder="Подпишись на рассылку" name="email">
                    <button class="le-button">Подписаться</button>

                </form>

            </div>
        </div><!-- /.container -->
    </div>

    <div class="link-list-row">
        <div class="container no-padding">
            <div class="col-xs-12 col-md-4 ">
                <div class="contact-info">
                    <div class="footer-logo">
                    </div>
                    <p>
                        Фотех Ниёзи 51<br/>
                        <br/>

                    </p>

                    <div class="social-icons">
                        <h3>Соц. сети</h3>
                        <ul>
                            <li><a href="#" class="fa fa-facebook"></a></li>
                            <li><a href="#" class="fa fa-twitter"></a></li>
                            <li><a href="#" class="fa fa-dribbble"></a></li>
                            <li><a href="#" class="fa fa-vk"></a></li>
                        </ul>
                    </div><!-- /.social-icons -->

                </div>
            </div>

            <div class="col-xs-12 col-md-8 no-margin">

                <div class="link-widget">
                    <div class="widget">
                        <h3>Информация</h3>
                        <ul>
                            <li><a href="{{url('/about')}}">О нас</a></li>
                            <li><a href="{{url('/contact')}}">Контакты</a></li>
                            <li><a href="{{url('/faqs')}}">ЧаВо</a></li>
                        </ul>
                    </div>
                </div>

                <div class="link-widget">
                    <div class="widget">
                        <h3>Помощь</h3>
                        <ul>
                            <li><a href="{{url('/faqs')}}">Как cделать заказ</a></li>
                            <li><a href="{{url('/delivery')}}">Доставка</a></li>
                            <li><a href="{{url('/pay')}}">Оплата</a></li>

                        </ul>
                    </div>
                </div>

                <div class="link-widget">
                    <div class="widget">
                        <h3>Навигация</h3>
                        <ul>
                            <li><a href="{{url('/feature')}}">Скоро в продаже</a></li>
                            <li><a href="{{url('/discount')}}">Акции</a></li>
                            <li><a href="{{url('/bestsellers')}}">Топ продаж</a></li>
                        </ul>
                    </div>
                </div>

                {{--   <div class="link-widget">
                       <div class="social-icons">
                           <h3>Мы в соц сетях</h3>
                           <ul>
                               <li><a href="#" class="fa fa-facebook"></a></li>
                               <li><a href="#" class="fa fa-twitter"></a></li>
                               <li><a href="#" class="fa fa-vk"></a></li>
                               <li><a href="#" class="fa fa-instagram"></a></li>
                           </ul>
                       </div>
                   </div> --}}
            </div>
        </div>
    </div>

    <div class="copyright-bar">
        <div class="container">
            <div class="col-xs-12 col-sm-6 no-margin">
                <div class="copyright">
                    &copy; <a href="#">GIFTS</a> - Все права защищены
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 no-margin">
                <div class="payment-methods ">
                    <ul>
                        <li><img alt="" src="{{asset('images/payments/pay.png')}}"></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>