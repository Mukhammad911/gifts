@extends('client.partials.layouts')
@section('content')
@if($new)

<div class="animate-dropdown">
    <!-- ========================================= BREADCRUMB ========================================= -->
    <div id="top-mega-nav">
        <div class="container">
            <nav>
                <ul class="inline">
                    <li class="dropdown le-dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-list"></i> Товары по категориям
                        </a>

                        <ul class="dropdown-menu">

                            @foreach(App\Sub_categories::all() as $sub_cat)
                                <li><a href="{{url('category/'.$sub_cat->id)}}">{{$sub_cat->name}}</a></li>
                            @endforeach
                        </ul>
                    </li>

                    <li class="breadcrumb-nav-holder">
                        <ul>
                            <li class=" breadcrumb-item">
                                <a href="{{url('/')}}">Главная</a>

                            </li><!-- /.breadcrumb-item -->

                            <li class=" breadcrumb-item current">
                                <a href="#">
                                    Новости
                                </a>
                            </li><!-- /.breadcrumb-item -->

                        </ul>
                    </li>
                </ul><!-- /.inline -->
            </nav>

        </div><!-- /.container -->
    </div><!-- /#top-mega-nav -->
    <!-- ========================================= BREADCRUMB : END ========================================= -->
</div>

<main id="blog" class="inner-bottom-xs">
    <div class="container">
        <div class="row">
        @if(\Session::has('message'))
        <div class="alert alert-success m-1" role="alert">
            <p> <i class="fa fa-check-circle-o"> </i> {{Session::get('message')}} </p>
        </div>
        @endif
        @if(\Session::has('delete'))
        <div class="alert alert-success m-1" role="alert">
            <p> <i class="fa fa-check-circle-o"> </i> {{Session::get('delete')}} </p>
        </div>
        @endif
   
            <div class="col-md-10">
                <div class="posts sidemeta">
                    <div class="post format-image">
                        <div class="date-wrapper">
                            <div class="date">
                                <span class="month">{{$new->updated_at}}</span>
                                
                            </div>
                        </div><!-- /.date-wrapper -->
                        <div class="post-content">
                            
                            <div class="post-media">
                            
                                <img src="{{asset('images/news/'.$new->img)}}" 
                                    alt=""  style="margin-top:55px; width:848px; height:429px;" />
                            
                            </div>
                            <h2 class="post-title">{{$new->theme}}</h2>
                            <ul class="meta">
                                <li>Опубликовано : {{App\User::where('id', 1)->first()->name}}</li>
                                <li>
                                    <a >{{$news_more->count()}} 
                                        @if($news_more->count() > 1)
                                        Комментарии
                                        @else
                                        Комментарий
                                        @endif
                                    </a>
                                </li>
                            </ul>
                            
                            <p>{{$new->news}}</p>
                        </div><!-- /.post-content -->
                    </div><!-- /.post -->        
                </div><!-- /.posts -->
            </div><!-- /.col -->
            
        </div><!-- /.row -->
    </div><!-- /.container -->

    
</main><!-- /.inner-bottom-xs -->

<section id="blog-single">
    <div class="container">
        <!-- ========================================= CONTENT ========================================= -->
        <div class="posts col-md-9">
            <section id="comments" class="inner-bottom-xs">
                <h3>{{$news_more->count()}} 
                    @if($news_more->count() > 1)
                    Комментарии
                    @else
                    Комментарий
                    @endif
                </h3>
                @foreach($news_more as $news_m)
                <div class="comment-item">
                    <div class="row no-margin">
                        <div class="col-lg-1 col-xs-12 col-sm-2 no-margin">
                            <div class="avatar">
                                <img src="{{asset('images/default-avatar.jpg')}}" alt="avatar">
                            </div>
                        </div>
                        <div class="col-xs-12 col-lg-11 col-sm-10 no-margin-right">
                            <div class="comment-body">
                                <div class="meta-info">
                                    <header class="row no-margin">
                                        <div class="pull-left">
                                            <h4 class="author"><a href="#">{{App\User::find($news_m->user_id)->name}}</a></h4>
                                            {{--<span class="date">- 21 hours ago</span>--}}
                                        </div>
                                        <div class="date inline pull-right">
                                        {{App\NewsComments::find($news_m->id)->created_at}}

                                        @if(Auth::check() && Auth::user()->level()==2)
                                            <div>
                                                <a href="{{url('news-comment/delete/'.$news_m->id)}}"
                                                    class="btn btn-danger btn-sm pull-right">Удалить
                                                </a>
                                            </div>
                                        @endif
                                        </div>
                                    </header>
                                </div>
                                
                                <p class="comment-content">{{$news_m->comment}}</p>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                
            </section>
            @if(Auth::check())
            <section id="reply-block" class="leave-reply">
                <h3>Написать комментарий</h3>
                <p>Ваша почта не публикуется. Поменченные поля являються объязательными <abbr class="required">*</abbr> </p>

                <form role="form" class="reply-form cf-style-1" action="{{url('add-news-comment')}}" method="POST">
                    {{ csrf_field() }}
                    <div class="row field-row">
                        <div class="col-xs-12 col-sm-6">
                            <label>Имя*</label>
                            <input class="le-input" name="name">
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <label>Почта*</label>
                            <input class="le-input" type="email" name="email">
                        </div>
                    </div>

                    <div class="row field-row">
                        <div class="col-xs-12">
                            <label>Сообщение</label>
                            <textarea rows="10" id="inputComment" name="comment" 
                                class="form-control le-input"></textarea>
                        </div>
                    </div>
                    <input type="hidden" name="news_id" value="{{$new->id}}">
                    <input type="hidden" name="user_id" value="{{$user->id}}">
                    <button class="le-button big post-comment-button" type="submit">Отправить</button>
                    @if (count($errors)) 
                    <div class="form-group">
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                @if($error == 'The name must be a string.')
                                    <li>Введите корректное имя</li>
                                @endif
                                @if($error == 'The comment must be a string.')
                                    <li>Некоррекнтный комментарий</li>
                                @endif
                                @if($error == 'The comment field is required.')
                                    <li>Поле комментарий пусто</li>
                                @endif
                                @if($error == 'The name field is required.')
                                    <li>Поле имя пусто</li>
                                @endif
                                @if($error == 'The email must be a valid email address.')
                                    <li>Некоррекнтный Email</li>
                                @endif
                                @if($error == 'The email field is required.')
                                    <li>Поле email пусто</li>
                                @endif
                                @endforeach    
                            </ul>
                        </div>
                    </div>
                    @endif
        
                </form>
            </section>
            @endif
        </div><!-- /.posts -->
    </div>
</section>

@endif
@endsection