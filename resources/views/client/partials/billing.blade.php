@extends('client.partials.layouts')
@section('content')
    <div class="animate-dropdown">
        <!-- ========================================= BREADCRUMB ========================================= -->
        <div id="top-mega-nav">
            <div class="container">
                <nav>
                    <ul class="inline">
                        <li class="dropdown le-dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-list"></i> Все категории
                            </a>
                            <ul class="dropdown-menu">
                                @foreach($categories as $cat)
                                    @foreach($cat->sub_categories as $sub_cat)
                                        <li>
                                            <a href="{{url('category/'.$sub_cat->id)}}">{{$sub_cat->name}}</a>
                                        </li>
                                    @endforeach
                                @endforeach
                            </ul>
                        </li>

                        <li class="breadcrumb-nav-holder">
                            <ul>
                                <li class=" breadcrumb-item">
                                    <a href="{{url('/')}}"  >Главная</a>
                                </li><!-- /.breadcrumb-item -->

                                <li class="breadcrumb-item current">
                                    <a href="#">Просмотр корзины</a>
                                </li><!-- /.breadcrumb-item -->
                            </ul>
                        </li><!-- /.breadcrumb-nav-holder -->
                    </ul>
                </nav>
            </div><!-- /.container -->
        </div><!-- /#top-mega-nav -->
        <!-- ========================================= BREADCRUMB : END ========================================= -->
    </div>
    <section id="checkout-page">
        <div class="container">
            <div class="col-xs-12 no-margin">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                @if($error == 'Your cart is empty. Please add product on your cart')
                                    <li>Корзина не должна быть пуста.</li>
                                @endif

                                @if($error == 'The name field is required.')
                                    <li>Поле имя не должно быть пустым.</li>
                                @endif

                                @if($error == 'The surname field is required.')
                                    <li>Поле Фамилия не должно быть пустым.</li>
                                @endif

                                @if($error == 'The address field is required.')
                                    <li>Поле Адрес не должно быть пустым.</li>
                                @endif
                                @if($error == 'The email field is required.')
                                    <li>Поле email не должно быть пустым.</li>
                                @endif
                                @if($error == 'The phone field is required.')
                                    <li>Поле телефон не должно быть пустым.</li>
                                @endif
                                @if($error == 'The group1 field is required.')
                                    <li>Прошу выбрать способ доставки</li>
                                @endif
                                @if($error == 'The email must be a valid email address.')
                                    <li>Формат введенного email не корректный</li>
                                @endif
                                @if($error == 'You cart is empty. Please add product on your cart')
                                    <li>Ваша корзина пуста. Пожалуйста добавьте товар.</li>
                                @endif
                                @if($error == 'The group2 field is required.')
                                    <li>Прошу выбрать способ оплаты</li>
                                @endif
                                
                            @endforeach
                        </ul>
                    </div>
                @endif
                    @if(session()->has('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div>
                    @endif
                <form method="POST" action="{{route('add.request.order')}}">
                    {{csrf_field()}}
                    <div class="billing-address">
                        <h2 class="border h1">ОПЛАТА И ДОСТАВКА</h2>
                        <div class="row field-row">
                            <div class="col-xs-12 col-sm-6">
                                <label>Имя <span class="text-danger">*</span></label>
                                <input name="name" class="le-input" value="{{ old('name') }}">
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <label>Фамилия <span class="text-danger">*</span></label>
                                <input name="surname" class="le-input" value="{{ old('surname') }}">
                            </div>
                        </div><!-- /.field-row -->

                        <div class="row field-row">
                            <div class="col-xs-12 col-sm-6">
                                <label>Адрес <span class="text-danger">*</span></label>
                                <input name="address" class="le-input placeholder" 
                                    data-placeholder="адрес, улица" value="{{ old('address') }}">
                            </div>
                            <div class="col-xs-12 col-sm-6 ">
                                <label>Город <span class="text-danger">*</span></label>
                                <input name="city" class="le-input placeholder" 
                                    data-placeholder="Город" value="{{ old('city') }}">
                            </div>
                        </div><!-- /.field-row -->

                        <div class="row field-row">
                            <div class="col-xs-12 col-sm-6">
                                <label>Email <span class="text-danger">*</span></label>
                                <input name="email" class="le-input" value="{{ old('email') }}">
                            </div>

                            <div class="col-xs-12 col-sm-6">
                                <label>Телефон <span class="text-danger">*</span></label>
                                <input name="phone" class="le-input" value="{{ old('phone') }}">
                            </div>
                            
                            <div class="col-xs-12 col-sm-6 pull-right">
                            <br/>
                                <label>Дополнителный телефон</label>
                                <input name="otherPhone" class="le-input" value="{{ old('otherPhone') }}">
                            </div>
                        </div><!-- /.field-row -->

                    </div><!-- /.billing-address -->

                    <section id="your-order">
                        <h2 class="border h1">Ваш заказ</h2>
                        @if($listOrdersDetail)
                            @foreach($listOrdersDetail as $key => $order)
                                <div class="row no-margin order-item">
                                    <div class="col-xs-4 col-sm-1 no-margin">
                                        <a href="#"
                                           class="qty">{{\App\Orders::where('products_id', $order->products_id)->where('session_order_id', $order->session_order_id)->count()}}
                                            x</a>
                                    </div>

                                    <div class="col-xs-4">
                                        <div class="title"><a
                                                    href="#">{{\App\Products::find($order->products_id)->name}}</a>
                                        </div>
                                        <div class="brand">
                                            @php
                                                try{
                                                    $brand = \App\Products::find($order->products_id)->brand;
                                                    echo $brand;
                                                }catch(\Exception $e){
                                                  echo "";
                                            }
                                            @endphp
                                        </div>
                                    </div>

                                    <div class="col-xs-4 col-sm-2 no-margin">
                                        <div class="price">
                                            TJS {{\App\Orders::where('products_id', $order->products_id)->where('session_order_id', $order->session_order_id)->sum('price')}}
                                        </div>
                                    </div>
                                </div><!-- /.order-item -->
                            @endforeach
                        @endif
                    </section><!-- /#your-order -->

                    <div id="total-area" class="row no-margin">
                        <div class="col-xs-12 col-lg-4 col-lg-offset-8 no-margin-right">
                            <div id="subtotal-holder">
                                <ul class="tabled-data inverse-bold no-border">
                                    <li>
                                        <label>Подытог</label>
                                        <div class="value ">
                                            @if(isset($sum_of_items))
                                                TJS {{$sum_of_items}}
                                            @else
                                                0
                                            @endif
                                        </div>
                                    </li>
                                    <li>
                                        <label>Доставка</label>
                                        <div class="value">
                                            <div class="radio-group">
                                                <input class="le-radio" type="radio" name="group1" value="own"><i
                                                        class="fake-box"></i>
                                                <div class="radio-label bold">Самовывоз</div>
                                                <br>
                                                <input class="le-radio" type="radio" name="group1"
                                                       value="other"><i class="fake-box"></i>
                                                <div class="radio-label">Курьер<br><span class="bold"></span></div>
                                            </div>
                                        </div>
                                    </li>

                                    <li>
                                        <label>Способ оплаты</label>
                                        <div class="value">
                                            <div class="radio-group">
                                                <input class="le-radio" type="radio" name="group2" value="off"><i
                                                        class="fake-box"></i>
                                                <div class="radio-label bold">Наличными</div>
                                                <br>
                                                <input class="le-radio" type="radio" name="group2"
                                                       value="on"><i class="fake-box"></i>
                                                <div class="radio-label">С Картой<br><span class="bold">Корти милли</span></div>
                                            </div>
                                        </div>
                                        <label>
                                            <div class="billing-cart-logo">
                                                <img alt="" src="{{asset('images/payments/pay.png')}}" style="width:15%;height:2%;top: -40%;">
                                            </div>
                                        </label>
                                    </li>
                                    <li style="margin:0;">
                                        
                                    </li>

                                </ul><!-- /.tabled-data -->

                                <ul id="total-field" class="tabled-data inverse-bold ">
                                    <li>
                                        <label>Итого</label>
                                        <div class="value">
                                            @if(isset($sum_of_items))
                                                TJS {{$sum_of_items}}
                                            @else
                                                0
                                            @endif
                                        </div>
                                    </li>
                                </ul><!-- /.tabled-data -->

                            </div><!-- /#subtotal-holder -->
                        </div><!-- /.col -->
                    </div><!-- /#total-area -->

                    

                    <div class="place-order-button">
                        <input type="submit" class="le-button big" value="ПОДТВЕРДИТЬ ЗАКАЗ">
                    </div><!-- /.place-order-button -->
                </form>
            </div><!-- /.col -->

        </div><!-- /.container -->
    </section>


@endsection