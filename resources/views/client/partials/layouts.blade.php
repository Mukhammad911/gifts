<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Онлайн магазин</title>

    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">

    <!-- Customizable CSS -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/colors/green.css')}}">
    <link rel="stylesheet" href="{{asset('css/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('css/owl.transitions.css')}}">
    <link rel="stylesheet" href="{{asset('css/animate.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/colors/red.css')}}">
{{-- <link rel="stylesheet" href="{{asset('css/load-more-button.css')}}"> --}}

<!-- Fonts -->
    <!-- <link href='//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800' rel='stylesheet' type='text/css'> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <!-- Icons/Glyphs -->
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">

    <!-- Favicon -->
    <link rel="icon" href="{{ asset('ico/favico-gift.png') }}" type="image/png" sizes="16x16">
    <link rel="icon" href="{{ asset('ico/favico-gift.png') }}" type="image/png" sizes="32x32">

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-52598430-1', 'auto');
        ga('send', 'pageview');
    </script>

</head>
<body>
<div class="wrapper">
@include('client.partials.preheader')

@include('client.partials.header')

<!-- <div id="top-banner-and-menu">
    
        
        <div class="container"> -->
    @yield('content')
</div>
</div>



<div>
    @include('client.partials.footer')
</div>



</div>


<script src="{{asset('js/jquery-1.10.2.min.js')}}"></script>
<script src="{{asset('js/jquery-migrate-1.2.1.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="//maps.google.com/maps/api/js?key=AIzaSyDDZJO4F0d17RnFoi1F2qtw4wn6Wcaqxao&sensor=false&amp;language=en"></script>
<script src="{{asset('js/gmap3.min.js')}}"></script>
<script src="{{asset('js/bootstrap-hover-dropdown.min.js')}}"></script>
<script src="{{asset('js/owl.carousel.min.js')}}"></script>
<script src="{{asset('js/css_browser_selector.min.js')}}"></script>
<script src="{{asset('js/echo.min.js')}}"></script>
<script src="{{asset('js/jquery.easing-1.3.min.js')}}"></script>
<script src="{{asset('js/bootstrap-slider.min.js')}}"></script>
<script src="{{asset('js/jquery.raty.min.js')}}"></script>
<script src="{{asset('js/jquery.prettyPhoto.min.js')}}"></script>
<script src="{{asset('js/jquery.customSelect.min.js')}}"></script>
<script src="{{asset('js/wow.min.js')}}"></script>
<script src="{{asset('js/buttons.js')}}"></script>
<script src="{{asset('js/scripts.js')}}"></script>

</body>
</html>