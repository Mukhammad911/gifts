@extends('client.partials.layouts')
@section('content')

<div class="animate-dropdown">
    <!-- ========================================= BREADCRUMB ========================================= -->
    <div id="top-mega-nav">
        <div class="container">
            <nav>
                <ul class="inline">
                    <li class="dropdown le-dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-list"></i> Товары по категориям
                        </a>

                        <ul class="dropdown-menu">
                        
                        @foreach(App\Sub_categories::all() as $sub_cat)
                            <li><a href="{{url('category/'.$sub_cat->id)}}">{{$sub_cat->name}}</a></li>
                        @endforeach    
                        </ul>
                    </li>

                    <li class="breadcrumb-nav-holder">
                        <ul>
                        <li class=" breadcrumb-item">
                            <a href="{{url('/')}}"  >Главная</a>
                            
                        </li><!-- /.breadcrumb-item -->

                        <li class=" breadcrumb-item current">
                            <a href="#" >
                                О нас   
                            </a>
                        </li><!-- /.breadcrumb-item -->
                        
                    </li><!-- /.breadcrumb-nav-holder -->

                </ul><!-- /.inline -->
            </nav>

        </div><!-- /.container -->
    </div><!-- /#top-mega-nav -->
    <!-- ========================================= BREADCRUMB : END ========================================= -->
</div>

<main id="about-us">
    <div class="container inner-top-xs inner-bottom-sm">
        <div class="row">
            <div class="col-xs-12 col-md-8 col-lg-8 col-sm-6">

                {{--<section id="who-we-are" class="section m-t-0">
                    <h2>О нас</h2>
                    <p>
                        Интеренет магазин Фарад - уникальный онлайн магазин
                        и торговая площадка для покупки и продажи товаров в 
                        Таджикистане. Широкий ассортимент товаров, включая 
                        строительные материалы, одежда, обувь, ассортимент 
                        смартфонов и гаджетов, бытовой техники, компьютеров, 
                        электроинструментов и парфюмерии от мировых брендов. 
                    </p>
                </section><!-- /#who-we-are -->--}}

            </div><!-- /.col -->
            
        </div><!-- /.row -->
    </div><!-- /.container -->
    
</main>



@endsection