@extends('client.partials.layouts')
@section('content')

<div class="animate-dropdown">
    <!-- ========================================= BREADCRUMB ========================================= -->
    <div id="top-mega-nav">
        <div class="container">
            <nav>
                <ul class="inline">
                    <li class="dropdown le-dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-list"></i> Товары по категориям
                        </a>

                        <ul class="dropdown-menu">
                        
                        @foreach(App\Sub_categories::all() as $sub_cat)
                            <li><a href="{{url('category/'.$sub_cat->id)}}">{{$sub_cat->name}}</a></li>
                        @endforeach    
                        </ul>
                    </li>

                    <li class="breadcrumb-nav-holder">
                        <ul>
                        <li class=" breadcrumb-item">
                            <a href="{{url('/')}}"  >Главная</a>
                            
                        </li><!-- /.breadcrumb-item -->

                        <li class=" breadcrumb-item current">
                            <a href="#" >
                                Оплата   
                            </a>
                        </li><!-- /.breadcrumb-item -->
                        
                    </li><!-- /.breadcrumb-nav-holder -->

                </ul><!-- /.inline -->
            </nav>

        </div><!-- /.container -->
    </div><!-- /#top-mega-nav -->
    <!-- ========================================= BREADCRUMB : END ========================================= -->
</div>

<main id="about-us">
    <div class="container inner-top-xs inner-bottom-sm">
        <div class="row">
            <div class="col-xs-12 col-md-8 col-lg-8 col-sm-6">

                <section id="who-we-are" class="section m-t-0">
                    <h2>Оплата</h2>
                    <p>
                        Заказ можно оплатить прямо сайте с помощью банковской 
                        карты Национальной Платежной Системы «Корти Милли», 
                        независимо от банка - эмитента вашей карты или в виде 
                        исключения, при получении товара, наличными. При оплате 
                        на сайте вы получите скидку на доставку. 
                        Все оплаты по картам Корти Милли БЕСПЛАТНО. 
                    </p>
                    <p>
                        При оформлении заказа, выберите метод оплаты – 
                        банковскую карту, введите данные карты и нажмите 
                        оплатить. Процесс и надёжность оплаты обеспечивает 
                        онлайн сервис Первого Микрофинансового Банка. 
                    </p>
                    <p>
                        После оплаты заказа, вам на почту или СМС-ем придет электронный 
                        чек. Если у вас возникли вопросы при оформлении заказа, 
                        вы можете обратиться к нашему онлайн консультанту или же 
                        написать нам на почту. 
                    </p>
                </section><!-- /#who-we-are -->

            </div><!-- /.col -->
            
        </div><!-- /.row -->
    </div><!-- /.container -->
    
</main>

@endsection