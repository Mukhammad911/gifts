@extends('client.partials.layouts')
@section('content')

    <div class="animate-dropdown">
        <!-- ========================================= BREADCRUMB ========================================= -->
        <div id="top-mega-nav">
            <div class="container">
                <nav>
                    <ul class="inline">
                        <li class="dropdown le-dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-list"></i> Товары по категориям
                            </a>

                            <ul class="dropdown-menu">

                                @foreach(App\Sub_categories::all() as $sub_cat)
                                    <li><a href="{{url('category/'.$sub_cat->id)}}">{{$sub_cat->name}}</a></li>
                                @endforeach
                            </ul>
                        </li>

                        <li class="breadcrumb-nav-holder">
                            <ul>
                                <li class=" breadcrumb-item">
                                    <a href="{{url('/')}}"  >Главная</a>

                                </li><!-- /.breadcrumb-item -->

                                <li class=" breadcrumb-item current">
                                    <a href="#" >
                                        Контакты
                                    </a>
                                </li><!-- /.breadcrumb-item -->

                                </li><!-- /.breadcrumb-nav-holder -->

                            </ul><!-- /.inline -->
                </nav>

            </div><!-- /.container -->
        </div><!-- /#top-mega-nav -->
        <!-- ========================================= BREADCRUMB : END ========================================= -->
    </div>

    <main id="about-us">
        <div class="container inner-top-xs inner-bottom-sm">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-lg-8 col-sm-6">
                    <section id="who-we-are" class="section m-t-0">
                        @if(\Session::has('message'))
                            <div class="alert alert-success" role="alert">
                                <p> {{Session::get('message')}}</p>
                            </div>
                        @endif
                        <h2>Контакты</h2>
                        <p>
                            Адрес г. Душанбе, ул. Фотех Ниёзи 51
                        </p>
                        <p>
                            Email: giftstj@gmail.com
                        </p>
                        <p>
                            Tel: +93 864 54 54
                        </p>
                        <h2>Режим работы</h2>
                        <p>
                            Понедельник — Пятница: с 09 00 по 20 00 <br/>
                            Суббота -  Воскресенье: с 11:00 по 16:00
                        </p>
                    </section><!-- /#who-we-are -->

                </div><!-- /.col -->

            </div><!-- /.row -->
        </div><!-- /.container -->

    </main>


    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <section class="section leave-a-message">
                    <h2 class="bordered">Напишите нам</h2>
                    <form id="contact-form" class="contact-form cf-style-1 inner-top-xs"
                          method="post" action="{{url('/send')}}">
                        {{ csrf_field() }}
                        <div class="row field-row">
                            <div class="col-xs-12 col-sm-6">
                                <label>Ваше имя*</label>
                                <input class="le-input" type="text" name="name">
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <label>Ваш Email*</label>
                                <input class="le-input" type="email" name="email">
                            </div>
                        </div><!-- /.field-row -->

                        <div class="field-row">
                            <label>Тема</label>
                            <input type="text" name="theme" class="le-input">
                        </div><!-- /.field-row -->

                        <div class="field-row">
                            <label>Ваше сообщение</label>
                            <textarea rows="8" class="le-input" name="message" ></textarea>
                        </div><!-- /.field-row -->

                        <div class="buttons-holder">
                            <button type="submit" class="le-button huge">Отправить</button>
                        </div><!-- /.buttons-holder -->

                        @if (count($errors))
                            <div class="form-group">
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{$error}}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        @endif
                    </form><!-- /.contact-form -->
                </section><!-- /.leave-a-message -->
            </div><!-- /.col -->

            <div class="col-md-4">
                {{-- <section class="our-store section inner-left-xs">
                     <h2 class="bordered">Наш Магазин</h2>
                     <address>
                         Адрес г. Душанбе, <br>
                         ул. Саъди Шерози 64
                     </address>
                     <address>
                         Email: faradshophelp@gmail.com
                     </address>
                     <address>
                         Tel: +992 93 583 21 33
                     </address>
                     <h3>Режим Работы</h3>
                     <ul class="list-unstyled operation-hours">
                         <li class="clearfix">
                             <span class="day">Понедельник:</span>
                             <span class="pull-right hours">с 9:00 по 17:00</span>
                         </li>
                         <li class="clearfix">
                             <span class="day">Вторник:</span>
                             <span class="pull-right hours">с 9:00 по 17:00</span>
                         </li>
                         <li class="clearfix">
                             <span class="day">Среда:</span>
                             <span class="pull-right hours">с 9:00 по 17:00</span>
                         </li>
                         <li class="clearfix">
                             <span class="day">Четверг:</span>
                             <span class="pull-right hours">с 9:00 по 17:00</span>
                         </li>
                         <li class="clearfix">
                             <span class="day">Пятница:</span>
                             <span class="pull-right hours">с 9:00 по 17:00</span>
                         </li>
                         <li class="clearfix">
                             <span class="day">Суббота:</span>
                             <span class="pull-right hours">с 11:00 по 14:00</span>
                         </li>
                         <li class="clearfix">
                             <span class="day">Воскресенье</span>
                             <span class="pull-right hours">с 11:00 по 14:00</span>
                         </li>
                     </ul>
                 </section><!-- /.our-store -->--}}
            </div><!-- /.col -->

        </div><!-- /.row -->
    </div>


@endsection