@extends('client.partials.layouts')
@section('content')
                                    @if(isset($all_products))
                                        @if(count($all_products) > 0)
                                            @foreach($all_products as $value)
                                            @foreach($value as $product)
                                            
                                                <div class="product-item product-item-holder">
                                                    @if($product->feature)
                                                        <div class="ribbon red"><span>Скоро</span></div>
                                                    @endif

                                                    @if($product->onsale)
                                                        <div class="ribbon red"><span>Распродажа</span></div>
                                                    @endif
                                                    @if($product->new)
                                                        <div class="ribbon blue"><span>Новый</span></div>
                                                    @endif
                                                    @if($product->bestseller)
                                                        <div class="ribbon green"><span>Топ продаж</span></div>
                                                    @endif
                                                    <div class="row">
                                                        <div class="no-margin col-xs-12 col-sm-4 image-holder">
                                                            <div class="image">
                                                                <img alt="" src="{{asset('images/blank.gif')}}"
                                                                     data-echo="{{asset('images/products/'.$product->img)}}"/>
                                                            </div>
                                                        </div><!-- /.image-holder -->
                                                        <div class="no-margin col-xs-12 col-sm-5 body-holder">
                                                            <div class="body">
                                                                @if($product->discount != "")
                                                                    <div class="label-discount green">{{$product->discount}}
                                                                        %
                                                                        Акция
                                                                    </div>
                                                                @endif
                                                                <div class="title">
                                                                    <a href="{{url('product/show/'.$product->id)}}">{{$product->name}}</a>
                                                                </div>
                                                                @if($product->brand != "")
                                                                    <div class="brand">{{$product->brand}}</div>
                                                                @endif
                                                                <div class="excerpt">
                                                                    @php
                                                                        try{
                                                                           $description = App\Products::find($product->id)->descriptions()->first()->name;
                                                                           echo('<p>'.$description.'</p>');
                                                                        }
                                                                        catch(\Exception $e)
                                                                        {}
                                                                    @endphp
                                                                </div>
                                                                {{--<div class="addto-compare">
                                                                    <a class="btn-add-to-compare" href="#">add to compare list</a>
                                                                </div> --}}
                                                            </div>
                                                        </div><!-- /.body-holder -->
                                                        <div class="no-margin col-xs-12 col-sm-3 price-area">
                                                            <div class="right-clmn">
                                                                <div class="price-current">{{$product->price}} TJS</div>
                                                                @if($product->old_price != "")
                                                                    <div class="price-prev">{{$product->old_price}}
                                                                        TJS
                                                                    </div>
                                                                @else
                                                                    <div class="price-prev"></div>
                                                                @endif
                                                                @if($product->available)
                                                                    <div class="availability">
                                                                        <label>Доступен:</label><span
                                                                                class="available">  В наличии</span>
                                                                    </div>
                                                                @else
                                                                    <div class="availability">
                                                                        <label>Доступен:</label><span
                                                                                class="text-danger">  Нет в наличии</span>
                                                                    </div>
                                                                @endif
                                                                <a class="le-button" href="#">В корзину</a>
                                                                {{-- <a class="btn-add-to-wishlist" href="#">add to wishlist</a> --}}
                                                            </div>
                                                        </div><!-- /.price-area -->
                                                    </div><!-- /.row -->
                                                </div><!-- /.product-item -->
                                                @endforeach
                                            @endforeach
                                        @endif
                                    @endif
@endsection
