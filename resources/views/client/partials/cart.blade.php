@extends('client.partials.layouts')
@section('content')
    <div class="animate-dropdown">
        <!-- ========================================= BREADCRUMB ========================================= -->
        <div id="top-mega-nav">
            <div class="container">
                <nav>
                    <ul class="inline">
                        <li class="dropdown le-dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-list"></i> Все категории
                            </a>
                            <ul class="dropdown-menu">
                                @foreach($categories as $cat)
                                    @foreach($cat->sub_categories as $sub_cat)
                                        <li>
                                            <a href="{{url('category/'.$sub_cat->id)}}">{{$sub_cat->name}}</a>
                                        </li>
                                    @endforeach
                                @endforeach
                            </ul>
                        </li>

                        <li class="breadcrumb-nav-holder">
                            <ul>
                                <li class="breadcrumb-item current gray">
                                    <a href="javascript:void(0)">Просмотр корзины</a>
                                </li>
                            </ul>
                        </li><!-- /.breadcrumb-nav-holder -->
                    </ul>
                </nav>
            </div><!-- /.container -->
        </div><!-- /#top-mega-nav -->
        <!-- ========================================= BREADCRUMB : END ========================================= -->
    </div>
    <section id="cart-page">
        <div class="container">
            <!-- ========================================= CONTENT ========================================= -->
            <div class="col-xs-12 col-md-9 items-holder no-margin">
                @if($listOrdersDetail)
                    @foreach($listOrdersDetail as $key => $order)
                        <div class="row no-margin cart-item">
                            <div class="col-xs-12 col-sm-2 no-margin">
                                <a href="#" class="thumb-holder">
                                    <img class="lazy" alt=""  src="{{asset('images/products/'. \App\Products::find($order->products_id)->img)}}" alt="" />
                                </a>
                            </div>

                            <div class="col-xs-12 col-sm-5 ">
                                <div class="title">
                                    <a href="#">{{\App\Products::find($order->products_id)->name}}</a>
                                </div>
                                <div class="brand">
                                    @php
                                        try{
                                            $brand = \App\Products::find($order->products_id)->brand;
                                            echo $brand;
                                        }catch(\Exception $e){
                                          echo "";
                                    }
                                    @endphp
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-3 no-margin">
                                <div class="quantity">
                                    <div class="le-quantity">
                                        <form>
                                            <a class="minus" href="{{url('order-product/minus/'.$order->products_id)}}"></a>
                                            <input name="quantity" readonly="readonly" type="text" value="{{\App\Orders::where('products_id', $order->products_id)->where('session_order_id', $order->session_order_id)->count()}}">
                                            <a class="plus" href="{{url('order-product/plus/'.$order->products_id)}}"></a>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-2 no-margin">
                                <div class="price">
                                    TJS {{\App\Orders::where('products_id', $order->products_id)->where('session_order_id', $order->session_order_id)->sum('price')}}
                                </div>
                                <a class="close-btn" href="{{url('order-product/remove-all/'.$order->products_id)}}"></a>
                            </div>
                        </div><!-- /.cart-item -->
                    @endforeach
                @endif
            </div>
            <!-- ========================================= CONTENT : END ========================================= -->

            <!-- ========================================= SIDEBAR ========================================= -->

            <div class="col-xs-12 col-md-3 no-margin sidebar ">
                <div class="widget cart-summary">
                    <h1 class="border">Корзина</h1>
                    <div class="body">
                        <ul class="tabled-data no-border inverse-bold">
                            <li>
                                <label>Подытог: </label>
                                <div class="value pull-right">
                                    @if($sum_of_items)
                                        TJS {{$sum_of_items}}
                                    @else
                                        TJS 0
                                    @endif
                                </div>
                            </li>
                            <li>
                                <label>Доставка</label>
                                <div class="value pull-right">Есть</div>
                            </li>
                        </ul>
                        <ul id="total-price" class="tabled-data inverse-bold no-border">
                            <li>
                                <label>Итого</label>
                                <div class="value pull-right">
                                    @if($sum_of_items)
                                        TJS {{$sum_of_items}}
                                    @else
                                        TJS 0
                                    @endif
                                </div>
                            </li>
                        </ul>
                        <div class="buttons-holder">
                            <a class="le-button big" href="{{url('order/billing-page')}}">Оформить заказ</a>
                            <a class="simple-link block" href="{{url('/')}}">Продолжить закупки</a>
                        </div>
                    </div>
                </div><!-- /.widget -->

                {{--<div id="cupon-widget" class="widget">
                    <h1 class="border">use coupon</h1>
                    <div class="body">
                        <form>
                            <div class="inline-input">
                                <input data-placeholder="enter coupon code" type="text" class="placeholder">
                                <button class="le-button" type="submit">Apply</button>
                            </div>
                        </form>
                    </div>
                </div> --}}<!-- /.widget -->
            </div> <!-- /.sidebar -->

            <!-- ========================================= SIDEBAR : END ========================================= -->
        </div>
    </section>
@endsection