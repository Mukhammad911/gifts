@extends('client.partials.layouts')
@section('content')

<div class="animate-dropdown">
    <!-- ========================================= BREADCRUMB ========================================= -->
    <div id="top-mega-nav">
        <div class="container">
            <nav>
                <ul class="inline">
                    <li class="dropdown le-dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-list"></i> Товары по категориям
                        </a>

                        <ul class="dropdown-menu">
                        
                        @foreach(App\Sub_categories::all() as $sub_cat)
                            <li><a href="{{url('category/'.$sub_cat->id)}}">{{$sub_cat->name}}</a></li>
                        @endforeach    
                        </ul>
                    </li>

                    <li class="breadcrumb-nav-holder">
                        <ul>
                        <li class=" breadcrumb-item">
                            <a href="{{url('/')}}"  >Главная</a>
                            
                        </li><!-- /.breadcrumb-item -->

                        <li class=" breadcrumb-item current">
                            <a href="#" >
                                Доставка   
                            </a>
                        </li><!-- /.breadcrumb-item -->
                        
                    </li><!-- /.breadcrumb-nav-holder -->

                </ul><!-- /.inline -->
            </nav>

        </div><!-- /.container -->
    </div><!-- /#top-mega-nav -->
    <!-- ========================================= BREADCRUMB : END ========================================= -->
</div>

<main id="about-us">
    <div class="container inner-top-xs inner-bottom-sm">
        <div class="row">
            <div class="col-xs-12 col-md-8 col-lg-8 col-sm-6">

               {{-- <section id="who-we-are" class="section m-t-0">
                    <h2>Доставка</h2>
                    <p>
                        Фарад экспресс или наши торговые партнёры доставляют
                        товары по г. Душанбе прямо на дом или же в доступных 
                        пунктах выдачи товара в г. Душанбе и в других регионах 
                        Республики Таджикистан. В процессе оформления заказа 
                        вам нужно указать точный адрес доставки или же выбрать 
                        доступные пункты выдачи товара. Стоимость доставки 
                        в разные регионы может отличаться в зависимости от 
                        региона и заказанного товара. 
                    </p>
                    <h2>Время доставки</h2>
                    <p>
                        Доставка осуществляется в течении 1-3 дней по городу Душанбе 
                        в зависимости от вида товара. Доставка в других доступных 
                        регионах РТ занимает от 3 до 15 дней в зависимости от региона 
                        и заказанного вида товара. 
                    </p>
                    <h2>Получение заказа</h2>
                    <p>
                        При получении товара, прошу убедиться, что товар соответствует 
                        вашему заказу, товар упакован, отсутствуют любые повреждения, 
                        имеются требуемые документы гарантии и т.д. При обнаружении любых 
                        таких фактов несоответствия или нарушения, вы имеете право вернуть 
                        товар и получить сумму купленного заказа обратно на ваш счет или же 
                        наличными. 
                    </p>
                </section><!-- /#who-we-are -->--}}

            </div><!-- /.col -->
            
        </div><!-- /.row -->
    </div><!-- /.container -->

    <section id="what-can-we-do-for-you" class="row light-bg inner-sm">
        <div class="container">
            <div class="row">
                
                <div class="col-md-12">
                    <ul class="services list-unstyled row m-t-35">
                        <li class="col-md-4">
                            <div class="service">
                                <div class="service-icon primary-bg"><i class="fa fa-truck"></i></div>
                                <h3>Быстрая доставка</h3>
                            </div>
                        </li>
                        <li class="col-md-4">
                            <div class="service">
                                <div class="service-icon primary-bg"><i class="fa fa-life-saver"></i></div>
                                <h3>Поддержка 24/7</h3>
                            </div>
                        </li>
                        <li class="col-md-4">
                            <div class="service">
                                <div class="service-icon primary-bg"><i class="fa fa-star"></i></div>
                                <h3>Гарантия качество</h3>
                            </div>
                        </li>
                    </ul><!-- /.services -->
                </div>
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /#what-can-we-do-for-you -->

    <section id="our-clients" class="row inner-sm">
        <div class="container">
            <h2 class="sr-only">Our Clients</h2>
            <ul class="list-unstyled row list-clients">
                <li class="col-md-2"><a href="#"><img alt="" src="assets/images/brands/brand-01.jpg"></a></li>
                <li class="col-md-2"><a href="#"><img alt="" src="assets/images/brands/brand-02.jpg"></a></li>
                <li class="col-md-2"><a href="#"><img alt="" src="assets/images/brands/brand-03.jpg"></a></li>
                <li class="col-md-2"><a href="#"><img alt="" src="assets/images/brands/brand-04.jpg"></a></li>
                <li class="col-md-2"><a href="#"><img alt="" src="assets/images/brands/brand-01.jpg"></a></li>
                <li class="col-md-2"><a href="#"><img alt="" src="assets/images/brands/brand-02.jpg"></a></li>
            </ul>
        </div>
    </section><!-- /#our-clients .row -->
</main>

@endsection