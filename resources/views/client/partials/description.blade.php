@extends('client.partials.layouts')
@section('content')
    @if($products)
    @php
    
    
    @endphp
        <div class="animate-dropdown">
            <!-- ========================================= BREADCRUMB ========================================= -->
            <div id="top-mega-nav">
                <div class="container">
                    <nav style="background-color: white;">
                        <ul class="inline">
                            <li class="dropdown le-dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-list"></i> Товары по категориям
                                </a>

                                <ul class="dropdown-menu">
                                    @foreach(App\Sub_categories::all() as $sub_cat)
                                        <li><a href="{{url('category/'.$sub_cat->id)}}">{{$sub_cat->name}}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                            {{--}}
                            <li class="breadcrumb-nav-holder">
                                <ul>
                                    <li class=" breadcrumb-item">
                                        <a href="{{url('/')}}">Главная</a>

                                    </li><!-- /.breadcrumb-item -->

                                    <li class="dropdown breadcrumb-item">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                                            @if($bc_categories != null)
                                                {{$bc_categories->name}}
                                            @endif

                                        </a>
                                        <ul class="dropdown-menu">
                                            <li>

                                                @if($bc_sub_categories_list->count() > 0)
                                                    @foreach($bc_sub_categories_list as $product)
                                                        <a href="{{url('category/'.$product->id)}}">{{$product->name}}</a>
                                                    @endforeach
                                                @endif

                                            </li>
                                        </ul>
                                    </li><!-- /.breadcrumb-item -->

                                    <li class="dropdown breadcrumb-item">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                                            @if($bc_sub_categories != null)
                                                {{$bc_sub_categories->name}}
                                            @endif

                                        </a>
                                        <ul class="dropdown-menu">
                                            <li>


                                                @if($bc_products_list->count() > 0 )
                                                    @foreach($bc_products_list as $product)
                                                        <a href="{{url('product/show/'.$product->id)}}">{{$product->name}}</a>
                                                    @endforeach
                                                @endif

                                            </li>
                                        </ul>
                                    </li><!-- /.breadcrumb-item -->

                                    <li class="breadcrumb-item current">
                                        <a href="#">

                                            @if($bc_products != null)
                                                {{$bc_products->name}}
                                            @endif

                                            {{$products->name}}
                                        </a>
                                    </li><!-- /.breadcrumb-item -->

                                    </li><!-- /.breadcrumb-nav-holder -->
                                --}}
                                </ul><!-- /.inline -->
                    </nav>

                </div><!-- /.container -->
            </div><!-- /#top-mega-nav -->
            <!-- ========================================= BREADCRUMB : END ========================================= -->
        </div>

        <div id="single-product">
            <div class="container">
                <div class="no-margin col-xs-12 col-sm-6 col-md-5 gallery-holder">
                    <div class="product-item-holder size-big single-product-gallery small-gallery">

                        <div id="owl-single-product" class="owl-carousel">
                            <div class="single-product-gallery-item" id="slide1">
                                @if($products->onsale)
                                    <div class="ribbon red"><span>Распродажа</span></div>
                                @endif

                                @if($products->bestseller)
                                    <div class="ribbon green"><span>Топ продаж</span></div>
                                @endif
                                @if($products->new)
                                    <div class="ribbon blue"><span>Новинка</span></div>
                                @endif

                                <a data-rel="prettyphoto">
                                    <img class="img-responsive" alt="" src="{{asset('images/blank.gif')}}"
                                         data-echo="{{asset('images/products/'.$products->img)}}"/>
                                </a>
                                @if($products->discount)
                                    <div class="label-discount green">{{$products->discount}}% Акция</div>

                                @endif
                            </div><!-- /.single-product-gallery-item -->
                            @if($images->count() > 0)
                                @foreach($images as $image)
                                    <div class="single-product-gallery-item" id="slide{{$loop->index+1}}">
                                        <a data-rel="prettyphoto">
                                            <img class="img-responsive" alt="" src="{{asset('images/blank.gif')}}"
                                                 data-echo="{{asset('images/products/'.$image->img)}}"/>
                                        </a>
                                    </div><!-- /.single-product-gallery-item -->
                                @endforeach
                            @endif
                        </div><!-- /.single-product-slider -->

                        <div class="single-product-gallery-thumbs gallery-thumbs">

                            <div id="owl-single-product-thumbnails" class="owl-carousel">
                                @if($images->count() > 0)
                                    <a class="horizontal-thumb active" data-target="#owl-single-product"
                                       data-slide="0" href="#slide1">
                                        <img width="67" height="60" alt="" src="{{asset('images/blank.gif')}}"
                                             data-echo="{{asset('images/products/'.$products->img)}}"/>
                                    </a>

                                    @foreach($images as $image)
                                        <a class="horizontal-thumb" data-target="#owl-single-product"
                                           data-slide="{{$loop->index+1}}" href="#slide{{$loop->index+2}}">
                                            <img width="67" height="60" alt="" src="{{asset('images/blank.gif')}}"
                                                 data-echo="{{asset('images/products/'.$image->img)}}"/>
                                        </a>
                                    @endforeach
                                @endif
                            </div><!-- /#owl-single-product-thumbnails -->

                            <div class="nav-holder left hidden-xs">
                                <a class="prev-btn slider-prev" data-target="#owl-single-product-thumbnails"
                                   href="#prev"></a>
                            </div><!-- /.nav-holder -->

                            <div class="nav-holder right hidden-xs">
                                <a class="next-btn slider-next" data-target="#owl-single-product-thumbnails"
                                   href="#next"></a>
                            </div><!-- /.nav-holder -->

                        </div><!-- /.gallery-thumbs -->

                    </div><!-- /.single-product-gallery -->
                </div><!-- /.gallery-holder -->


                <div class="no-margin col-xs-12 col-sm-7 body-holder">
                    <div class="body">
                        @if($rate != null)
                            @if($rate == 0)
                                <div class="star-holder inline">
                                    <img src="../../../images/star-off.png" alt="1" title="нет оценки">
                                    <img src="../../../images/star-off.png" alt="2" title="нет оценки">
                                    <img src="../../../images/star-off.png" alt="3" title="нет оценки">
                                    <img src="../../../images/star-off.png" alt="4" title="нет оценки">
                                    <img src="../../../images/star-off.png" alt="5" title="нет оценки">
                                </div>
                            @endif

                            @if($rate == 1)
                                <div class="star-holder inline">
                                    <img src="../../../images/star-on.png" alt="1" title="ужасно">
                                    <img src="../../../images/star-off.png" alt="2" title="ужасно">
                                    <img src="../../../images/star-off.png" alt="3" title="ужасно">
                                    <img src="../../../images/star-off.png" alt="4" title="ужасно">
                                    <img src="../../../images/star-off.png" alt="5" title="ужасно">
                                    <input type="hidden" name="score">
                                </div>
                            @endif
                            @if($rate == 2)
                                <div class="star-holder inline">
                                    <img src="../../../images/star-on.png" alt="1" title="плохо">
                                    <img src="../../../images/star-on.png" alt="2" title="плохо">
                                    <img src="../../../images/star-off.png" alt="3" title="плохо">
                                    <img src="../../../images/star-off.png" alt="4" title="плохо">
                                    <img src="../../../images/star-off.png" alt="5" title="плохо">
                                </div>
                            @endif
                            @if($rate == 3)
                                <div class="star-holder inline">
                                    <img src="../../../images/star-on.png" alt="1" title="средне">
                                    <img src="../../../images/star-on.png" alt="2" title="средне">
                                    <img src="../../../images/star-on.png" alt="3" title="средне">
                                    <img src="../../../images/star-off.png" alt="4" title="средне">
                                    <img src="../../../images/star-off.png" alt="5" title="средне">
                                </div>
                            @endif
                            @if($rate == 4)
                                <div class="star-holder inline">
                                    <img src="../../../images/star-on.png" alt="1" title="хорошо">
                                    <img src="../../../images/star-on.png" alt="2" title="хорошо">
                                    <img src="../../../images/star-on.png" alt="3" title="хорошо">
                                    <img src="../../../images/star-on.png" alt="4" title="хорошо">
                                    <img src="../../../images/star-off.png" alt="5" title="хорошо">
                                </div>
                            @endif
                            @if($rate == 5)
                                <div class="star-holder inline">
                                    <img src="../../../images/star-on.png" alt="1" title="отлично">
                                    <img src="../../../images/star-on.png" alt="2" title="отлично">
                                    <img src="../../../images/star-on.png" alt="3" title="отлично">
                                    <img src="../../../images/star-on.png" alt="4" title="отлично">
                                    <img src="../../../images/star-on.png" alt="5" title="отлично">
                                </div>
                            @endif
                        @endif
                        <div class="availability">
                            @if($products->available)
                                <label>Товар:</label>
                                <span class="available">  В наличии</span>
                            @else
                                <label>Товар:</label>
                                <span class="text-danger">  Нет в наличии</span>
                            @endif
                        </div>
                        <div class="title"><a href="#">{{$products->name}}</a></div>
                        <div class="excerpt">
                            @if($products->descriptions->count() > 0)
                                <p>{{$products->descriptions->first()->name}}</p>

                            @else
                            <p>{{$products->description}}</p>
                            @endif
                        </div>

                        <div class="prices">
                            <div class="price-current">{{$products->price}} TJS</div>
                            @if($products->old_price)
                                <div class="price-prev">{{$products->old_price}} TJS</div>
                            @endif
                        </div>

                        <div class="qnt-holder">
                            <form action="{{ route('cart.add.product') }}"
                                  method="POST">
                                @csrf
                                <input name="product_id" value="{{$products->id}}"
                                       type="hidden">
                                <input class="le-button" type="submit"
                                       value="В корзину">
                            </form>
                        </div><!-- /.qnt-holder -->
                    </div><!-- /.body -->

                </div><!-- /.body-holder -->
            </div><!-- /.container -->
        </div><!-- /.single-product -->

        <section id="single-product-tab">
            <div class="container">
                <div class="tab-holder">

                    <ul class="nav nav-tabs simple">
                        @if(Session::has('class'))
                        <li ><a href="#description" data-toggle="tab">Описание</a></li>
                        <li><a href="#additional-info" data-toggle="tab">Информация</a></li>
                        <li class="active"><a href="#reviews" data-toggle="tab">Комментарии ({{$comments->count()}})</a></li>
                        
                        @else
                        <li class="active"><a href="#description" data-toggle="tab">Описание</a></li>
                        <li><a href="#additional-info" data-toggle="tab">Информация</a></li>
                        <li><a href="#reviews" data-toggle="tab">Комментарии ({{$comments->count()}})</a></li>
                        @endif
                    </ul><!-- /.nav-tabs -->

                    <div class="tab-content">
                        <div class="tab-pane {{Session::has('class') ? '' : 'active'}} " id="description">
                            @if($products->descriptions->count() > 0)
                                <p>{{$products->descriptions->first()->name}}</p>

                            @else
                                <p>{{$products->description}}</p>
                            @endif
                        </div><!-- /.tab-pane #description -->

                        <div class="tab-pane" id="additional-info">
                            <ul class="tabled-data">
                                @if($details->count() > 0)
                                    @foreach($details as $detail)
                                        <li>
                                            <label>{{$detail->name}}</label>
                                            <div class="value">{{$detail->value}}</div>
                                        </li>
                            @endforeach
                            @endif
                        </div><!-- /.tab-pane #additional-info -->

                        <div class="tab-pane {{Session::has('class') ? 'active' : ''}}" id="reviews">
                            <div class="comments">
                                @if($comments->count() > 0)
                                    @foreach($comments as $comment)
                                        <div class="comment-item">
                                            <div class="row no-margin">
                                                <div class="col-lg-1 col-xs-12 col-sm-2 no-margin">
                                                    <div class="avatar">
                                                        <img alt="avatar" src="{{asset('images/default-avatar.jpg')}}">
                                                    </div><!-- /.avatar -->
                                                </div><!-- /.col -->

                                                <div class="col-xs-12 col-lg-11 col-sm-10 no-margin">
                                                    <div class="comment-body">
                                                        <div class="meta-info">
                                                            <div class="author inline">
                                                                <a href="#" class="bold">
                                                                    {{App\User::find($comment->user_id)->name}}
                                                                </a>
                                                            </div>
                                                            
                                                            {{-- @dd(App\User::find($comment->user_id)->product_marks()->get()) --}} 
                                                            @if($mark != null)
                                                            @foreach($mark as $m)
                                                             {{-- @dd(App\Marks::find($m->products_id)->rate ) --}}
                                                            

                                                            {{-- @if(App\User::find($m->user_id)->product_marks()->first()  != null)
                                                                @if(App\User::find($m->user_id)->product_marks()->first()->rate  == 0)
                                                                    <div class="star-holder inline">
                                                                        <img src="../../../images/star-off.png" alt="1"
                                                                             title="нет оценки">
                                                                        <img src="../../../images/star-off.png" alt="2"
                                                                             title="нет оценки">
                                                                        <img src="../../../images/star-off.png" alt="3"
                                                                             title="нет оценки">
                                                                        <img src="../../../images/star-off.png" alt="4"
                                                                             title="нет оценки">
                                                                        <img src="../../../images/star-off.png" alt="5"
                                                                             title="нет оценки">
                                                                    </div>

                                                                @elseif(App\User::find($m->user_id)->product_marks()->first()->rate  == 1)
                                                                    <div class="star-holder inline">
                                                                        <img src="../../../images/star-on.png" alt="1"
                                                                             title="ужасно">
                                                                        <img src="../../../images/star-off.png" alt="2"
                                                                             title="ужасно">
                                                                        <img src="../../../images/star-off.png" alt="3"
                                                                             title="ужасно">
                                                                        <img src="../../../images/star-off.png" alt="4"
                                                                             title="ужасно">
                                                                        <img src="../../../images/star-off.png" alt="5"
                                                                             title="ужасно">
                                                                    </div>

                                                                @elseif(App\User::find($m->user_id)->product_marks()->first()->rate  == 2)
                                                                    <div class="star-holder inline">
                                                                        <img src="../../../images/star-on.png" alt="1"
                                                                             title="плохо">
                                                                        <img src="../../../images/star-on.png" alt="2"
                                                                             title="плохо">
                                                                        <img src="../../../images/star-off.png" alt="3"
                                                                             title="плохо">
                                                                        <img src="../../../images/star-off.png" alt="4"
                                                                             title="плохо">
                                                                        <img src="../../../images/star-off.png" alt="5"
                                                                             title="плохо">
                                                                    </div>

                                                                @elseif(App\User::find($m->user_id)->product_marks()->first()->rate  == 3)
                                                                    <div class="star-holder inline">
                                                                        <img src="../../../images/star-on.png" alt="1"
                                                                             title="средне">
                                                                        <img src="../../../images/star-on.png" alt="2"
                                                                             title="средне">
                                                                        <img src="../../../images/star-on.png" alt="3"
                                                                             title="средне">
                                                                        <img src="../../../images/star-off.png" alt="4"
                                                                             title="средне">
                                                                        <img src="../../../images/star-off.png" alt="5"
                                                                             title="средне">
                                                                        <input type="hidden" name="score">
                                                                    </div>

                                                                @elseif(App\User::find($m->user_id)->product_marks()->first()->rate  == 4)
                                                                    <div class="star-holder inline">
                                                                        <img src="../../../images/star-on.png" alt="1"
                                                                             title="хорошо">
                                                                        <img src="../../../images/star-on.png" alt="2"
                                                                             title="хорошо">
                                                                        <img src="../../../images/star-on.png" alt="3"
                                                                             title="хорошо">
                                                                        <img src="../../../images/star-on.png" alt="4"
                                                                             title="хорошо">
                                                                        <img src="../../../images/star-off.png" alt="5"
                                                                             title="хорошо">
                                                                    </div>

                                                                @else(App\User::find($m->user_id)->product_marks()->first()->rate  == 5)
                                                                    <div class="star-holder inline">
                                                                        <img src="../../../images/star-on.png" alt="1"
                                                                             title="отлично">
                                                                        <img src="../../../images/star-on.png" alt="2"
                                                                             title="отлично">
                                                                        <img src="../../../images/star-on.png" alt="3"
                                                                             title="отлично">
                                                                        <img src="../../../images/star-on.png" alt="4"
                                                                             title="отлично">
                                                                        <img src="../../../images/star-on.png" alt="5"
                                                                             title="отлично">
                                                                    </div>
                                                                @endif
                                                            @endif --}}
                                                            @endforeach 
                                                            @endif
                                                            <div class="date inline pull-right">
                                                                {{App\Comments::find($comment->id)->created_at}}

                                                                @if(Auth::check() && Auth::user()->level()==2)
                                                                    <div>
                                                                        <a href="{{url('comment/delete/'.$comment->id)}}"
                                                                           class="btn btn-danger btn-sm">Удалить
                                                                        </a>
                                                                    </div>
                                                                @endif
                                                            </div>
                                                            
                                                        </div><!-- /.meta-info -->
                                                        <p class="comment-text">
                                                            {{$comment->name}}
                                                        </p><!-- /.comment-text -->
                                                    </div><!-- /.comment-body -->
                                                </div><!-- /.col -->
                                            </div><!-- /.row -->
                                        </div><!-- /.comment-item -->
                                    @endforeach

                                @endif

                            </div><!-- /.comments -->

                            @if(Auth::check())
                                <div class="add-review row">
                                    <div class="col-sm-8 col-xs-12">
                                        <div class="new-review-form">
                                            <h2>Добавить комментарий</h2>
                                            <form action="{{url('product/show/add/comment')}}" id="contact-form"
                                                class="contact-form" method="post">
                                                {{ csrf_field() }}
                                                <div class="field-row star-row">
                                                    <label>Ваша оценка</label>
                                                    <div class="star-holder">
                                                        <div class="star big" data-score="0"
                                                             style="cursor: pointer; width: 80px;">
                                                            <input type="hidden" name="score">
                                                            <input type="hidden" name="product_id"
                                                                   value="{{$products->id}}">
                                                            <input type="hidden" name="user_id" value="{{$user->id}}">

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="field-row">
                                                    <label>Ваш комментарий</label>
                                                    <textarea name="comment" rows="8" class="le-input"></textarea>
                                                    <input type="hidden" name="product_id" value="{{$products->id}}">
                                                    <input type="hidden" name="user_id" value="{{$user->id}}">
                                                </div><!-- /.field-row -->

                                                <div class="buttons-holder">
                                                    <button type="submit" class="le-button huge">Добавить</button>
                                                </div><!-- /.buttons-holder -->
                                            </form><!-- /.contact-form -->
                                        </div><!-- /.new-review-form -->
                                    </div><!-- /.col -->
                                </div><!-- /.add-review -->
                            @endif
                        </div><!-- /.tab-pane #reviews -->
                    </div><!-- /.tab-content -->
                </div><!-- /.tab-holder -->
            </div><!-- /.container -->
        </section><!-- /#single-product-tab -->
    @endif
@endsection