<header style="margin-top: 0;padding-top: 2%;">
    <div class="container no-padding">
        <div class="col-xs-12 col-sm-12 col-md-3 logo-holder">

            <a href="{{url('/')}}">
                <div class="logo"></div>

            </a>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 top-search-holder no-margin">
            <div class="contact-row">
                <div class="phone inline">
                    <i class="fa fa-phone"></i> Тел: 93 864 54 54

                </div>
                <div class="contact inline">
                    <i class="fa fa-envelope"></i> giftstj@<span class="le-color">gmail.com</span>
                </div>
            </div>
            <div class="search-area">
                <form method="GET" action="{{url('/search')}}">
                    <div class="control-group">
                        <input class="search-field" placeholder="Поиск" name="search" value="" required/>

                        <button class="search-button" type="submit"></button>
                        {{-- <a class="search-button " role="submit" ></a> --}}
                    </div>

                </form>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-3 top-cart-row no-margin" id="cart-mobile">
            <div class="top-cart-row-container">

                <div class="top-cart-holder dropdown animate-dropdown">
                    <div class="basket">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <div class="basket-item-count">
                                <span class="count">
                                   @if(isset($count_item))
                                        {{$count_item}}
                                    @else
                                        0
                                    @endif
                                </span>
                                <img src="{{asset('images/icon-cart.png')}}" alt=""/>
                            </div>
                            <div class="total-price-basket">
                                <span class="lbl">корзина:</span>
                                <span class="total-price">
                                    <span class="sign">TJS</span><span class="value">
                                        @if(isset($sum_of_items))
                                            {{$sum_of_items}}
                                        @else
                                            0
                                        @endif
                                    </span>
                                </span>
                            </div>
                        </a>
                        <ul class="dropdown-menu">
                            @if(isset($listOrders))
                                @if($listOrders)
                                    @foreach($listOrders as $order)
                                        <li>
                                            <div class="basket-item">
                                                <div class="row">
                                                    <div class="col-xs-4 col-sm-4 no-margin text-center">
                                                        <div class="thumb">
                                                            <img alt=""
                                                                 src="{{asset('images/products/'.\App\Products::find($order->products_id)->img)}}"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-8 col-sm-8 no-margin">
                                                        <div class="title">{{\App\Products::find($order->products_id)->name}}</div>
                                                        <div class="price">
                                                            TJS {{\App\Products::find($order->products_id)->price}}</div>
                                                    </div>
                                                </div>
                                                <a class="close-btn"
                                                   href="{{url('remove/cart-order/'.$order->id)}}"></a>
                                            </div>
                                        </li>

                                    @endforeach
                                    <li class="checkout">
                                        <div class="basket-item">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-4">
                                                    <a href="{{url('page-cart')}}" class="le-button inverse">Корзина</a>
                                                </div>
                                                <div class="col-xs-12 col-sm-8">
                                                    <a href="{{url('order/billing-page')}}" class="le-button">Оформить заказ</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                @endif
                            @endif
                        </ul>

                    </div>
                </div>
            </div>
        </div>

    </div>

</header>
<div class="container container-fluid">
    <div class="row">
        <div class="col-12">


        </div>
    </div>
</div>
