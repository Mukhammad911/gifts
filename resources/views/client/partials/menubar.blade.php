
    <!-- <div id="top-banner-and-menu"> -->
    <!-- <div class="container-fluid"> -->
        <div class="col-xs-12 col-sm-4 col-md-3 sidemenu-holder">
            <div class="side-menu animate-dropdown">
                <div class="head"><i class="fa fa-list"></i> Все категории</div>
                <nav class="yamm megamenu-horizontal" role="navigation">
                    <ul class="nav">
                        @foreach($categories as $cat)
                        <li class="dropdown menu-item">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{$cat->name}}</a>
                            <ul class="dropdown-menu mega-menu">
                                <li class="yamm-content">
                                    <div class="row">
                                        <div class="col-xs-12 col-lg-4">
                                            <ul>
                                            @foreach($cat->sub_categories as $sub_cat)
                                                <li><a href="{{$sub_cat->id}}">{{$sub_cat->name}}</a></li>
                                            @endforeach
                                            </ul>
                                        </div>

                                        <div class="col-xs-12 col-lg-4">
                                            <ul>
                                                <li><a href="#">Power Supplies Power</a></li>
                                            </ul>
                                        </div>

                                        <div class="dropdown-banner-holder">
                                            <a href="#"><img alt="" src="{{asset('images/banners/banner-side.png')}}" /></a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        @endforeach
                    </ul>
                </nav>
            </div>
        </div>
</div>
</div>