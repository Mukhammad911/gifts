@extends('client.partials.layouts')
@section('content')

@if($products)



<div class="animate-dropdown">
    <!-- ========================================= BREADCRUMB ========================================= -->
    <div id="top-mega-nav">
        <div class="container">
            <nav>
                <ul class="inline">
                    <li class="dropdown le-dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-list"></i> Товары по категориям
                        </a>

                        <ul class="dropdown-menu">
                        
                        @foreach(App\Sub_categories::all() as $sub_cat)
                            <li><a href="{{url('category/'.$sub_cat->id)}}">{{$sub_cat->name}}</a></li>
                        @endforeach    
                        </ul>
                    </li>

                    <li class="breadcrumb-nav-holder">
                        <ul>
                        <li class=" breadcrumb-item">
                            <a href="{{url('/')}}"  >Главная</a>
                            
                        </li><!-- /.breadcrumb-item -->

                        <li class=" breadcrumb-item current">
                            <a href="#" >
                                Скоро   
                            </a>
                        </li><!-- /.breadcrumb-item -->
                        
                    </li><!-- /.breadcrumb-nav-holder -->

                </ul><!-- /.inline -->
            </nav>

        </div><!-- /.container -->
    </div><!-- /#top-mega-nav -->
    <!-- ========================================= BREADCRUMB : END ========================================= -->
</div>


<section id="category-grid">
    <div class="container">
        <!-- ========================================= SIDEBAR ========================================= -->
        <!-- ========================================= SIDEBAR : END ========================================= -->

        <!-- ========================================= CONTENT ========================================= -->
        <div class="col-xs-12 col-sm-9 no-margin wide sidebar">
            <section id="gaming">
                <div class="grid-list-products">

                    <div class="tab-content">
                        <div id="grid-view" class="products-grid fade tab-pane in active">

                            <div class="product-grid-holder">
                                <div class="row no-margin">
                                <div class="container">
                                    @if($products)
                                    @foreach($products as $product)
                                    
                                    <div class="col-xs-12 col-sm-3 no-margin product-item-holder hover">
                                        <div class="product-item">
                                            @if($product->onsale)
                                                <div class="ribbon red"><span>Распродажа</span></div>
                                            @endif
                                            @if($product->new)
                                                <div class="ribbon blue"><span>Новинка</span></div>
                                            @endif
                                            @if($product->bestseller)
                                                <div class="ribbon green"><span>Топ продаж</span></div>
                                            @endif
                                            <a href="{{url('product/show/'.$product->id)}}">
                                            <div class="image">
                                                <img alt="{{$product->name}}" src="{{asset('images/blank.gif')}}" 
                                                    data-echo="{{asset('images/products/'.$product->img)}}" 
                                                    title="{{$product->name}}" width="246px" height="186px" />
                                            </div>
                                            </a>
                                            <div class="body">
                                                <div class="label-discount clear"></div>
                                                <div class="title">
                                                    @if($product->discount != "")
                                                        <div class="label-discount green">{{$product->discount}}% Акция</div>
                                                    @endif
                                                    <a href="{{url('product/show/'.$product->id)}}">{{$product->name}}</a>
                                                </div>
                                                @if($product->brand != "")
                                                    <div class="brand">{{$product->brand}}</div>
                                                @endif
                                                
                                            </div>
                                            <div class="prices">
                                                @if($product->old_price != "")
                                                    <div class="price-prev">{{$product->old_price}} TJS</div>
                                                @else
                                                    <div class="price-prev"></div>
                                                @endif
                                                <div class="price-current pull-right">{{$product->price}} TJS</div>
                                            </div>
                                            <div class="hover-area">
                                                <div class="add-cart-button">
                                                <form action="{{ route('cart.add.product') }}"
                                                        method="POST">
                                                    @csrf
                                                    <input name="product_id" value="{{$product->id}}"
                                                            type="hidden">
                                                    <input class="le-button" type="submit"
                                                            value="В корзину">
                                                </form>
                                                </div>
                                                {{-- <div class="wish-compare">
                                                    <a class="btn-add-to-wishlist" href="#">add to wishlist</a>
                                                    <a class="btn-add-to-compare" href="#">compare</a>
                                                </div> --}}
                                            </div>
                                        </div><!-- /.product-item -->
                                    </div><!-- /.product-item-holder -->
                                    
                                    @endforeach
                                    @endif
                                    </div>
                                </div><!-- /.row -->
                            </div><!-- /.product-grid-holder -->

                            <div class="pagination-holder">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 text-left">
                                    {{$products->links()}}   
                                    </div>
                                    
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="result-counter">
                                        
                                         Показано <span>{{$products->firstItem()}}-{{$products->lastItem()}}</span> из <span>{{$products->total()}} </span> результатов
                                        </div>
                                        
                                    </div>

                                </div><!-- /.row -->
                            </div><!-- /.pagination-holder -->
                        </div><!-- /.products-grid #grid-view -->

                        

                    </div><!-- /.tab-content -->
                </div><!-- /.grid-list-products -->

            </section>
        </div><!-- /.col -->
        <!-- ========================================= CONTENT : END ========================================= -->
    </div><!-- /.container -->
</section><!-- /#category-grid -->


@endif
@endsection

