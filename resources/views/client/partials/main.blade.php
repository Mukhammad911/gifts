@extends('client.partials.layouts')
@section('content')

    <div id="top-banner-and-menu">
        <div class="container">
            @if( Session::has('success_message'))
                <div class="alert alert-success" role="alert">
                    <p style="font-size:2rem;">{{$success_message=Session::get('success_message')}}</p>
                </div>
            @endif
            <div class="col-xs-12 col-sm-4 col-md-3 sidemenu-holder" >
                <div class="side-menu animate-dropdown " style="cursor: pointer;
    user-select: none;">
                    <div class="head" onclick="hideShow()"><i class="fa fa-list"></i> Все категории</div>
                    <div id="top-banner-and-menu" style="    position: relative;

    top: -1px;
    margin: auto;
    width: 100%;
    float: left;
    left: 0px;">
                        <div class="container" id="hide" style="    height: 477px;
    overflow: hidden scroll;
    margin: 0;
    padding: 0; ">
                            <div class="col-xs-12 col-sm-4 col-md-3 sidemenu-holder">
                                <div class="side-menu animate-dropdown">
                                    <nav class="yamm megamenu-horizontal" role="navigation">
                                        <ul class="nav">
                                            @foreach($categories as $cat)
                                                <li class="dropdown menu-item" style="    width: 97.5%;">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{$cat->name}}</a>
                                                    <ul class="dropdown-menu mega-menu">
                                                        <li class="yamm-content">
                                                            <div class="row">
                                                                <div class="col-2">
                                                                    @foreach($cat->sub_categories as $sub_cat)
                                                                        <div class="col-xs-12 col-lg-4">
                                                                            <ul>
                                                                                <li><a href="{{url('category/'.$sub_cat->id)}}">{{$sub_cat->name}}</a></li>
                                                                            </ul>
                                                                        </div>
                                                                    @endforeach
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-8 col-md-9 homebanner-holder">
                <div id="hero" class="">
                    <div id="owl-main" class="owl-carousel owl-inner-nav owl-ui-sm">
                        @foreach($banners_list as $list)
                            <div class="item" style="background-image: url({{asset('images/banners/'.$list->img)}});
                                    background-size: 100%;"
                                 id="banner-mobile">
                                <div class="container-fluid">
                                    <div class="caption vertical-center text-left">
                                        <div class="big-text fadeInDown-1 text-info">

                                        </div>

                                        <div class="excerpt fadeInDown-2">

                                        </div>

                                        <div class="button-holder fadeInDown-3">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div id="products-tab" class="wow fadeInUp">
            <div class="container">
                <div class="tab-holder">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#featured" data-toggle="tab">Новинки</a></li>
                        <li><a href="#new-arrivals" data-toggle="tab">Акции</a></li>
                        <li><a href="#top-sales" data-toggle="tab">Распродажа</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane active" id="featured">
                            <div class="product-grid-holder">
                                @foreach($news as $new)
                                    <div class="col-sm-4 col-md-3  no-margin product-item-holder hover blogBox moreBox" style="display: none;">
                                        <div class="product-item">
                                            @if($new->new)
                                                <div class="ribbon blue"><span>Новинка</span></div>
                                            @endif

                                            @if($new->feature)
                                                <div class="ribbon red"><span>Скоро</span></div>
                                            @endif
                                            @if($new->bestseller)
                                                <div class="ribbon green"><span>Топ продаж</span></div>
                                            @endif
                                            @if($new->onsale)
                                                <div class="ribbon red"><span>Распродажа</span></div>
                                            @endif

                                            <a href="{{url('product/show/'.$new->id)}}">
                                                <div class="image">
                                                    <img alt="{{$new->name}}" src="{{asset('assets/images/blank.gif')}}"
                                                         data-echo="{{asset('images/products/'.$new->img)}}"
                                                         title="{{$new->name}}" class="img-resize" />
                                                </div>
                                            </a>
                                            <div class="body">
                                                @if($new->discount)
                                                    <div class="label-discount green">{{$new->discount}}% Акция</div>
                                                @endif
                                                <div class="label-discount clear"></div>
                                                <div class="title">
                                                    <a href="{{url('product/show/'.$new->id)}}">{{$new->name}}</a>
                                                </div>
                                                <div class="brand">{{$new->brand}}</div>
                                            </div>
                                            <div class="prices">
                                                <div class="price-prev"></div>
                                                <div class="price-current pull-right">{{$new->price}}TJS</div>
                                            </div>

                                            <div class="hover-area">
                                                <div class="add-cart-button">
                                                    <form action="{{ route('cart.add.product') }}"
                                                          method="POST">
                                                        @csrf
                                                        <input name="product_id" value="{{$new->id}}"
                                                               type="hidden">
                                                        <input class="le-button" type="submit"
                                                               value="В корзину">
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                @if($news->count() > 4)
                                    <div id="loadMore" style="" class="loadmore-holder text-center">
                                        <a  href="#" class="btn-loadmore">
                                            <i class="fa fa-plus"></i>
                                            Загрузить больше
                                        </a>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="tab-pane blogBox " id="new-arrivals">
                            <div class="product-grid-holder">
                                @foreach($discount as $dis)

                                    <div class="col-sm-4 col-md-3 no-margin product-item-holder hover blogBox1 moreBox1" style="display: none;">
                                        <div class="product-item">
                                            @if($dis->new)
                                                <div class="ribbon blue"><span>Новинка</span></div>
                                            @endif
                                            @if($dis->feature)
                                                <div class="ribbon red"><span>Скоро</span></div>
                                            @endif
                                            @if($dis->bestseller)
                                                <div class="ribbon green"><span>Топ продаж</span></div>
                                            @endif
                                            @if($dis->onsale)
                                                <div class="ribbon red"><span>Распродажа</span></div>
                                            @endif

                                            <a href="{{url('product/show/'.$dis->id)}}">
                                                <div class="image">
                                                    <img alt="{{$dis->name}}" src="{{asset('assets/images/blank.gif')}}"
                                                         data-echo="{{asset('images/products/'.$dis->img)}}"
                                                         title="{{$dis->name}}" class="img-resize" />
                                                </div>
                                            </a>
                                            <div class="body">
                                                @if($dis->discount)
                                                    <div class="label-discount green">{{$dis->discount}}% Акция</div>
                                                @endif
                                                <div class="label-discount clear"></div>
                                                <div class="title">
                                                    <a href="{{url('product/show/'.$dis->id)}}">{{$dis->name}}</a>
                                                </div>
                                                <div class="brand">{{$dis->brand}}</div>
                                            </div>
                                            <div class="prices">

                                                <div class="price-prev">
                                                    @if($dis->old_price)
                                                        {{$dis->old_price}} TJS
                                                    @endif
                                                </div>
                                                <div class="price-current pull-right">{{$dis->price}} TJS</div>
                                            </div>
                                            <div class="hover-area">
                                                <div class="add-cart-button">
                                                    <form action="{{ route('cart.add.product') }}"
                                                          method="POST">
                                                        @csrf
                                                        <input name="product_id" value="{{$dis->id}}"
                                                               type="hidden">
                                                        <input class="le-button" type="submit"
                                                               value="В корзину">
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                <br>
                                @if($discount->count() > 4)
                                    <div id="loadMore1" style="" class="loadmore-holder text-center">
                                        <a  href="#" class="btn-loadmore">
                                            <i class="fa fa-plus"></i>
                                            Загрузить больше
                                        </a>
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="tab-pane" id="top-sales">
                            <div class="product-grid-holder">
                                @foreach($onsales as $onsale)
                                    <div class="col-sm-4 col-md-3 no-margin product-item-holder hover blogBox2 moreBox2" style="display:none;">
                                        <div class="product-item">
                                            @if($onsale->onsale)
                                                <div class="ribbon red"><span>Распродажа</span></div>
                                            @endif
                                            @if($onsale->new)
                                                <div class="ribbon blue"><span>Новинка</span></div>
                                            @endif
                                            @if($onsale->feature)
                                                <div class="ribbon red"><span>Скоро</span></div>
                                            @endif
                                            @if($onsale->bestseller)
                                                <div class="ribbon green"><span>Топ продаж</span></div>
                                            @endif
                                            <a href="{{url('product/show/'.$onsale->id)}}">
                                                <div class="image">
                                                    <img alt="{{$onsale->name}}" src="{{asset('assets/images/blank.gif')}}"
                                                         data-echo="{{asset('images/products/'.$onsale->img)}}"
                                                         title="{{$onsale->name}}" class="img-resize" />
                                                </div>
                                            </a>
                                            <div class="body">
                                                @if($onsale->discount)
                                                    <div class="label-discount green">{{$onsale->discount}}% Акция</div>
                                                @endif
                                                <div class="label-discount clear"></div>
                                                <div class="title">
                                                    <a href="{{url('product/show/'.$onsale->id)}}">{{$onsale->name}}</a>
                                                </div>
                                                <div class="brand">{{$onsale->brand}}</div>
                                            </div>
                                            <div class="prices">
                                                <div class="price-prev">
                                                    @if($onsale->old_price)
                                                        {{$onsale->old_price}} TJS
                                                    @endif
                                                </div>
                                                <div class="price-current pull-right">{{$onsale->price}} TJS</div>
                                            </div>
                                            <div class="hover-area">
                                                <div class="add-cart-button">
                                                    <form action="{{ route('cart.add.product') }}"
                                                          method="POST">
                                                        @csrf
                                                        <input name="product_id" value="{{$onsale->id}}"
                                                               type="hidden">
                                                        <input class="le-button" type="submit"
                                                               value="В корзину">
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                @if($onsales->count() > 4)
                                    <div id="loadMore2" style="" class="loadmore-holder text-center">
                                        <a  href="#" class="btn-loadmore">
                                            <i class="fa fa-plus"></i>
                                            Загрузить больше
                                        </a>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- ========================================= BEST SELLERS ========================================= -->


        @if($tops->count() > 0 )
            <section id="recently-reviewd" class="wow fadeInUp mobile_hide">
                <div class="container">
                    <div class="carousel-holder hover">

                        <div class="title-nav">
                            <h2 class="h1">Топ продаж</h2>
                            <div class="nav-holder">
                                <a href="#prev" data-target="#owl-recently-viewed" class="slider-prev btn-prev fa fa-angle-left"></a>
                                <a href="#next" data-target="#owl-recently-viewed" class="slider-next btn-next fa fa-angle-right"></a>
                            </div>
                        </div><!-- /.title-nav -->

                        <div id="owl-recently-viewed" class="owl-carousel product-grid-holder">
                            @foreach($tops as $top)
                                <div class="no-margin carousel-item product-item-holder size-small hover">
                                    <div class="product-item">
                                        <div class="ribbon green"><span>Топ продаж</span></div>
                                        @if($top->onsale)
                                            <div class="ribbon red"><span>Распродажа</span></div>
                                        @endif
                                        @if($top->new)
                                            <div class="ribbon blue"><span>Новинка</span></div>
                                        @endif
                                        @if($top->feature)
                                            <div class="ribbon red"><span>Скоро</span></div>
                                        @endif

                                        <a href="{{url('product/show/'.$top->id)}}">
                                            <div class="image">
                                                <img alt="" src="assets/images/blank.gif"
                                                     data-echo="{{asset('images/products/'.$top->img)}}"
                                                     width="" height="109" style="padding: 10px;" />
                                            </div>
                                        </a>
                                        <div class="body">
                                            <div class="title">
                                                <a href="{{url('product/show/'.$top->id)}}">{{$top->name}}</a>
                                            </div>
                                            @if($top->brand)
                                                <div class="brand">{{$top->brand}}</div>
                                            @else
                                                <div class="brand"></div>
                                            @endif
                                        </div>
                                        <div class="prices">
                                            <div class="price-current text-right">{{$top->price}} TJS</div>
                                        </div>
                                        <div class="hover-area">
                                            <div class="add-cart-button">
                                                <form action="{{ route('cart.add.product') }}"
                                                      method="POST">
                                                    @csrf
                                                    <input name="product_id" value="{{$top->id}}"
                                                           type="hidden">
                                                    <input class="le-button" type="submit"
                                                           value="В корзину">
                                                </form>
                                            </div>
                                        </div>
                                    </div><!-- /.product-item -->
                                </div><!-- /.product-item-holder -->
                            @endforeach
                        </div><!-- /#recently-carousel -->
                    </div><!-- /.carousel-holder -->
                </div><!-- /.container -->
            </section><!-- /#recently-reviewd -->
        @endif





        @if($data != null )
            <section id="bestsellers" class="color-bg wow fadeInUp">
                <div class="container">
                    <h1 class="section-title">Вы недавно смотрели</h1>

                    <div class="product-grid-holder medium">
                        <div class="col-xs-12 col-md-12 no-margin">

                            <div class="row no-margin">
                                @foreach($data as $recent)
                                    @foreach(App\Products::where('id', $recent)->get() as $view)
                                        <div class="col-xs-12 col-sm-2 no-margin product-item-holder  hover" style="min-height: 300px">
                                            <div class="product-item">
                                                <a href="{{url('product/show/'.$view->id)}}">
                                                    <div class="image recently-image">
                                                        <img alt="" src="assets/images/blank.gif" class="img-resize"
                                                             data-echo="{{asset('images/products/'.$view->img)}}"  />
                                                    </div>
                                                </a>
                                                <div class="body">
                                                    <div class="label-discount clear"></div>
                                                    <div class="title">
                                                        <a href="{{url('product/show/'.$view->id)}}">{{$view->name}}</a>
                                                    </div>
                                                    @if($view->brand)
                                                        <div class="brand">{{$view->brand}}</div>
                                                    @else
                                                        <div class="brand"></div>
                                                    @endif
                                                </div>
                                                <div class="prices">

                                                    <div class="price-current text-right">{{$view->price}}</div>
                                                </div>
                                                <div class="hover-area">
                                                    <div class="add-cart-button">
                                                        <form action="{{ route('cart.add.product') }}"
                                                              method="POST">
                                                            @csrf
                                                            <input name="product_id" value="{{$view->id}}"
                                                                   type="hidden">
                                                            <input class="le-button" type="submit"
                                                                   value="В корзину">
                                                        </form>
                                                    </div>

                                                </div>
                                            </div>
                                        </div><!-- /.product-item-holder -->
                                    @endforeach
                                @endforeach
                            </div><!-- /.row -->
                        </div><!-- /.col -->
                    </div><!-- /.product-grid-holder -->
                </div><!-- /.container -->
            </section><!-- /#bestsellers -->
        @endif
    </div>
    <!-- ========================================= RECENTLY VIEWED ========================================= -->

    <!-- ========================================= RECENTLY VIEWED : END ========================================= -->

    <!-- ========================================= TOP BRANDS ========================================= -->
    @if($brands_list->count() > 0)
        <section id="top-brands" class="wow fadeInUp">
            <div class="container">
                <div class="carousel-holder">

                    <div class="title-nav">
                        <h1>Топ Бренды</h1>
                        <div class="nav-holder">
                            <a href="#prev" data-target="#owl-brands" class="slider-prev btn-prev fa fa-angle-left"></a>
                            <a href="#next" data-target="#owl-brands"
                               class="slider-next btn-next fa fa-angle-right"></a>
                        </div>
                    </div><!-- /.title-nav -->

                    <div id="owl-brands" class="owl-carousel brands-carousel">
                        @foreach($brands_list as $list)
                            <div class="carousel-item">
                                <a href="#">
                                    <img alt="{{$list->name}}"
                                         src="{{asset('images/brands/'.$list->img)}}"
                                         height="35px" title="{{$list->name}}" />
                                </a>
                            </div><!-- /.carousel-item -->
                        @endforeach

                    </div><!-- /.brands-caresoul -->

                </div><!-- /.carousel-holder -->
            </div><!-- /.container -->
        </section><!-- /#top-brands -->
        <!-- ========================================= TOP BRANDS : END ========================================= -->
    @endif

    <script>
        $( document ).ready(function () {
            $(".moreBox").slice(0, 4).show();
            if ($(".blogBox:hidden").length != 0) {
                $("#loadMore").show();
            }
            $("#loadMore").on('click', function (e) {
                e.preventDefault();
                $(".moreBox:hidden").slice(0, 4).slideDown();
                if ($(".moreBox:hidden").length == 0) {
                    $("#loadMore").fadeOut('slow');
                }
            });
        });

        $( document ).ready(function () {
            $(".moreBox1").slice(0, 4).show();
            if ($(".blogBox1:hidden").length != 0) {
                $("#loadMore1").show();
            }
            $("#loadMore1").on('click', function (e) {
                e.preventDefault();
                $(".moreBox1:hidden").slice(0, 4).slideDown();
                if ($(".moreBox1:hidden").length == 0) {
                    $("#loadMore1").fadeOut('slow');
                }
            });
        });

        $( document ).ready(function () {
            $(".moreBox2").slice(0, 4).show();
            if ($(".blogBox2:hidden").length != 0) {
                $("#loadMore2").show();
            }
            $("#loadMore2").on('click', function (e) {
                e.preventDefault();
                $(".moreBox2:hidden").slice(0, 4).slideDown();
                if ($(".moreBox2:hidden").length == 0) {
                    $("#loadMore2").fadeOut('slow');
                }
            });
        });

    </script>
    <script>
        var visible = false;
        function hideShow() {

            if(visible) {

                document.getElementById('hide' ).style.display = 'none';
                visible = false;

            } else {
                document.getElementById('hide' ).style.display = 'block';
                visible = true;
            }
        }

    </script>

@endsection