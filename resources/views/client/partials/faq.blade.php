@extends('client.partials.layouts')
@section('content')

    <div class="animate-dropdown">
        <!-- ========================================= BREADCRUMB ========================================= -->
        <div id="top-mega-nav">
            <div class="container">
                <nav>
                    <ul class="inline">
                        <li class="dropdown le-dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-list"></i> Товары по категориям
                            </a>

                            <ul class="dropdown-menu">

                                @foreach(App\Sub_categories::all() as $sub_cat)
                                    <li><a href="{{url('category/'.$sub_cat->id)}}">{{$sub_cat->name}}</a></li>
                                @endforeach
                            </ul>
                        </li>

                        <li class="breadcrumb-nav-holder">
                            <ul>
                                <li class=" breadcrumb-item">
                                    <a href="{{url('/')}}">Главная</a>

                                </li><!-- /.breadcrumb-item -->

                                <li class=" breadcrumb-item current">
                                    <a href="#">
                                        ЧаВо
                                    </a>
                                </li><!-- /.breadcrumb-item -->

                            </ul>
                        </li>
                    </ul><!-- /.inline -->
                </nav>

            </div><!-- /.container -->
        </div><!-- /#top-mega-nav -->
        <!-- ========================================= BREADCRUMB : END ========================================= -->
    </div>


    <main id="faq" class="inner-bottom-md">
        <div class="container">

            <section class="section">

                <div class="page-header">
                    <h2 class="page-title">Часто задаваемые вопросы</h2>
                    <p class="page-subtitle">Обновлено 05.08.2019</p>
                </div>
                {{--<div id="questions" class="panel-group panel-group-faq">
                    <div class="panel panel-faq">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#questions" href="#collapseOne"
                                   aria-expanded="false" class="collapsed">
                                    Как купить товар и оформить заказ на сайте?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false"
                             style="height: 0px;">
                            <div class="panel-body">
                                Это очень просто. Вы можете просмотреть каталог
                                товаров и перенести выбранный вами товар в корзину.
                                Затем, вам нужно перейти в корзину и указать контактные
                                данные, выбрать метод оплаты и доставки После
                                ввода данных карты, вы подтверждаете заказ и оплату.
                                При подтверждении заказа, вы можете ознакомится с
                                условиями покупки товара. После оплаты заказа,
                                вам на почту или СМС-оповещением придет электронный чек.
                            </div>
                        </div>
                    </div><!-- /.panel-faq -->

                    <div class="panel panel-faq">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#questions" class="collapsed" href="#collapseTwo"
                                   aria-expanded="false">
                                    Требуется ли регистрация на сайте для оформления заказа?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" aria-expanded="false">
                            <div class="panel-body">
                                Нет не требуется. Вы можете зарегистрироваться по желанию
                                для получения новостей и акций от Фарад магазина.
                            </div>
                        </div>
                    </div><!-- /.panel-faq -->

                    <div class="panel panel-faq">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#questions" class="collapsed"
                                   href="#collapseFour" aria-expanded="false">
                                    Какая сумма минимального заказа?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse" aria-expanded="false"
                             style="height: 0px;">
                            <div class="panel-body">
                                Минимальный заказ – 100 сомони.
                            </div>
                        </div>
                    </div><!-- /.panel-faq -->

                    <div class="panel panel-faq">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#questions" class="collapsed"
                                   href="#collapseFive" aria-expanded="false">
                                    Могу ли я вернуть купленный товар?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseFive" class="panel-collapse collapse" aria-expanded="false">
                            <div class="panel-body">
                                Товар можно вернуть для обмена на другой товар.
                                Обмен товара можно оформить в течение 10 дней при
                                соблюдении следующих обязательный условий:
                                <br/>

                                <span style="margin-left:30px;"> &#10004;	товар не был в употреблении </span> <br/>
                                <span style="margin-left:30px;"> &#10004;	товар не имеет физических повреждений </span>
                                <br/>
                                <span style="margin-left:30px;"> &#10004;	сохранена оригинальная упаковка товара </span>
                                <br/>
                                <span style="margin-left:30px;"> &#10004;	товарный чек и прочие документы в наличии. </span>
                                <br/><br/>

                                Товар нужно вернуть для обмена в указанный пункт
                                выдачи товаров или же в торговых точках наших партнёров.

                            </div>
                        </div>
                    </div><!-- /.panel-faq -->

                    <div class="panel panel-faq">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#questions" class="collapsed" href="#collapseSix"
                                   aria-expanded="false">
                                    Кому обратится с предложением, жалобами и вопросами?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseSix" class="panel-collapse collapse" aria-expanded="false">
                            <div class="panel-body">
                                Вы можете обратиться к нашему онлайн консультанту или же написать
                                на адрес электронной почты faradshophelp@gmail.com или же позвонив
                                по номеру +992 93 583 21 33
                            </div>
                        </div>
                    </div><!-- /.panel-faq -->

                    <div class="panel panel-faq">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#questions" class="collapsed"
                                   href="#collapseSeven" aria-expanded="false">
                                    Что нам делать если не получили товар в течение оговоренного срока?
                                </a>
                            </h4>
                        </div>
                        <div id="collapseSeven" class="panel-collapse collapse" aria-expanded="false">
                            <div class="panel-body">
                                Если по какой-то причине товар не был доставлен,
                                вы можете обратиться к нам в любое удобное для
                                вас время. Мы гарантируем быстрое решение подобных
                                вопросов. В противном случае, вы имеете право
                                обратиться в Первый Микрофинансовый Банк для
                                возврата суммы купленного товара. Банк, после
                                проверки документов и фактов не доставления
                                товара, возвращает вам сумму купленного товара
                                в рамках банковских правил и процедур.
                            </div>
                        </div>
                    </div><!-- /.panel-faq -->
                </div><!-- /.panel-group -->--}}
            </section><!-- /.section -->

        </div><!-- /.container -->
    </main>


@endsection