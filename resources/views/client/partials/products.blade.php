@extends('client.partials.layouts')
@section('content')
    <div class="animate-dropdown">
        <!-- ========================================= BREADCRUMB ========================================= -->
        <div id="top-mega-nav">
            <div class="container">
                <nav>
                    <ul class="inline">
                        <li class="dropdown le-dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-list"></i> Товары по категориям
                            </a>

                            <ul class="dropdown-menu">

                                @foreach(App\Sub_categories::all() as $sub_cat)
                                    <li><a href="{{url('category/'.$sub_cat->id)}}">{{$sub_cat->name}}</a></li>
                                @endforeach
                            </ul>
                        </li>

                        <li class="breadcrumb-nav-holder">
                            <ul>
                                <li class=" breadcrumb-item">
                                    <a href="{{url('/')}}">Главная</a>

                                </li><!-- /.breadcrumb-item -->

                                <li class="dropdown breadcrumb-item">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        {{@App\Categories::find($sub_categories->categories_id)->name}}
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            @foreach(@App\Sub_categories::where('categories_id', '=', App\Categories::find($sub_categories->categories_id)->id)->get() as $sub_cat)
                                                <a href="{{url('category/'.$sub_cat->id)}}">{{$sub_cat->name}}</a>
                                            @endforeach
                                        </li>
                                    </ul>
                                </li><!-- /.breadcrumb-item -->

                                <li class="dropdown breadcrumb-item current">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        {{App\Sub_categories::find($sub_categories->id)->name}}
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            {{-- App\Products::find($product->id)->descriptions()->first()->name --}}
                                            @foreach(App\Sub_categories::find($sub_categories->id)->products()->get() as $product)
                                                <a href="{{url('product/show/'.$product->id)}}">{{$product->name}}</a>
                                            @endforeach
                                        </li>
                                    </ul>
                                </li><!-- /.breadcrumb-item -->

                            </ul>

                    </ul><!-- /.inline -->
                </nav>

            </div><!-- /.container -->
        </div><!-- /#top-mega-nav -->
        <!-- ========================================= BREADCRUMB : END ========================================= -->
    </div>

    <section id="category-grid">
        <div class="container">
            <!-- ========================================= SIDEBAR ========================================= -->
            <div class="col-xs-12 col-sm-3 no-margin sidebar narrow">
                <!-- ========================================= PRODUCT FILTER ========================================= -->
                <div class="widget" id="widget">
                    <h1>Фильтр</h1>
                    <span><h3 onclick="filterClose()" id="filter_close_button">Закрыть Фильтр</h3></span>
                    <form action="{{route('filter')}}" method="post">
                            {{csrf_field()}}
                    <input type="hidden" name="sub_category_id" value="{{$sub_categories->id}}">
                    <input type="hidden" name="sub_id" value="{{$sub_categories->id}}">

                        <div class="body bordered">
                            @if(isset($max_min))
                                @if($max_min)
                                    <div class="price-filter">
                                        <h2>Цена</h2>
                                        <hr>
                                        <div class="price-range-holder">

                                            <input type="text" class="price-slider" value="">

                                            

                                            <input type="hidden" name="min_set_value" id="min_set_value" value="{{$max_min['min']}}">
                                            <input type="hidden" name="max_set_value" id="max_set_value" value="{{$max_min['max']}}">

                                            <span class="filter-button">
                                                <a href="javascript:void(0)">
                                                <button type="submit" style="background: none; border: none">Фильтр</button></a>
                                            </span>
                                        </div>
                                    </div><!-- /.price-filter -->
                                @endif
                            @endif
                    </div><!-- /.body -->
                    @if(isset($filterDetails))
                        @if(!empty($filterDetails))
                            @foreach($filterDetails as $key => $value)
                            <div class="body bordered">
                                    <div class="category-filter">
                                        <h2>{{$key}}</h2>
                                        <input name="key[]" type="hidden" value="{{$key}}">
                                        <hr>
                                        <ul>
                                            <li>
                                                @foreach($value as $r)
                                                    @foreach($r as $row)
                                                        <input  name="value[{{$key}}][]"
                                                            class="le-checkbox" type="checkbox" value="{{$row->value}}"/>
                                                        <label>{{$row->value}}</label>
                                                        <span
                                                            class="pull-right">(
                                                            @if(isset($count_filter))
                                                                    {{count($count_filter[$row->value])}}
                                                            @endif)
                                                        </span><br>
                                                    @endforeach
                                                @endforeach
                                            </li>
                                        </ul>
                                    </div><!-- /.category-filter -->
                                    <div class="price-range-holder">
                                        <span class="filter-button">
                                            <a href="" class="pull-right">
                                                <button type="submit" style="background: none; border: none">Фильтр</button>
                                            </a>
                                        </span>
                                    </div>
                            </div>
                            @endforeach
                            @endif
                        @endif
                    </form>
                </div><!-- /.widget -->
                <!-- ========================================= PRODUCT FILTER : END ========================================= -->

            </div>
            <!-- ========================================= SIDEBAR : END ========================================= -->

            <!-- ========================================= CONTENT ========================================= -->
            <div class="col-xs-12 col-sm-9 no-margin wide sidebar">
                <section id="gaming">
                    <div class="grid-list-products">
                        <h2 class="section-title">{{$sub_categories->name}}</h2>
    
                        <div class="grid-list-buttons">
                            <ul>
                                <li id="filterOpen" class="grid-list-button-item" onclick="filterOpen()" style="float: left;"><a><i
                                                class="fa fa-filter"></i> Фильтр</a></li>
                                <li class="grid-list-button-item active"><a data-toggle="tab" href="#grid-view"><i
                                                class="fa fa-th-large"></i> Сетка</a></li>
                                <li class="grid-list-button-item "><a data-toggle="tab" href="#list-view"><i
                                                class="fa fa-th-list"></i> Список</a></li>
                            </ul>
                        </div>
                        
                        <br/>

                        <div class="tab-content">
                            <div id="grid-view" class="products-grid fade tab-pane in active">

                                <div class="product-grid-holder">
                                    <div class="row no-margin">
                                    
                                        @foreach($products as $product)
                                            <div class="col-xs-12 col-sm-4 no-margin product-item-holder hover">
                                                <div class="product-item">

                                                    @if($product->feature)
                                                        <div class="ribbon red"><span>Скоро</span></div>
                                                    @endif

                                                    @if($product->onsale)
                                                        <div class="ribbon red"><span>Распродажа</span></div>
                                                    @endif
                                                    @if($product->new)
                                                        <div class="ribbon blue"><span>Новинка</span></div>
                                                    @endif
                                                    @if($product->bestseller)
                                                        <div class="ribbon green"><span>Топ продаж</span></div>
                                                    @endif
                                                    <a href="{{url('product/show/'.$product->id)}}">
                                                    <div class="image img-list">
                                                        <img alt="" src="{{asset('images/blank.gif')}}"
                                                             data-echo="{{asset('images/products/'.$product->img)}}" 
                                                             width="" height="" class="img-resize" />
                                                    </div>
                                                    </a>
                                                    <div class="body">
                                                            @if($product->discount != "")
                                                                <div class="label-discount green">{{$product->discount}}% Акция</div>
                                                            @else
                                                                <div class="label-discount clear"></div>
                                                            @endif
                                                        
                                                        <div class="title">
                                                            
                                                            <a href="{{url('product/show/'.$product->id)}}">{{$product->name}}</a>
                                                        </div>
                                                        @if($product->brand != "")
                                                            <div class="brand">{{$product->brand}}</div>
                                                        @endif

                                                    </div>
                                                    <div class="prices">
                                                        @if($product->old_price != "")
                                                            <div class="price-prev">{{$product->old_price}}TJS</div>
                                                        @else
                                                            <div class="price-prev"></div>
                                                        @endif
                                                        <div class="price-current pull-right">{{$product->price}}TJS</div>
                                                    </div>
                                                    <div class="hover-area">
                                                        <div class="add-cart-button">
                                                            <form action="{{ route('cart.add.product') }}"
                                                                  method="POST">
                                                                @csrf
                                                                <input name="product_id" value="{{$product->id}}"
                                                                       type="hidden">
                                                                <input class="le-button" type="submit"
                                                                       value="В корзину">
                                                            </form>
                                                        </div>
                                                        {{-- <div class="wish-compare">
                                                            <a class="btn-add-to-wishlist" href="#">add to wishlist</a>
                                                            <a class="btn-add-to-compare" href="#">compare</a>
                                                        </div> --}}
                                                    </div>
                                                </div><!-- /.product-item -->
                                            </div><!-- /.product-item-holder -->
                                        @endforeach
                                    </div><!-- /.row -->
                                </div><!-- /.product-grid-holder -->

                                <div class="pagination-holder">
                                    <div class="row">

                                        <div class="col-xs-12 col-sm-6 text-left">
                                            {{$products->links()}}
                                        </div>

                                        <div class="col-xs-12 col-sm-6">
                                            <div class="result-counter">
                                                Показано <span>{{$products->firstItem()}}
                                                    -{{$products->lastItem()}}</span> из
                                                <span>{{$products->total()}}</span> результатов
                                            </div>
                                        </div>

                                    </div><!-- /.row -->
                                </div><!-- /.pagination-holder -->
                            </div><!-- /.products-grid #grid-view -->

                            <div id="list-view" class="products-grid fade tab-pane ">
                                <div class="products-list">
                                    @foreach($products as $product)
                                        <div class="product-item product-item-holder">
                                            @if($product->feature)
                                                <div class="ribbon red"><span>Скоро</span></div>
                                            @endif

                                            @if($product->onsale)
                                                <div class="ribbon red"><span>Распродажа</span></div>
                                            @endif
                                            @if($product->new)
                                                <div class="ribbon blue"><span>Новый</span></div>
                                            @endif
                                            @if($product->bestseller)
                                                <div class="ribbon green"><span>Топ продаж</span></div>
                                            @endif
                                            <div class="row">
                                                <div class="no-margin col-xs-12 col-sm-4 image-holder">
                                                    <div class="image">
                                                    <a href="{{url('product/show/'.$product->id)}}">
                                                        <img alt="{{$product->name}}" src="{{asset('images/blank.gif')}}"
                                                             data-echo="{{asset('images/products/'.$product->img)}}"
                                                             title="{{$product->name}}" />
                                                        </a>
                                                            </div>
                                                </div><!-- /.image-holder -->
                                                <div class="no-margin col-xs-12 col-sm-5 body-holder">
                                                    <div class="body">
                                                        @if($product->discount != "")
                                                            <div class="label-discount green">{{$product->discount}}%
                                                                Акция
                                                            </div>
                                                        @endif
                                                        <div class="title">
                                                            <a href="{{url('product/show/'.$product->id)}}">{{$product->name}}</a>
                                                        </div>
                                                        @if($product->brand != "")
                                                            <div class="brand">{{$product->brand}}</div>
                                                        @endif
                                                        <div class="excerpt">
                                                            @if($product->descriptions->count() > 0)
                                                                <p>{{$product->descriptions->first()->name}}</p>
                                                            @else
                                                                <p>{{$product->description}}</p>
                                                            @endif


                                                        </div>
                                                        {{--<div class="addto-compare">
                                                            <a class="btn-add-to-compare" href="#">add to compare list</a>
                                                        </div> --}}
                                                    </div>
                                                </div><!-- /.body-holder -->
                                                <div class="no-margin col-xs-12 col-sm-3 price-area">
                                                    <div class="right-clmn">
                                                        <div class="price-current">{{$product->price}} TJS</div>
                                                        @if($product->old_price != "")
                                                            <div class="price-prev">{{$product->old_price}} TJS</div>
                                                        @else
                                                            <div class="price-prev"></div>
                                                        @endif
                                                        @if($product->available)
                                                            <div class="availability"><label>Доступен:</label><span
                                                                        class="available">  В наличии</span></div>
                                                        @else
                                                            <div class="availability"><label>Доступен:</label><span
                                                                        class="text-danger">  Нет в наличии</span></div>
                                                        @endif
                                                        <form action="{{ route('cart.add.product') }}"
                                                                  method="POST">
                                                                @csrf
                                                                <input name="product_id" value="{{$product->id}}"
                                                                       type="hidden">
                                                                <input class="le-button" type="submit"
                                                                       value="В корзину">
                                                            </form>
                                                        {{-- <a class="btn-add-to-wishlist" href="#">add to wishlist</a> --}}
                                                    </div>
                                                </div><!-- /.price-area -->
                                            </div><!-- /.row -->
                                        </div><!-- /.product-item -->
                                    @endforeach
                                </div><!-- /.products-list -->

                                <div class="pagination-holder">
                                    <div class="row">

                                        <div class="col-xs-12 col-sm-6 text-left">
                                            {{$products->links()}}
                                        </div>

                                        <div class="col-xs-12 col-sm-6">
                                            <div class="result-counter">
                                                Показано <span>{{$products->firstItem()}}
                                                    -{{$products->lastItem()}}</span> из
                                                <span>{{$products->total()}}</span> результатов
                                            </div><!-- /.result-counter -->
                                        </div>

                                    </div><!-- /.row -->
                                </div><!-- /.pagination-holder -->

                            </div><!-- /.products-grid #list-view -->

                        </div><!-- /.tab-content -->
                    </div><!-- /.grid-list-products -->

                </section>
            </div><!-- /.col -->
            <!-- ========================================= CONTENT : END ========================================= -->
        </div><!-- /.container -->
    </section><!-- /#category-grid -->
    <script src="/js/jquery-1.10.2.min.js"></script>
    <script src="/js/bootstrap-slider.min.js"></script>

    <script type="text/javascript">


    if ($('.price-slider').length > 0) {
        $('.price-slider').slider({
            min: {!! $max_min['min'] !!},
            max: {!! $max_min['max'] + 3 !!},
            step: 1,
            value: [{!! $max_min['min'] !!}, {!! $max_min['max'] + 3 !!}],
            handle: "square"
        }).on('slideStop', changeSliderRange);
    }

    function changeSliderRange(newMin, newMax){

        var min = newMin.value[0];
        var max = newMin.value[1];

        $('#min_set_value').val(min);
        $('#max_set_value').val(max);

    }
    function filterOpen() {
            document.getElementById("widget").style.display = "block";
    }
    function filterClose() {
        document.getElementById("widget").style.display = "none";
    }
</script>
@endsection
