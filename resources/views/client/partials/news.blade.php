@extends('client.partials.layouts')
@section('content')

    <div class="animate-dropdown">
        <!-- ========================================= BREADCRUMB ========================================= -->
        <div id="top-mega-nav">
            <div class="container">
                <nav>
                    <ul class="inline">
                        <li class="dropdown le-dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-list"></i> Товары по категориям
                            </a>

                            <ul class="dropdown-menu">

                                @foreach(App\Sub_categories::all() as $sub_cat)
                                    <li><a href="{{url('category/'.$sub_cat->id)}}">{{$sub_cat->name}}</a></li>
                                @endforeach
                            </ul>
                        </li>

                        <li class="breadcrumb-nav-holder">
                            <ul>
                                <li class=" breadcrumb-item">
                                    <a href="{{url('/')}}">Главная</a>

                                </li><!-- /.breadcrumb-item -->

                                <li class=" breadcrumb-item current">
                                    <a href="#">
                                        Новости
                                    </a>
                                </li><!-- /.breadcrumb-item -->

                            </ul>
                        </li>
                    </ul><!-- /.inline -->
                </nav>

            </div><!-- /.container -->
        </div><!-- /#top-mega-nav -->
        <!-- ========================================= BREADCRUMB : END ========================================= -->
    </div>

    <main id="blog" class="inner-bottom-xs">
        <div class="container">
            <div class="row">
                @if($news->count() > 0)
                    @foreach($news as $new)
                        <div class="col-md-10">
                            <div class="posts sidemeta">
                                <div class="post format-image">
                                    <div class="date-wrapper">
                                        <div class="date">
                                            <span class="month">{{$new->updated_at}}</span>
                                        </div>
                                    </div><!-- /.date-wrapper -->
                                    <div class="post-content">

                                        <div class="post-media">

                                            <img src="{{asset('images/news/'.$new->img)}}"
                                                 alt=""  style="margin-top:55px; width:848px; height:429px;" />

                                        </div>
                                        <h2 class="post-title">{{$new->theme}}</h2>
                                        <ul class="meta">
                                            <li>Опубликовано : {{App\User::where('id', 1)->first()->name}}</li>
                                            <li><a href="#">{{App\NewsComments::where('news_id', $new->id)->count()}} Комментарий</a></li>
                                        </ul>

                                        @if(strlen($new->news < 150))
                                            <p>{{ mb_strimwidth($new->news, 0, 100, "...")}}</p>
                                        @else
                                            <p>{{$new->news}}</p>
                                        @endif
                                        <a href="{{url('news-more/'.$new->id)}}" class="le-button huge">Подробно</a>
                                    </div><!-- /.post-content -->
                                </div><!-- /.post -->
                            </div><!-- /.posts -->
                        </div><!-- /.col -->
                    @endforeach
                @endif

            </div><!-- /.row -->
        </div><!-- /.container -->
    </main><!-- /.inner-bottom-xs -->

@endsection