<nav class="top-bar animate-dropdown">
    <div class="container">
        <div class="col-xs-12 col-sm-6 no-margin">
            <ul>
                <li><a href="{{url('/')}}">Главная</a></li>
                <li><a href="{{url('news')}}">Новости</a></li>
                <li><a href="{{url('faqs')}}">ЧаВо</a></li>
                <li><a href="{{url('contact')}}">Контакты</a></li>
            </ul>
        </div>
        @if(!Auth::check())
        <div class="col-xs-12 col-sm-6 no-margin">
            <ul class="right">
                <li><a href="{{url('login')}}">Регистрация</a></li>
                <li><a href="{{url('login')}}">Войти</a></li>
            </ul>
        </div>
        @else
        <div class="col-xs-12 col-sm-6 no-margin">
            <ul class="right">    
                <li class="nav-item ">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right " aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                            {{ __('Выход') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                        </form>
                    </div>
                </li>
            </ul>
        </div>
        @endif
    </div>
</nav>

