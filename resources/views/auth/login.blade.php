@extends('client.partials.layouts')
@section('content')


<main id="authentication" class="inner-bottom-md">
    <div class="container">
        <div class="row">

            <div class="col-md-6">
                <section class="section sign-in inner-right-xs">
                    <h2 class="bordered">Войти</h2>

                    <form role="form" class="login-form cf-style-1"
                        method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <div class="field-row">
                            <label>Эл. почта</label>
                            <input type="email" name="email" id="email" 
                            class="le-input @error('email') is-invalid @enderror">
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div><!-- /.field-row -->

                        <div class="field-row">
                            <label>Пароль</label>
                            <input type="password" id="password" name="password" 
                                class="le-input @error('password') is-invalid @enderror">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div><!-- /.field-row -->

                        <div class="field-row clearfix">
                            <span class="pull-left">
                                <label class="content-color"><input type="checkbox" class="le-checbox auto-width inline"> <span class="bold">Запомнить меня</span></label>
                            </span>
                            <span class="pull-right">
                                <a href="#" class="content-color bold">Забыли пароль</a>
                            </span>
                        </div>

                        <div class="buttons-holder">
                            <button type="submit" class="le-button huge">Войти</button>
                        </div><!-- /.buttons-holder -->
                    </form><!-- /.cf-style-1 -->

                </section><!-- /.sign-in -->
            </div><!-- /.col -->

            <div class="col-md-6">
                <section class="section register inner-left-xs">
                    <h2 class="bordered">Создать новый Аккаунт</h2>
                    <form role="form" class="register-form cf-style-1" method="POST" action="{{ route('register') }}">
                    {{ csrf_field() }}
                        <div class="field-row">
                            <label>Имя</label>
                            <input type="text" id="name" name="name"
                                class="le-input @error('name') is-invalid @enderror" >
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div><!-- /.field-row -->
                        
                        <div class="field-row">
                            <label>Эл. почта</label>
                            <input type="email" id="email" name="email"
                                class="le-input @error('email') is-invalid @enderror">
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div><!-- /.field-row -->

                        <div class="field-row">
                            <label>Пароль</label>
                            <input type="password" id="email" name="password" 
                                class="le-input @error('password') is-invalid @enderror">
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div><!-- /.field-row -->

                        <div class="field-row">
                            <label>Подтвердите пароль</label>
                            <input type="password" name="password_confirmation" id="password-confirm" 
                            class="le-input">
                        </div><!-- /.field-row -->

                        <div class="buttons-holder">
                            <button type="submit" class="le-button huge">Зарегистрироваться</button>
                        </div><!-- /.buttons-holder -->
                    </form>

                </section><!-- /.register -->

            </div><!-- /.col -->

        </div><!-- /.row -->
    </div><!-- /.container -->
</main><!-- /.authentication -->


@endsection