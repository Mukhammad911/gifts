@extends('emails.base')

@section('title', 'Оформлен заказ на сайте gifts.tj')

@section('styles')

@endsection

@section('content')
    <table class="main" width="100%" cellpadding="0" cellspacing="0" style="border-radius: 0 0 0 0;border-top: 0;border-bottom: 0;">
        <tr>
            <td class="content-wrap aligncenter">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="content-block">
                            <h2 style="text-transform: capitalize;">Здравствуйте, {{$order->name}}!</h2>
                        </td>
                    </tr>
                    <tr>
                        <td class="content-block">
                            <p style="color:black">
                                Поздравляем! Вы успешно оплатили заказ #{{ $order->id }}. Ожидайте звонка оператора.
                            </p>

                        </td>
                    </tr>
                    <tr>
                        <td class="content-block">
                            <table class="invoice">
                                <tr>
                                    <td style="text-transform: capitalize;">{{$order->name}}<br>Заказ #{{ $order->id }}<br>{{ $order->created_at }}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <table class="invoice-items" cellpadding="0" cellspacing="0">
                                            @php
                                                $ordered_products = \App\Orders::where('order_requests_id', $order->id)->get();
                                                $total = 0;
                                            @endphp
                                            @if($ordered_products->count() > 0)
                                                @foreach ($ordered_products as $product)
                                                    <tr>
                                                        <td>{{ \App\Products::find($product->products_id)->name }}</td>
                                                        <td class="alignright">{{ $product->price }}</td>
                                                        @php
                                                            $total = $total+ $product->price
                                                        @endphp
                                                    </tr>
                                                @endforeach
                                            @endif
                                            <tr class="total">
                                                <td class="alignright" width="80%">Total</td>
                                                <td class="alignright">{{ number_format((float)$total, 2, '.', '') }} TJS</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="content-block">
                            <p>
                                Если у Вас возникнут вопросы незамедлительно обращайтесь в нашу службу поддержки через контактную форму:
                                <a href="http://upwork.tj/contact">gifts.tj</a>
                            </p>
                            <p>
                                С уважением, gifts.tj
                            </p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
@endsection