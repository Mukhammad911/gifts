@extends('emails.base')

@section('title', 'Зарегистрирован новый пользователь gifts.tj')

@section('styles')

@endsection

@section('content')
    <table class="main" width="100%" cellpadding="0" cellspacing="0" style="border-radius: 0 0 0 0;border-top: 0;border-bottom: 0;">
        <tr>
            <td class="content-wrap aligncenter">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="content-block">
                            <h2>Поздравляем!</h2>
                        </td>
                    </tr>
                    <tr>
                        <td class="content-block">
                            <p>
                                Ваша учетная запись в <a href="http://upwork.tj">gifts.tj</a>
                                успешно создана. Пароль для вашей учетной записи был добавлен.
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td class="content-block">
                            <table class="invoice">
                                <tr>
                                    <td>
                                        Учетные данные для авторизации на сайте:
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Логин:</b> {{ $user->email }}<br>
                                        <b>Пароль:</b> {{ $user->password }}<br>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                </table>
            </td>
        </tr>
    </table>
@endsection