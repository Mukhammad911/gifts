@extends('admin.partials.layout')
@section('content')
    <main class="o-page__content">
        @if(\Session::has('message'))
            <div class="alert alert-success" role="alert">
                <p> <i class="fa fa-check-circle-o"> </i> {{Session::get('message')}} </p>
            </div>
        @endif

        @if (count($errors))
            <br/>
            <div class="form-group">
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @endif

        <div class="col-12">
            <form action="{{url(config('app.admin_prefix').'/add_products/save')}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="">
                    <div class="row">
                        <div class="col-3">
                            <label for="exampleInputEmail1">Название Товара<span class="text-danger">*</span></label>
                            <input required type="text" name="name" class="form-control" id="name"
                                   placeholder="Название Товара" value="{{old('name')}}" />

                            <label for="exampleInputEmail1" class="mt-2">Цена<span class="text-danger">*</span></label>
                            <input required type="text" name="price" class="form-control mt-1" id="price"
                                   placeholder="Цена Товара" value="{{old('price')}}"/>


                            <label for="exampleInputEmail1" class="mt-2">Загрузить картинку<span class="text-danger">*</span></label><br/>
                            <span class="text-danger small">
                                <i>
                                    файл должен быть формата jpg, jpeg, png
                                </i>
                            </span>
                            <input required type="file" name="img" class="mt-1" id="img" />


                            <label for="exampleInputEmail1" class="mt-2">Выбор категории<span class="text-danger">*</span></label>

                            <p>
                                <a class="btn btn-primary" data-toggle="collapse" href="#multiCollapseExample3" role="button"
                                   aria-expanded="false" aria-controls="multiCollapseExample3">
                                    Категории
                                </a>
                            </p>

                            <div class="row">
                                <div class="col">
                                    <div class="collapse multi-collapse" id="multiCollapseExample3">
                                        <div class="card card-body ">

                                            @foreach($categories as $category)
                                                <div class="card-body p-1 d-sm-flex justify-content-between">
                                                    <h6 class="">{{$category->name}}</h6>
                                                    <div class="d-flex justify-content-center">
                                                        <div class="col-auto mr-auto">
                                                            <input class="col-auto mr-auto categories" type="checkbox"
                                                                   onchange="getSubCategories({{$category->id}})" value="{{$category->id}}">
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach

                                        </div>
                                    </div>
                                </div>

                            </div>



                            <label for="exampleInputEmail1" class="mt-2">Выбор подкатегорий<span class="text-danger">*</span></label>

                            <p>
                                <a class="btn btn-primary" data-toggle="collapse" href="#multiCollapseExample1" role="button"
                                   aria-expanded="false" aria-controls="multiCollapseExample1">
                                    Подкатегории
                                </a>
                            </p>

                            <div class="row">
                                <div class="col">
                                    <div class="collapse multi-collapse show" id="multiCollapseExample1">
                                        <div class="card card-body " id="sub_cats">
                                            {{--
                                            @foreach($sub_categories as $sub_category)

                                                <div class="card-body p-1 d-sm-flex justify-content-between sub_cats">
                                                    <h6 class="">{{$sub_category->name}}</h6>
                                                    <div class="d-flex justify-content-center">
                                                        <div class="col-auto mr-auto">
                                                            <input class="col-auto mr-auto" type="checkbox"  name="sub_category[]"
                                                                   value="{{$sub_category->id}}">
                                                        </div>
                                                    </div>
                                                </div>

                                            @endforeach
                                            --}}
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <label for="exampleInputEmail1" class="mt-3">Описание товара</label>

                            <textarea class="form-control" name="description" id="description" value="{{old('description')}}" rows="7"></textarea>
                        </div>

                        <div class="col-3">
                            <label for="exampleInputEmail1" class="">Старая Цена</label>
                            <input type="text" name="old_price" class="form-control" id="old_price" placeholder="Старая Цена Товара"/>

                            <label for="exampleInputEmail1" class="mt-2">Акция</label>
                            <input type="text" name="discount" class="form-control mt-1" id="discount" placeholder="Акция"/>
                            <span class="text-info small"><i>Введите отрицательное число, Пример: -10</i></span>

                            <label for="exampleInputEmail1" class="mt-2">Бренд/Поставщик</label>
                            <input type="text" name="brand" class="form-control mt-1" id="brand" placeholder="Бренд/Поставщик"/>

                            <label for="exampleInputEmail1" class="mt-2">Рапродажа</label>
                            <select name="onsale"  class="form-control mt-1" id="onsale">
                                <option value="0" selected>Нет</option>
                                <option value="1">Да</option>
                            </select>

                            <label for="exampleInputEmail1" class="mt-2">Новый товар</label>
                            <select name="new"  class="form-control mt-1" id="new">
                                <option value="0" selected>Нет</option>
                                <option value="1">Да</option>
                            </select>

                            <label for="exampleInputEmail1" class="mt-2">Топ продаж</label>
                            <select name="bestseller"  class="form-control mt-1" id="bestseller">
                                <option value="0" selected>Нет</option>
                                <option value="1">Да</option>
                            </select>

                            <label for="exampleInputEmail1" class="mt-2">В наличии</label>
                            <select name="available"  class="form-control mt-1" id="available">
                                <option value="0" >Нет</option>
                                <option value="1" selected>Да</option>
                            </select>

                            <label for="exampleInputEmail1" class="mt-2">Скоро в продаже </label>
                            <select name="feature"  class="form-control mt-1" id="new">
                                <option value="0" selected>Нет</option>
                                <option value="1">Да</option>
                            </select>

                        </div>

                        <div class="col-4">
                            <div class="form-group col-xs-3" id="divf">
                                @if($template_details->count() > 0 )
                                <label for="exampleInputEmail1" class="">Выбор из шаблона<span class="text-danger">*</span></label>

                                <p>
                                    <a class="btn btn-primary" data-toggle="collapse" href="#multiCollapseExample2" role="button"
                                       aria-expanded="false" aria-controls="multiCollapseExample2" >
                                        Шаблон деталей
                                    </a>
                                </p>

                                <div class="row">
                                    <div class="col">
                                        <div class="collapse multi-collapse" id="multiCollapseExample2">
                                            <div class="card card-body ">

                                                @foreach($template_details as $template_detail)
                                                    <div class="card-body p-1 d-sm-flex justify-content-between">
                                                        <a class="btn btn-light" title="Выбрать детали из шаблона"
                                                           onclick="getTemplate({{$template_detail->id}})">
                                                            {{$template_detail->name}}
                                                        </a>
                                                    </div>
                                                @endforeach
                                                    <p class="text-danger">Если не хотите добавить какую либо деталь
                                                        из шаблона, оставьте поле значение пустым.
                                                    </p>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                                @endif

                                <label for="exampleInputEmail1">Детали</label><br>
                                <button type="button" id="add" class="btn btn-info btn-sm" onclick="service()">
                                    <i class="fa fa-plus"> </i>
                                </button>
                                <button  type="button" class="btn btn-danger btn-sm" onclick="remove()" style="margin-left: 10px;">
                                    <i class="fa fa-minus"></i>
                                </button>
                                <div class="row">
                                    <div class="col-6">
                                        <div id="detail" class="mt-2 service " style="margin-top: 10px">
                                            {{--<input type="text" name="detail[]" class="form-control sel" placeholder="Название" style="margin-top: 10px;">--}}
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div id="value" class="mt-2 service " style="margin-top: 10px">
                                            {{--<input type="text" name="value[]" class="form-control cel" placeholder="Значение" style="margin-top: 10px;">--}}
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>

                        <div class="col-2">
                            <div class="form-group col-xs-3" id="divf">
                                <label for="exampleInputEmail1">Картинки</label><br>
                                <button type="button" id="add" class="btn btn-info btn-sm" onclick="plus()">
                                    <i class="fa fa-plus"> </i>
                                </button>
                                <button  type="button" class="btn btn-danger btn-sm" onclick="minus()" style="margin-left: 10px;">
                                    <i class="fa fa-minus"></i>
                                </button>
                                <div class="row">
                                    <div class="col-12">
                                        <div id="pictures" class="mt-2 " style="margin-top: 10px">
                                            {{--<input type="text" name="detail[]" class="form-control sel" placeholder="Название" style="margin-top: 10px;">--}}
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>



                    </div>

                </div>
                <input type="submit" class="btn btn-success mt-3 mb-5 col-12" value="Добавить" />
            </form>
        </div>
    </main>

    <script>
        function service() {

            var input = document.createElement("input");

            input.setAttribute("type", "text");
            input.setAttribute("required", "required");
            input.setAttribute("name", "detail[]");
            input.setAttribute("class", "form-control sel");
            input.setAttribute("style", "margin-top:10px;");
            input.setAttribute("placeholder", "Название");

            document.getElementById("detail").appendChild(input);

            var val = document.createElement("input");

            val.setAttribute("type", "text");
            val.setAttribute("required", "required");
            val.setAttribute("name", "value[]");
            val.setAttribute("class", "form-control cel");
            val.setAttribute("style", "margin-top:10px;");
            val.setAttribute("placeholder", "Значение");

            document.getElementById("value").appendChild(val);

        }

        function remove() {


            var detail_remove = document.body.getElementsByClassName("sel");
            if(detail_remove.length > 0) {
                document.getElementById('detail').removeChild(detail_remove[detail_remove.length - 1]);

            }

            var value_remove = document.body.getElementsByClassName("cel");
            if(value_remove.length > 0) {

                document.getElementById('value').removeChild(value_remove[value_remove.length - 1]);
            }
        }

        function plus() {

            var pics = document.createElement("input");

            pics.setAttribute("type", "file");
            pics.setAttribute("name", "pictures[]");
            pics.setAttribute("class", "pel");
            pics.setAttribute("style", "margin-top:10px;");

            document.getElementById("pictures").appendChild(pics);
        }

        function minus() {


            var pic_remove = document.body.getElementsByClassName("pel");
            if(pic_remove.length > 0) {
                document.getElementById('pictures').removeChild(pic_remove[pic_remove.length - 1]);

            }
        }

    </script>

    <script>
        function getTemplate(id){
            $.ajax({
                type:'GET',
                url:'/get_template/'+id,
                dataType: 'json',

                success: function (data) {
                    //console.log(data);
                    //container.html('');

                    $.each(data, function(index, item) {
                        //container.html(''); //clears container for new data


                        $.each(item, function(i, detail) {
                            if(detail.value == null){
                                detail.value = '';
                            }
                            $('#detail').append('<input type="text" value="'+detail.key+'" class="form-control sel" name="detail[]" style="margin-top: 10px;" placeholder="Название"/>');
                            $('#value').append('<input type="text" value="'+detail.value+'" class="form-control cel" name="value[]" style="margin-top: 10px;" placeholder="Значение"/>');
                        });


                        //container.append('<br>');
                    });
                },error:function(){
                    //console.log(data);
                }
            });
        }
    </script>

    <script>

        function getSubCategories(id){
            //console.log($(".categories")[0].checked);

            $.each($(".categories"), function (d, cat) {

              if(cat.checked){
                  //console.log(cat.value, cat.checked);
                  $.ajax({
                      type:'GET',
                      url:'/get_sub_categories/'+cat.value,
                      dataType: 'json',

                      success: function (data) {
                          //$(".sub_cats").remove();
                          $.each(data, function(index, item) {
                              $.each(item, function(i, sub_cat) {
                                  //console.log($('#sub_cats'+sub_cat.categories_id).length);
                                  //console.log(sub_cat);
                                  if($('#sub_cats'+sub_cat.id).length == 0){
                                      $('#sub_cats').append('<div class="card-body p-1 d-sm-flex justify-content-between sub_cats'+sub_cat.categories_id+'" id="sub_cats'+sub_cat.id+'"> <h6 class="">"'+sub_cat.name+'"</h6> <div class="d-flex justify-content-center"><div class="col-auto mr-auto"><input class="col-auto mr-auto" type="checkbox" value="'+sub_cat.id+'" name="sub_category[]" /></div></div> </div>');
                                  }

                                  //$('#detail').append('<input type="text" value="'+detail.key+'" class="form-control sel" name="detail[]" style="margin-top: 10px;" placeholder="Название"/>');
                                  //$('#value').append('<input type="text" value="'+detail.value+'" class="form-control cel" name="value[]" style="margin-top: 10px;" placeholder="Значение"/>');
                              });



                          });
                      },error:function(){
                          console.log(data);
                      }
                  });
              }
              else{
                  $(".sub_cats"+cat.value).remove();
                  //console.log(cat.value, cat.checked);
              }

            });

        }
    </script>

@endsection