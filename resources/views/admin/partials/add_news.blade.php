@extends('admin.partials.layout')
@section('content')
<main class="o-page__content">
    @if(\Session::has('message'))
        <div class="alert alert-success" role="alert">
            <p> <i class="fa fa-check-circle-o"> </i> {{Session::get('message')}} </p>
        </div>
    @endif
    
    <div class="col-4">
        <form action="{{url(config('app.admin_prefix').'/add_news/save')}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="container">
            <label for="exampleInputEmail1">Тема<span class="text-danger">*</span></label>
            <input type="text" name="theme" class="form-control mt-1" id="theme" placeholder="тема" required>
            <label for="exampleInputEmail1"  class="mt-1">Новость<span class="text-danger">*</span></label>
            <textarea  name="news"  id="news"  required
                style="height:150px; min-width:300px; max-width:800px; resize:horizontal; "> </textarea>
            
            <label for="exampleInputEmail1" class="mt-2">Загрузить картинку<span class="text-danger">*</span></label><br/>
            <span class="text-danger small"><i>файл должен быть формата jpg, jpeg, png и с разрешением больше 600x500</i></span>
            <input  type="file" name="img" class="mt-1" id="img" required/>
            
            <input type="submit" class="btn btn-success mt-3" value="Добавить">    
        @if (count($errors)) 
        <div class="form-group">
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach    
                </ul>
            </div>
        </div>
        @endif
        </div>    
        </form>        
    </div>
</main>

@endsection