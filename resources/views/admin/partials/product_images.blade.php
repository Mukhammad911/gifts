@extends('admin.partials.layout')
@section('content')
    <main class="o-page__content">
        <div class="col-12">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>№</th>
                    <th>Название продукта</th>
                    <th>Название картинки</th>
                    <th>картинка</th>
                    <th>Операции</th>
                </tr>
                </thead>
                <tbody>
                @if(count($images) != 0)
                    @foreach($images as $image)
                        <tr>
                            <td>{{$image->id}}</td>
                            <td>{{App\Products::find($image->products_id)->name}}</td>
                            <td>{{$image->name}}</td>
                            <td>
                                <img src="{{asset('images/products/'.$image->img)}}" width="150px" alt=""> 
                            </td>
                            <td>
                                <a class="btn btn-danger btn-sm text-white p-1"
                                   href="{{url(config('app.admin_prefix').'/product_images/delete/'.$image->id)}}">
                                <i class="fa fa-edit"></i> Удалить</a>
                            </td>
                        </tr>
                @endforeach
                @endif
                <tbody>
            </table>
            <br/>
        </div>
        <div class="col-3">
            <form action="{{url(config('app.admin_prefix').'/add_product_images/')}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="container">
                    <label for="exampleInputEmail1">Название Картинки</label>
                    <input type="text" name="name" class="form-control mt-1" id="name">

                    <label for="exampleInputEmail1" class="mt-2">Загрузить картинку<span class="text-danger">*</span></label><br/>
                    <span class="text-danger small"><i>файл должен быть формата jpg, jpeg, png и с разрешением больше 433x325</i></span>
                    <input required type="file" name="img" class="mt-1" id="img"/>

                    <input type="hidden" name="product_id" value="{{$products->id}}">
                    
                    <input type="submit" class="btn btn-success mt-3" value="Добавить">
                    @if (count($errors))
                        <div class="form-group">
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                </div>
            </form>
        </div>
    </main>
@endsection