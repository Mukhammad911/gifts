@extends('admin.partials.layout')
@section('content')
<main class="o-page__content">
    @if(\Session::has('message'))
    <div class="alert alert-success" role="alert">
        <p> <i class="fa fa-check-circle-o"> </i> {{Session::get('message')}} </p>
    </div>
    @endif
    <div class="col-8">
        <form action="{{url(config('app.admin_prefix').'/update_descriptions/save')}}" method="post">
        {{ csrf_field() }}
        <div class="container">
            <label for="exampleInputEmail1">Название Товара</label>
            <input type="text" name="name"  class="form-control mt-1 col-4" id="name" disabled value="{{$data->name}}">
            <label for="exampleInputEmail1" class="mt-3">Цена Товара</label>
            <input type="text" name="price" class="form-control mt-1 col-4" id="price" disabled value="{{$data->price}}">
            <label for="exampleInputEmail1" class="mt-3">Описание товара</label>
            
            @if(!$data_desc)
            <textarea  name="description"  
                id="description" 
                style="height:150px; min-width:300px; max-width:800px; resize:horizontal; ">
            </textarea>
            @else
            <textarea  name="description"
                id="description" 
                style="height:150px; min-width:300px; max-width:800px; resize:horizontal; ">{{$data_desc->name}}
            </textarea>
            @endif
            <input type="hidden" name="id" value="{{$data->id}}">
            <input type="submit" class="btn btn-success mt-3" value="Изменить">    
        @if (count($errors)) 
        <div class="form-group">
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach    
                </ul>
            </div>
        </div>
        @endif
        </div>    
        </form>        
    </div>
</main>
@endsection