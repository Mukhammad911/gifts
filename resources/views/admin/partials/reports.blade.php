<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,600" rel="stylesheet">

    <!-- Favicon -->
    <link rel="apple-touch-icon" href="{{asset('apple-touch-icon.png')}}">
    <link rel="shortcut icon" href="{{asset('images/favicon.ico')}}">

    <link rel="stylesheet" href="{{asset('css/admin/main.min.css')}}">
    <!-- Stylesheet -->

    <!-- Bootstrap -->

    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css" rel="stylesheet">

    <!-- Bootstrap 3.3.7 -->
    <!-- DateTimepicker style -->

    <link rel="stylesheet" href="{{ asset('css\order_statistics.css') }}">

    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/jquery-jgrowl/1.4.1/jquery.jgrowl.min.css" />



    <title>Панель Администратора</title>
</head>
<body class="o-page">

@include('admin.partials.sidebar')
@include('admin.partials.header')

<main class="o-page__content">
    <div class="container-fluid">
        <div class="content-wrapper">
            <div class="statistics-loader"></div>
            {{-- Forms --}}
            <div class="form-group">
                <div class="row" style="margin-left: 10px;">
                    <div class="form-group col-lg-2">
                        <label>Статус Заказа</label>
                        <select name="status" class="form-control " style="width: 100%;" id="status_select">
                            <option value="all">Все</option>
                            <option value="cart">cart</option>
                            <option value="complete">complete</option>
                            <option value="boxing">boxing</option>
                            <option value="canceled">canceled</option>
                            <option value="paid">paid</option>
                            <option value="payment">payment</option>
                            <option value="preformed">preformed</option>
                            <option value="shipping">shipping</option>
                        </select>
                    </div>
                    <div class="form-group col-lg-2">
                        <label>Начальная дата</label>
                        <div class='input-group date' id='datetimepicker_date_start'>
                            <input
                                    name="date_start"
                                    type='date'
                                    class="form-control"
                                    autocomplete="off"
                                    value=""/>
                            <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                        </div>
                    </div>
                    <div class="form-group col-lg-2">
                        <label>Конечная дата</label>
                        <div class='input-group date' id='datetimepicker_date_end'>
                            <input
                                    name="date_end"
                                    type='date'
                                    class="form-control"
                                    autocomplete="off"
                                    value=""/>
                            <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                        </div>
                    </div>
                    <div class="form-group col-lg-1">
                        <label>Фильтр</label>
                        <div class='input-group'>
                            <button class="btn btn-primary btn order-filter-btn" type="submit">Фильтровать</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- End Forms --}}

        {{-- Charts --}}
        <div class="form-group">
            <div class="row data_table">
                <div class="col-lg-3">
                    <canvas id="donught-chart"></canvas>
                </div>
                <div class="col-lg-4">
                    <canvas id="line-chart" style="height:250px"></canvas>
                </div>

                {{-- Data Table --}}
                <div class="form-group">
                    <div class="row data_table">
                        <div class="col-lg-12">
                            <table id="statistics-table" class="display nowrap"
                                   style="width:100%; display:none">
                                <thead>
                                <tr>
                                    <th>Номер Заказа</th>
                                    <th>Имя</th>
                                    <th>Фамилия</th>
                                    <th>Адрес</th>
                                    <th>Почта</th>
                                    <th>Телефон</th>
                                    <th>Телефон. доп</th>
                                    <th>Город</th>
                                    <th>Доставка</th>
                                    <th>Статус</th>
                                    <th>Корти Милли</th>
                                    <th>Цена</th>
                                    <th>Товар</th>
                                    <th>Создан</th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                                <tfoot>
                                <tr>
                                    <th>Номер Заказа</th>
                                    <th>Имя</th>
                                    <th>Фамилия</th>
                                    <th>Адрес</th>
                                    <th>Почта</th>
                                    <th>Телефон</th>
                                    <th>Телефон. доп</th>
                                    <th>Город</th>
                                    <th>Доставка</th>
                                    <th>Статус</th>
                                    <th>Корти Милли</th>
                                    <th>Цена</th>
                                    <th>Товар</th>
                                    <th>Создан</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                {{-- End Data Table --}}
            </div>
            <div class="statistics-loader"></div>
        </div>
    </div>
</main>


<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>

<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
<!-- DatepickerJS -->
<script src="{{ asset('bower_components\moment\moment.js') }}"></script>

<script src="{{ asset('bower_components\eonasdan\datatimepicker\js\bootstrap-datetimepicker.js') }}"></script>

<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-jgrowl/1.4.1/jquery.jgrowl.min.js"></script>

<script src="{{ asset('bower_components\chart.js\Chart.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/Common.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/Helper.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/OrderStatistics.js') }}"></script>

</body>
</html>