@extends('admin.partials.layout')
@section('content')
<main class="o-page__content">
<div class="container-fluid">
    <div class="card mb-4 wow fadeIn">
        <div class="card-body d-sm-flex justify-content-between">
            <h4 class="mb-2 mb-sm-0 pt-1">
                Рассылки
            </h4>    
            
        </div>
        <div class="container">
        <table class="table table-striped table-bordered data-table">
            
            <thead>
            <tr>
                <th scope="col"><strong>№</strong></th>     
                <th scope="col"><strong>Email</strong></th>
                <th scope="col"><strong>Действие</strong></th>
            </tr>
            </thead>
            <tbody>
            @foreach($newsletters as $newsletter)
            <tr>
                <th scope="row">{{$newsletter->id}}</th>
                <td>{{$newsletter->email}}</td>
                <td>
                <a class="btn btn-danger btn-sm text-white" 
                   href="{{url(config('app.admin_prefix').'/newsletters/delete/'.$newsletter->id)}}">
                <i class="fa fa-trash-o"></i> Удалить</a>
                </td>
            </tr>
            @endforeach
            </tbody>
            
        </table>
        </div>
    </div>              
</div>      
</main>
<script src="{{ asset('teleglobal\callcenter\bower_components\datatables.net\js\jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('teleglobal\callcenter\bower_components\datatables.net-bs\js\dataTables.bootstrap.min.js') }}"></script>
<script>
    $(function () {
        $('.data-table').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': true
        });
    });

</script>

@endsection