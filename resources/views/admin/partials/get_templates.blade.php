@extends('admin.partials.layout')
@section('content')
<main class="o-page__content">
        <div class="col-12">
        @if( Session::has('message'))
            <div class="alert alert-success" role="alert">
                <p style="font-size:2rem;">{{$success_message=Session::get('message')}}</p>
            </div>
        @endif
        <h4 class="mb-3">Товар {{App\Products::find($product_id)->name}}</h4>
        <form method="post" action="{{route('template.save')}}">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>KEY</th>
                    <th>VALUE</th>
                </tr>
                </thead>
                {{ csrf_field() }}
                <tbody>
                    @foreach($data as $row)
                        <tr>
                            <td><input name="key[]" type="hidden" value="{{$row->key}}">{{$row->key}}</td>
                            <td><input name="value[]" type="text" value="{{$row->value}}" required></td>
                        </tr>
                    @endforeach
                <tbody>
                <input type="hidden" name="product_id" value="{{$product_id}}">
            </table>
            <input type="submit" class="btn btn-success mt-3 mb-2" value="Сохранить">
            </form>
        </div>
    </main>
@endsection