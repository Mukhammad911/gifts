@extends('admin.partials.layout')
@section('content')
<main class="o-page__content">
    @if(\Session::has('message'))
        <div class="alert alert-success" role="alert">
            <p> <i class="fa fa-check-circle-o"> </i> {{Session::get('message')}} </p>
        </div>
    @endif
    
    <div class="col-3">
        <form action="{{url(config('app.admin_prefix').'/add_banners/save')}}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="container">
            <label for="exampleInputEmail1">Название<span class="text-danger">*</span></label>
            <input type="text" name="name" class="form-control mt-1" id="name" placeholder="Название" required>
            
            <label for="exampleInputEmail1" class="mt-2">Загрузить картинку<span class="text-danger">*</span></label><br/>
            <span class="text-danger small"><i>файл должен быть формата jpg, jpeg, png и с разрешением больше 898x525</i></span>
            <input  type="file" name="img" class="mt-1" id="img" required/>
            
            <input type="submit" class="btn btn-success mt-3" value="Добавить">    
        @if (count($errors)) 
        <div class="form-group">
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach    
                </ul>
            </div>
        </div>
        @endif
        </div>    
        </form>        
    </div>
</main>

@endsection