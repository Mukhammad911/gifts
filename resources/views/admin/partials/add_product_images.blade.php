@extends('admin.partials.layout')
@section('content')
    <main class="o-page__content">
        <div class="col-12">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>№</th>
                    <th>Название продукта</th>
                    <th>Название детали</th>
                    <th>Значение</th>
                    <th>Создан</th>
                    <th>Обновлён</th>
                    <th>Операции</th>
                </tr>
                </thead>
                <tbody>
                @if(count($images) != 0)
                    @foreach($images as $image)
                        <tr>
                            <td>{{$image->id}}</td>
                            <td>{{App\Products::find($image->products_id)->name}}</td>
                            <td>{{$images->name}}</td>
                            <td>{{$images->img}}</td>
                            <td>{{$detail->created_at}}</td>
                            <td>{{$detail->updated_at}}</td>
                            <td>
                                <a class="btn btn-danger btn-sm text-white p-1"
                                   href="{{url(config('app.admin_prefix').'/product-details/delete/'.$detail->id)}}">
                                <i class="fa fa-edit"></i> Удалить</a>
                            </td>
                        </tr>
                @endforeach
                @endif
                <tbody>
            </table>
            <br/>
        </div>
        <div class="col-3">
            <form action="/controlshop/add-details" method="post">
                {{ csrf_field() }}
                <div class="container">
                    <label for="exampleInputEmail1">Название детали</label>
                    <input type="text" name="detail_name" class="form-control mt-1" id="detail_name">

                    <label for="exampleInputEmail1" class="mt-3">Значение Детали</label>
                    <input type="text" name="detail_value" class="form-control mt-1" id="detail_value">
                    <input type="hidden" name="product_id" class="form-control mt-1" value="{{$products->id}}">

                    <input type="submit" class="btn btn-success mt-3" value="Добавить">
                    @if (count($errors))
                        <div class="form-group">
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                </div>
            </form>
        </div>
    </main>
@endsection