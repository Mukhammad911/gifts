@extends('admin.partials.layout')
@section('content')
<main class="o-page__content">
<div class="container-fluid mt-5">
    <div class="card mb-4 wow fadeIn">
        <div class="card-body d-sm-flex justify-content-between">
            <h4 class="mb-2 mb-sm-0 pt-1">
                Детали товаров
            </h4>    
            
            <form class="d-flex justify-content-center">
                <input type="search" class="form-control" 
                    placeholder="Поиск">
                <button class="btn btn-primary btn-sm my-0 p"
                    type="submit">
                <i class="fa fa-search"></i>
                </button>
            </form>
        </div>
        <div class="container p-5">
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th scope="col"><strong>№</strong></th>     
                <th scope="col-md-4"><strong>Товар</strong></th>
                <th scope="col"><strong>Детали</strong></th>
                <th scope="col"><strong>Действие</strong></th>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $product)
            @foreach($product->descriptions as $desc)
            @foreach($desc->details as $detail)
            <tr>
                <th scope="row">{{$product->id}}</th>
                <td>{{$product->name}}</td>
                <td>{{$detail->name}}</td>
                <td>
                    <a class="btn btn-info btn-sm text-white"
                        href="admindescriptions/update/{{$product->id}}">
                    <i class="fa fa-edit"></i> Детали</a>
                    <a class="btn btn-danger btn-sm text-white"
                        href="admindescriptions/delete/{{$desc->id}}">
                    <i class="fa fa-trash-o"></i> Удалить</a>
                </td>
                {{-- <td>
                <a class="btn btn-warning btn-sm text-white"
                    href="admincategories_update/{{$desc->id}}"
                >
                <i class="fa fa-edit"></i> Изменить</a>
                <a class="btn btn-danger btn-sm text-white" 
                    href="/admincategories/delete/{{$desc->id}}">
                <i class="fa fa-trash-o"></i> Удалить</a>
                </td> --}}
            </tr>
            @endforeach
            @endforeach
            @endforeach
            </tbody>
        </table>
        </div>
    </div>
</div>

</main>
@endsection