@extends('admin.partials.layout')
@section('content')
<main class="o-page__content">
    <div class="col-3">
        @if(Session::has('error'))
            <div class="alert alert-danger">{{ Session::get('error') }}</div>
        @endif
            <form action="/controlshop/add_some_products/save" method="post" enctype="multipart/form-data" >
                {{ csrf_field() }}
                <div class="container">
                    <label for="exampleInputEmail1">Выберите файл Excel </label>
                    <input type="file" name="excel"  id="excel" />
                    <input type="submit" class="btn btn-success mt-3" value="Добавить" />
                    @if (count($errors))
                    <div class="form-group">
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @endif
                </div>
            </form>
    </div>
</main>
@endsection