@extends('admin.partials.layout')
@section('content')
    <main class="o-page__content">
        <div class="card mb-4 wow fadeIn">
            @if(\Session::has('message'))
                <div class="alert alert-success" role="alert">
                    <p> <i class="fa fa-check-circle-o"> </i> {{Session::get('message')}} </p>
                </div>
            @endif

            <div class="card-body d-sm-flex justify-content-between">
                <h4 class="mb-2 mb-sm-0 pt-1">
                    Товары
                </h4>

                <form class="d-flex justify-content-center">
                    <div class="col-auto  mr-auto">
                        <a href="{{url(config('app.admin_prefix').'/add_some_products')}} "
                           class="btn btn-primary mr-3">
                            <i class="fa fa-file-excel-o"></i>    Добавить товары из файла
                        </a>
                        <a href="{{url(config('app.admin_prefix').'/add_products')}}"
                           class="btn btn-primary">
                            <i class="fa fa-plus"></i>    Добавить товар
                        </a>
                    </div>
                </form>
            </div>
            <div class="container-fluid">

                <table class="table table-striped table-responsive table-bordered data-table">
                    <thead>
                    <tr>
                        <th scope="col"><strong>№</strong></th>
                        <th scope="col" style="width:5%;"><strong>Товары</strong></th>
                        <th scope="col"><strong>Цена</strong></th>
                        <th scope="col"><strong>Подкатегория</strong></th>
                        <th scope="col" style="width:6%;"><strong>Категории</strong></th>
                        <th scope="col"><strong>Описание</strong></th>
                        <th scope="col"><strong>Дополнительно</strong></th>
                        <th scope="col"><strong>Действие</strong></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <th scope="row">{{$product->id}}</th>
                            <td>{{$product->name}}</td>
                            <td>{{$product->price}}</td>
                            <td>
                                @foreach($product->sub_categories as $sub_category)
                                    {{--$product->sub_categories->name ?? ''--}}
                                    <span>-</span> {{$sub_category->name}}<br>
                                @endforeach
                            </td>

                            <td>
                                @foreach($product->sub_categories as $sub_category)
                                    <span>-</span> {{$sub_category->categories->name ?? ''}}<br>
                                @endforeach
                            </td>
                            <td>
                                {{$product->description}}
                            </td>
                            <td>

                                <a class="btn btn-info btn-sm text-white p-1"
                                   href="{{url(config('app.admin_prefix').'/product-details/'.$product->id)}}">
                                    <i class="fa fa-edit"></i> Детали
                                </a>
                                <a class="btn btn-success btn-sm text-white p-1"
                                   href="{{url(config('app.admin_prefix').'/product_images/'.$product->id)}}">
                                    <i class="fa fa-image"></i> Картинки
                                </a>
                            </td>
                            <td>
                                <a
                                        class="btn btn-primary"
                                        href="{{url(config('app.admin_prefix').'/adminproducts/update/'.$product->id)}}">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <a
                                        class="btn btn-danger"
                                        href="{{url(config('app.admin_prefix').'/adminproducts/delete/'.$product->id)}}"
                                ><i class="fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th scope="col"><strong>№</strong></th>
                        <th scope="col" style="width:5%;"><strong>Товары</strong></th>
                        <th scope="col"><strong>Цена</strong></th>
                        <th scope="col"><strong>Подкатегория</strong></th>
                        <th scope="col" style="width:6%;"><strong>Категории</strong></th>
                        <th scope="col"><strong>Описание</strong></th>
                        <th scope="col"><strong>Дополнительно</strong></th>
                        <th scope="col"><strong>Действие</strong></th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </main>

    <script>
        $(function () {
            $('.data-table').DataTable({
                'paging': true,
                'lengthChange': true,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': true
            });
        });

    </script>
@endsection