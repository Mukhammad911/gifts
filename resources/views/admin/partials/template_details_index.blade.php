@extends('admin.partials.layout')
@section('content')
<main class="o-page__content">
<div class="container-fluid">
    @if( Session::has('message'))
        <div class="alert alert-success" role="alert">
            <p>{{$success_message=Session::get('message')}}</p>
        </div>
    @endif
    <div class="card mb-4 wow fadeIn">
        <div class="card-body d-sm-flex justify-content-between">
            <h4 class="mb-2 mb-sm-0 pt-1">
                Template Details
            </h4>    
        
            <form class="d-flex justify-content-center">
                <div class="col-auto  mr-auto">
                    <a href="{{route('template_details.create')}} " class="btn btn-success">
                    <i class="fa fa-plus"></i>    Добавить шаблон
                    </a>
                </div>
                <div class="col-auto  mr-auto">
                    <a href="{{route('template_details_items.create')}} " class="btn btn-success">
                    <i class="fa fa-plus"></i>    Добавить детали для шаблона
                    </a>
                </div>
            </form>
        </div>
        <div class="container">
        <table class="table table-striped table-bordered data-table">
            
            <thead>
            <tr>
                <th scope="col"><strong>№</strong></th>     
                <th scope="col-md-4"><strong>Templates</strong></th>
                <th scope="col"><strong>actions</strong></th>
                <th scope="col"><strong>created at</strong></th>
                <th scope="col"><strong>updated at</strong></th>
            </tr>
            </thead>
            <tbody>
            @foreach($data as $row)
            <tr>
                <th scope="row">{{$row->id}}</th>
                <td><a href="{{route('template_details.show',$row->id)}}">{{$row->name}}</a>
            </td>
                <td>
                <form action="{{ route('template_details.destroy',$row->id) }}" method="POST">
                    <a class="btn btn-warning btn-sm text-white"
                        href="{{route('template_details.edit',$row->id)}}"
                    >
                    <i class="fa fa-edit"></i> Change</a>
                
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger btn-sm text-white">
                    <i class="fa fa-trash-o"></i> Delete</button>
                </form>
                </td>
                <td>{{$row->created_at}}</td>
                <td>{{$row->updated_at}}</td>
            </tr>
            @endforeach
            </tbody>
            
        </table>
        </div>
    </div>              
</div>      
</main>
<script src="{{ asset('teleglobal\callcenter\bower_components\datatables.net\js\jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('teleglobal\callcenter\bower_components\datatables.net-bs\js\dataTables.bootstrap.min.js') }}"></script>
<script>
    $(function () {
        $('.data-table').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': true
        });
    });

</script>
@endsection