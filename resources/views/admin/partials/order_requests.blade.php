@extends('admin.partials.layout')
@section('content')

    <main class="o-page__content">
        <div class="container-fluid">
            @if(\Session::has('message'))
                <div class="alert alert-success" role="alert">
                    <p> {{Session::get('message')}}</p>
                </div>

            @endif
            <div class="card-body d-sm-flex justify-content-between">
                <h4 class="mb-2 mb-sm-0 pt-1">
                    Заказы
                </h4>

            </div>
            <div class="container">
                <table class="table table-striped table-bordered data-table">
                    <thead>
                    <tr>
                        <th  class="p-1"><strong>№</strong></th>
                        <th  class="p-1"><strong>Имя</strong></th>
                        <th  class="p-1"><strong>Фамилия</strong></th>
                        <th  class="p-1"><strong>Адрес</strong></th>
                        <th  class="p-1"><strong>Email</strong></th>
                        <th  class="p-1"><strong>Телефон</strong></th>
                        <th  class="p-1"><strong>Телефон, доп</strong></th>
                        <th  class="p-1"><strong>Город</strong></th>
                        <th  class="p-1"><strong>Доставка</strong></th>
                        <th  class="p-1"><strong>Оплата</strong></th>
                        <th  class="p-1"><strong>Статус</strong></th>
                        <th  class="p-1"><strong>Изменить</strong></th>
                        <th  class="p-1"><strong>Дата</strong></th>
                        <th  class="p-1"><strong>Просмотр</strong></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($order_requests as $order)
                        <tr>
                            <th scope="row" class="p-1"><a href="{{url(config('app.admin_prefix').  '/info/order/'.$order->id)}}">{{$order->id}}</a></th>
                            <td class="p-1">{{$order->name}}</td>
                            <td class="p-1">{{$order->surname}}</td>
                            <td class="p-1">{{$order->address}}</td>
                            <td class="p-1">{{$order->email}}</td>
                            <td class="p-1">{{$order->phone}}</td>
                            <td class="p-1">{{$order->otherPhone}}</td>
                            <td class="p-1">{{$order->city}}</td>
                            <td class="p-1">{{$order->delivery}}</td>
                            <td class="p-1">{{$order->korti_milli}}</td>


                            <td class="p-1">{{$order->status}}</td>
                            <td class="p-1">
                                <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                                    <div class="btn-group" role="group">
                                        <button id="btnGroupDrop1" type="button"
                                                class="btn btn-secondary btn-sm dropdown-toggle text-white"
                                                data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false">
                                            Поменять статус
                                        </button>
                                        <div class="dropdown-menu " aria-labelledby="btnGroupDrop1">
                                            <a class="dropdown-item p-1"
                                               href="{{url(config('app.admin_prefix').'/orders/change_status/'.$order->id.'/cart')}}">
                                                Cart
                                            </a>
                                            <a  class="dropdown-item p-1"
                                                href="{{url(config('app.admin_prefix').'/orders/change_status/'.$order->id.'/complete')}}">
                                                Completed
                                            </a>
                                            <a  class="dropdown-item p-1"
                                                href="{{url(config('app.admin_prefix').'/orders/change_status/'.$order->id.'/boxing')}}">
                                                Boxing
                                            </a>
                                            <a  class="dropdown-item p-1"
                                                href="{{url(config('app.admin_prefix').'/orders/change_status/'.$order->id.'/canceled')}}">
                                                Canceled
                                            </a>
                                            <a  class="dropdown-item p-1"
                                                href="{{url(config('app.admin_prefix').'/orders/change_status/'.$order->id.'/payment')}}">
                                                Payment
                                            </a>
                                            <a  class="dropdown-item p-1"
                                                href="{{url(config('app.admin_prefix').'/orders/change_status/'.$order->id.'/paid')}}">
                                                Paid
                                            </a>

                                            <a  class="dropdown-item p-1"
                                                href="{{url(config('app.admin_prefix').'/orders/change_status/'.$order->id.'/preformed')}}">
                                                Preformed
                                            </a>
                                            <a  class="dropdown-item p-1"
                                                href="{{url(config('app.admin_prefix').'/orders/change_status/'.$order->id.'/shipping')}}">
                                                Shipping
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td class="p-1">{{$order->created_at}}</td>
                            <td class="p-1">
                                <a class="btn btn-info btn-sm"
                                   href="{{url(config('app.admin_prefix').'/info/order/'.$order->id)}}">
                                    Просмотр
                                </a>
                            </td>
                        </tr>

                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th  class="p-1"><strong>№</strong></th>
                        <th  class="p-1"><strong>Имя</strong></th>
                        <th  class="p-1"><strong>Фамилия</strong></th>
                        <th  class="p-1"><strong>Адрес</strong></th>
                        <th  class="p-1"><strong>Email</strong></th>
                        <th  class="p-1"><strong>Телефон</strong></th>
                        <th  class="p-1"><strong>Телефон, доп</strong></th>
                        <th  class="p-1"><strong>Город</strong></th>
                        <th  class="p-1"><strong>Доставка</strong></th>
                        <th  class="p-1"><strong>Оплата</strong></th>
                        <th  class="p-1"><strong>Статус</strong></th>
                        <th  class="p-1"><strong>Изменить</strong></th>
                        <th  class="p-1"><strong>Дата</strong></th>
                        <th  class="p-1"><strong>Просмотр</strong></th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </main>
    <script src="{{ asset('teleglobal\callcenter\bower_components\datatables.net\js\jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('teleglobal\callcenter\bower_components\datatables.net-bs\js\dataTables.bootstrap.min.js') }}"></script>
    <script>
        $(function () {
            $('.data-table').DataTable({
                'paging': true,
                'lengthChange': true,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': true
            });
        });

    </script>


@endsection