@extends('admin.partials.layout')
@section('content')
<main class="o-page__content">
    @if( Session::has('message'))
        <div class="alert alert-success" role="alert">
            <p style="font-size:2rem;">{{$success_message=Session::get('message')}}</p>
        </div>
    @endif
    <div class="col-3">
        <form action="{{route('template_details.update',$data->id)}}" method="post">
        {{ csrf_field() }}
        {{ method_field('PATCH') }}
        <div class="container">
            <label for="name">Название шаблона детали</label>
            <input type="text" name="name" class="form-control mt-1" id="name" value="{{$data->name}}">
            <input type="hidden" name="id" value="{{$data->id}}">
            <input type="submit" class="btn btn-success mt-3" value="Изменить">    
        @if (count($errors)) 
        <div class="form-group">
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach    
                </ul>
            </div>
        </div>
        @endif
        </div>  
        </form>      
    </div>
</main>

@endsection