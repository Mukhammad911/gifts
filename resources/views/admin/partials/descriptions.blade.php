@extends('admin.partials.layout')
@section('content')
<main class="o-page__content">
<div class="container-fluid">
    <div class="card mb-4 wow fadeIn">
        <div class="card-body d-sm-flex justify-content-between">
            <h4 class="mb-2 mb-sm-0 pt-1">
                Описание товаров
            </h4>    
            
        </div>
        <div class="container">
        <table class="table table-striped table-bordered data-table">
            
            <thead>
            <tr>
                <th scope="col"><strong>№</strong></th>     
                <th scope="col-md-4"><strong>Товар</strong></th>
                <th scope="col" style="width:25%"><strong>Описание</strong></th>
                <th scope="col"><strong>Действие</strong></th>
                <th scope="col"><strong>Создан</strong></th>
                <th scope="col"><strong>Обновлен</strong></th>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $product)
            @foreach($product->descriptions as $desc)
            <tr>
                <th scope="row">{{$product->id}}</th>
                <td>{{$product->name}}</td>
                <td>{{$desc->name}}</td>
                <td>
                    <a class="btn btn-info btn-sm text-white"
                        href="admindescriptions/update/{{$product->id}}">
                    <i class="fa fa-edit"></i> Описание</a>
                    <a class="btn btn-danger btn-sm text-white"
                        href="admindescriptions/delete/{{$desc->id}}">
                    <i class="fa fa-trash-o"></i> Удалить</a>
                </td>
                <td>{{$desc->created_at}}</td>
                <td>{{$desc->updated_at}}</td>
                {{-- <td>
                <a class="btn btn-warning btn-sm text-white"
                    href="admincategories_update/{{$desc->id}}"
                >
                <i class="fa fa-edit"></i> Изменить</a>
                <a class="btn btn-danger btn-sm text-white" 
                    href="/admincategories/delete/{{$desc->id}}">
                <i class="fa fa-trash-o"></i> Удалить</a>
                </td> --}}
            </tr>
            @endforeach
            @endforeach
            </tbody>
        </table>
        {{--{{$products->links()}}--}}
        </div>
    </div>
</div>
</main>
<script src="{{ asset('teleglobal\callcenter\bower_components\datatables.net\js\jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('teleglobal\callcenter\bower_components\datatables.net-bs\js\dataTables.bootstrap.min.js') }}"></script>
<script>
    $(function () {
        $('.data-table').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': true
        });
    });

</script>
@endsection