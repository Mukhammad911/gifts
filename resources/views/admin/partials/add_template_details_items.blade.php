@extends('admin.partials.layout')
@section('content')
<main class="o-page__content">
    @if( Session::has('message'))
        <div class="alert alert-success" role="alert">
            <p style="font-size:2rem;">{{$success_message=Session::get('message')}}</p>
        </div>
    @endif
    <div class="col-3">
        <form action="{{route('template_details_items.store')}}" method="post">
            {{ csrf_field() }}
            <div class="container">
                <label for="exampleInputEmail1">Название Детали</label>
                <input type="text" name="template_key" class="form-control mt-1" id="template_key" placeholder="Название Детали"/>
                <label for="exampleInputEmail1">Значение Детали</label>
                <input type="text" name="template_value" class="form-control mt-1" id="template_value" placeholder="Значение Детали"/>
                <label for="exampleInputEmail1" class="mt-2">Название Шаблона</label>
                
                <select name="template" onchange="getSelect();" class="form-control mt-1" id="template"> 
                @if(isset($id_row))  
                    <option value="{{ $id_row}}"> {{App\Template_details::where('id', '=', $id_row )->first()->name}}</option>
                @else
                    @foreach($data as $row)
                    <option value="{{ $row->id }}"> {{ $row->name }} </option>
                    @endforeach
                @endif
                </select>
                <input type="submit" class="btn btn-success mt-3" value="Добавить" />    
            @if (count($errors)) 
            <div class="form-group">
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach    
                    </ul>
                </div>
            </div>
            @endif
            </div>    
        </form>        
    </div>
</main>
<script>
function getSelect() {
    var select = document.getElementById('category').value;
    console.log(select);
}
</script>
@endsection