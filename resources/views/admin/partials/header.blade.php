<main class="o-page__content">
<header class="c-navbar u-mb-medium">
    <button class="c-sidebar-toggle u-mr-small">
        <span class="c-sidebar-toggle__bar"></span>
        <span class="c-sidebar-toggle__bar"></span>
        <span class="c-sidebar-toggle__bar"></span>
    </button><!-- // .c-sidebar-toggle -->

    <h2 class="c-navbar__title u-mr-auto">Главная</h2>

    <div class="dropdown">
        <a class=" dropdown-toggle c-avatar c-avatar--small" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <img class="c-avatar__img" src="{{asset('images/admin/boss.png')}}" >
        </a>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" style="position: absolute; transform: translate3d(-72px, 50px, 0px); top: 0px; left: 0px; will-change: transform;">
        <a class="dropdown-item" href="{{ route('logout') }}"
            onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
            {{ __('Выход') }}
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
        </form>

        </div>
    </div>
</header>
</main>