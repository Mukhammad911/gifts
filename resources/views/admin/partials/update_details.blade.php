@extends('admin.partials.layout')
@section('content')
<main class="o-page__content">
    @if(\Session::has('message'))
        <div class="alert alert-success" role="alert">
            <p> <i class="fa fa-check-circle-o"> </i> {{Session::get('message')}} </p>
        </div>
    @endif
    
    <div class="col-4">    
        <form action="{{url(config('app.admin_prefix').'/update_detail/save')}}" method="post">
        {{ csrf_field() }}
        <div class="container">
            
            
            @if($data_det)
            <label class="m-1" for="">Название детали</label>
            <input type="text"  name="detail" class="form-control m-1" 
                id="detail" value="{{$data_det->name}}">
            <label class="m-1" for="">Значение детали</label>
            <input type="text"  name="value" class="form-control m-1" 
                id="detail" value="{{$data_det->value}}">
            @endif
            <input type="hidden" name="id" value="{{$data_det->id}}" />
            <input type="submit" class="btn btn-success mt-3" value="Изменить">    
        @if (count($errors)) 
        <div class="form-group">
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach    
                </ul>
            </div>
        </div>
        @endif
        </div>    
        </form>        
    </div>
</main>
@endsection