@extends('admin.partials.layout')
@section('content')
<main class="o-page__content">
    @if(\Session::has('message'))
        <div class="alert alert-success" role="alert">
            <p> <i class="fa fa-check-circle-o"> </i> {{Session::get('message')}} </p>
        </div>
    @endif
    <div class="col-3">
        <form action="{{url(config('app.admin_prefix').'/update_categories/save')}}" method="post">
        {{ csrf_field() }}
        <div class="container">
            <label for="exampleInputEmail1">Название Категории</label>
            <input type="text" name="name" class="form-control mt-1" id="name" value="{{$data->name}}">
            <input type="hidden" name="id" value="{{$data->id}}">
            <input type="submit" class="btn btn-success mt-3" value="Изменить">    
        @if (count($errors)) 
        <div class="form-group">
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach    
                </ul>
            </div>
        </div>
        @endif
        </div>    
        </form>        
    </div>
</main>

@endsection