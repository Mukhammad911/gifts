@extends('admin.partials.layout')
@section('content')
<main class="o-page__content">
<div class="container-fluid">
    <div class="card mb-4 wow fadeIn">
        <div class="card-body d-sm-flex justify-content-between">
            <h4 class="mb-2 mb-sm-0 pt-1">
                Новости
            </h4>    
            <form class="d-flex justify-content-center">
                <div class="col-auto  mr-auto">
                    <a href="{{url(config('app.admin_prefix').'/add_news')}}" class="btn btn-success">
                    <i class="fa fa-plus"></i>    Добавить новость
                    </a>
                </div>
                
            </form>
        </div>
        <div class="container">
        <table class="table table-striped table-bordered data-table">
            
            <thead>
            <tr>
                <th scope="col"><strong>№</strong></th>     
                <th scope="col"><strong>Тема</strong></th>
                <th   scope="col"><strong>Новость</strong></th>
                <th scope="col"><strong>Картинка</strong></th>
                <th scope="col"><strong>Действие</strong></th>
            </tr>
            </thead>
            <tbody>
            @foreach($news as $new)
            <tr>
                <th scope="row">{{$new->id}}</th>
                <td>{{$new->theme}}</td>
                <td>{{$new->news}}</td>
                <td>
                    <img src="{{asset('images/news/'.$new->img)}}" style="width:250px; height:140px;"  alt="">
                </td>
                <td>
                <a class="btn btn-warning btn-sm text-white" 
                   href="{{url(config('app.admin_prefix').'/news/update/'.$new->id)}}">
                <i class="fa fa-trash-o"></i> Изменить</a>
                <br/>
                <a class="btn btn-danger btn-sm text-white mt-2" 
                   href="{{url(config('app.admin_prefix').'/news/delete/'.$new->id)}}">
                <i class="fa fa-trash-o"></i> Удалить</a>
                </td>
            </tr>
            @endforeach
            </tbody>
            
        </table>
        </div>
    </div>              
</div>      
</main>
<script>
    $(function () {
        $('.data-table').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': true
        });
    });

</script>

@endsection