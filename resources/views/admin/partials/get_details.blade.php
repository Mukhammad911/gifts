@extends('admin.partials.layout')
@section('content')
<style>
	#customDropdown{
		cursor:pointer;
	}
    a {
        color: #222C3C;
        font-family: helvetica;
        text-decoration: none;
    }
    a:hover{
        text-decoration:none;
    }
	#dropContainer{
		position:fixed;
		top: 11rem;
		left: 32rem;
		overflow-y: auto;
		max-height:25rem;
		width:19rem;
		background-color:#AAB0F7;
		display:none;
		text-align:left;
		padding-left:5px;
		padding-right:5px;
	}
</style>
    <main class="o-page__content">
    @if(\Session::has('message'))
        <div class="alert alert-success" role="alert">
            <p> <i class="fa fa-check-circle-o"> </i> {{Session::get('message')}} </p>
        </div>
    @endif
    
        <div class="col-12">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>№</th>
                    <th>Название продукта</th>
                    <th>Деталь</th>
                    <th>Значение</th>
                    <th>Создано</th>
                    <th>Обновлено</th>
                    <th>Операции</th>
                </tr>
                </thead>
                <tbody>
                @if(count($details) != 0)
                    @foreach($details as $detail)
                        <tr>
                            <td>{{$detail->id}}</td>
                            <td>{{App\Products::find($detail->products_id)->name}}</td>
                            <td>{{$detail->name}}</td>
                            <td>
                                {{$detail->value}} 
                            </td>
                            <td>{{$detail->created_at}}</td>
                            <td>{{$detail->updated_at}}</td>
                            <td>
                                <a class="btn btn-warning btn-sm text-white p-1"
                                   href="{{url(config('app.admin_prefix').'/product-details/update/'.$detail->id)}}">
                                <i class="fa fa-edit"></i> Обновить</a>
                                
                                <a class="btn btn-danger btn-sm text-white p-1"
                                   href="{{url(config('app.admin_prefix').'/product-details/delete/'.$detail->id)}}">
                                <i class="fa fa-trash-o"></i> Удалить</a>
                            </td>
                        </tr>
                @endforeach
                @endif
                <tbody>
            </table>
            <br/>
    </div>
        </div>
        <div class="col-3">
            <form action="{{url(config('app.admin_prefix').'/add-details/')}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="container">
                    <label for="exampleInputEmail1">Название Детали</label>
                    <input type="text" name="detail_name" class="form-control mt-1" id="name">

                    <label for="exampleInputEmail1" class="mt-2">Значение Детали<span class="text-danger">*</span></label><br/>
                    
                    <input required type="text" name="detail_value" class="form-control mt-1" id="value"/>

                    <div id="customDropdown" class="btn btn-success pt-2 mt-3">
                            <span class="valueHolder">Выбрать из шаблона</span>
                    </div> 
                   
                    <input type="hidden" name="product_id" value="{{$products->id}}">
                        <div id="dropContainer">
                            @if(isset($template_details))
                                @foreach($template_details as $template)
                                    <h4><a href="{{url(config('app.admin_prefix').'/get_templates/'.$template->id.'/'.$products->id)}}"><div class="dropOption ml-5">{{$template->name}}</div></a></h4>
                                @endforeach
                            @endif
                        </div>
                    <input type="submit" class="btn btn-success mt-3 mb-2" value="Добавить">
                    @if (count($errors))
                        <div class="form-group">
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{$error}}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                </div>
            </form>
        </div>
    </main>
    <script>
$(document).ready(function(){
	$('#customDropdown').on('click', function(event){
		var container = $('#dropContainer');
		var drop = $('#customDropdown');
		var target = $(event.target);

		if(target.hasClass('valueHolder') || target.attr('id') === 'customDropdown'){
			container.fadeToggle();
		}else if(target.hasClass('dropOption')){
			drop.find('span.valueHolder').text(target.text());
			container.hide();
		}
	});
    $(document).mouseup(function (e){
		var div = $("#dropContainer"); 
		if (!div.is(e.target) 
		    && div.has(e.target).length === 0) { 
			div.hide();
		}
	});
});
</script>
@endsection