@extends('admin.partials.layout')
@section('content')
<main class="o-page__content">
<div class="container-fluid">
    <div class="card mb-4 wow fadeIn">
    @if(\Session::has('message'))
        <div class="alert alert-success" role="alert">
            <p> <i class="fa fa-check-circle-o"> </i> {{Session::get('message')}} </p>
        </div>
    @endif
        <div class="card-body d-sm-flex justify-content-between">
            <h4 class="mb-2 mb-sm-0 pt-1">
                Категории
            </h4>    
            
            <form class="d-flex justify-content-center">
                <div class="col-auto  mr-auto">
                    <a href="{{url(config('app.admin_prefix').'/add_categories')}} " class="btn btn-success">
                    <i class="fa fa-plus"></i>    Добавить категорию
                    </a>
                </div>
            </form>
        </div>
        <div class="container">
        <table class="table table-striped table-bordered data-table">
            
            <thead>
            <tr>
                <th scope="col"><strong>№</strong></th>     
                <th scope="col-md-4"><strong>Категория</strong></th>
                <th scope="col"><strong>Действие</strong></th>
            </tr>
            </thead>
            <tbody>
            @foreach($categories as $cat)
            <tr>
                <th scope="row">{{$cat->id}}</th>
                <td>{{$cat->name}}</td>
                <td>
                <a class="btn btn-warning btn-sm text-white"
                    href="{{url(config('app.admin_prefix').'/admincategories_update/'.$cat->id)}} "
                >
                <i class="fa fa-edit"></i> Изменить</a>
                <a class="btn btn-danger btn-sm text-white" 
                   href="{{url(config('app.admin_prefix').'/admincategories/delete/'.$cat->id)}}">
                <i class="fa fa-trash-o"></i> Удалить</a>
                </td>
            </tr>
            @endforeach
            </tbody>
            
        </table>
        </div>
    </div>              
</div>      
</main>
<script src="{{ asset('teleglobal\callcenter\bower_components\datatables.net\js\jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('teleglobal\callcenter\bower_components\datatables.net-bs\js\dataTables.bootstrap.min.js') }}"></script>
<script>
    $(function () {
        $('.data-table').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': true
        });
    });

</script>
@endsection