@extends('admin.partials.layout')
@section('content')
    <main class="o-page__content">
        <div class="container-fluid">
            <div class="card mb-4 wow fadeIn">
                @if(\Session::has('message'))
                    <div class="form-control bg-success col-6 text-white"> {{Session::get('message')}}</div>
                @endif
                <div class="card-body d-sm-flex justify-content-between">
                    <h4 class=" mb-sm-0">
                        Информация заказа
                    </h4>
                </div>
                <div class="container ">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th scope="col"><strong>№</strong></th>
                            <th scope="col"><strong>Цена</strong></th>
                            <th scope="col"><strong>Валюта</strong></th>
                            <th scope="col"><strong>Товары</strong></th>
                            <th scope="col"><strong>Статус</strong></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($orders as $order)
                            <tr>
                                <th scope="row">{{$order->id}}</th>
                                <td>{{$order->price}}</td>
                                <td>{{$order->currency}}</td>
                                <td>{{App\Products::where('id', '=', $order->products_id )->first()->name}}</td>
                                <td>{{$order->status}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </main>

@endsection