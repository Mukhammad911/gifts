@extends('admin.partials.layout')
@section('content')
<main class="o-page__content">
<div class="container-fluid">
    <div class="card mb-4 wow fadeIn">
    
    @if(\Session::has('message'))
    <div class="alert alert-success" role="alert">
        <p> {{Session::get('message')}}</p>
    </div>
    @endif
    
        <div class="card-body d-sm-flex justify-content-between">
            <h4 class="mb-2 mb-sm-0 pt-1">
                Сообщении
            </h4>    
        </div>
        <div class="container">
        <table class="table table-striped table-bordered data-table">
            <thead>
                <tr>
                    <th scope="col" class="p-1"><strong>№</strong></th>     
                    <th scope="col-md-4" class="p-1"><strong>Имя</strong></th>
                    <th scope="col" class="p-1"><strong>Email</strong></th>
                    <th scope="col" class="p-1"><strong>Тема</strong></th>
                    <th scope="col" class="p-1"><strong>Сообщения</strong></th>
                    <th scope="col" class="p-1"><strong>Статус</strong></th>
                    <th scope="col" class="p-1"><strong>Изм. статус</strong></th>
                    <th scope="col" class="p-1"><strong>Действие</strong></th>
                </tr>
            </thead>
            <tbody>
            @foreach($feedbacks as $feedback)
                <tr>
                    <th scope="row" class="p-1">{{$loop->index+1}}</th>
                    <td class="p-1">{{$feedback->name}}</td>
                    <td class="p-1">{{$feedback->email}}</td>
                    <td class="p-1">{{$feedback->theme}}</td>
                    <td class="p-1">{{$feedback->message}}</td>
                    @if($feedback->status == 'panding')
                    <td class="p-1  btn-danger  text-white">{{$feedback->status}}</td>
                    @endif
                    @if($feedback->status == 'answered')
                    <td class="p-1 btn-warning text-white">{{$feedback->status}}</td>
                    @endif
                    @if($feedback->status == 'close')
                    <td class="p-1 btn-success text-white">{{$feedback->status}}</td>
                    @endif
                    <td class="p-1">
                    <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                        <div class="btn-group" role="group">
                            <button id="btnGroupDrop1" type="button" 
                                class="btn btn-secondary btn-sm dropdown-toggle text-white" 
                                data-toggle="dropdown" aria-haspopup="true" 
                                aria-expanded="false">
                            Поменять статус
                            </button>
                            <div class="dropdown-menu " aria-labelledby="btnGroupDrop1">
                                <a class="dropdown-item p-1" 
                                    href="{{url(config('app.admin_prefix').'/feedback/change_status/'.$feedback->id.'/panding')}}">
                                    Panding
                                </a>
                                <a  class="dropdown-item p-1"
                                    href="{{url(config('app.admin_prefix').'/feedback/change_status/'.$feedback->id.'/answered')}}">
                                    Answered
                                </a>
                                <a  class="dropdown-item p-1"
                                    href="{{url(config('app.admin_prefix').'/feedback/change_status/'.$feedback->id.'/close')}}">
                                    Close
                                </a>
                            </div>
                        </div>
                    </div>
                    </td>
                    <td class="p-1">
                    <a class="btn btn-danger btn-sm text-white" 
                    href="{{url(config('app.admin_prefix').'/feedbacks/delete/'.$feedback->id)}}">
                    <i class="fa fa-trash-o"></i> Удалить</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th scope="col" class="p-1"><strong>№</strong></th>     
                    <th scope="col-md-4" class="p-1"><strong>Имя</strong></th>
                    <th scope="col" class="p-1"><strong>Email</strong></th>
                    <th scope="col" class="p-1"><strong>Тема</strong></th>
                    <th scope="col" class="p-1"><strong>Сообщения</strong></th>
                    <th scope="col" class="p-1"><strong>Статус</strong></th>
                    <th scope="col" class="p-1"><strong>Изм. статус</strong></th>
                    <th scope="col" class="p-1"><strong>Действие</strong></th>
                </tr>
            </tfoot>
        </table>
        </div>
    </div>              
</div>      
</main>

<script>
    $(function () {
        $('.data-table').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': true
        });
    });

</script>


@endsection