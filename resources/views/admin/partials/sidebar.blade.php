<div class="o-page__sidebar js-page-sidebar">
    <div class="c-sidebar">
        <a class="c-sidebar__brand" href="#">
                Администратор
        </a>
        
        <h4 class="c-sidebar__title">Панель Управление</h4>
        <ul class="c-sidebar__list">
            <li class="c-sidebar__item">
                <a class="c-sidebar__link act {{ (request()->is(config('app.admin_prefix').'/admin_categories')) ? 'is-active' : '' }}
                {{ (request()->is(config('app.admin_prefix').'/add_categories')) ? 'is-active' : '' }}
                {{ (request()->is(config('app.admin_prefix').'/admincategories_update/')) ? 'is-active' : '' }}"
                    href="{{url(config('app.admin_prefix').'/admin_categories')}}">
                    <i class="fa fa-bars u-mr-xsmall"></i>Категории 
                </a>
            </li>

            <li class="c-sidebar__item">
                <a class="c-sidebar__link act {{ (request()->is(config('app.admin_prefix').'/admin_sub_categories')) ? 'is-active' : '' }}
                {{ (request()->is(config('app.admin_prefix').'/add_sub_categories')) ? 'is-active' : '' }}"
                    href="{{url(config('app.admin_prefix').'/admin_sub_categories')}}">
                    <i class="fa fa-align-center u-mr-xsmall"></i>Подкатегории
                </a>
            </li>

            <li class="c-sidebar__item">
                <a class="c-sidebar__link act {{ (request()->is(config('app.admin_prefix').'/admin_products')) ? 'is-active' : '' }}
                {{ (request()->is(config('app.admin_prefix').'/add_products')) ? 'is-active' : '' }}"
                    href="{{url(config('app.admin_prefix').'/admin_products')}}">
                    <i class="fa fa-bars u-mr-xsmall"></i>Товары
                </a>
            </li>

            
            <li class="c-sidebar__item">
                <a class="c-sidebar__link act {{ (request()->is(config('app.admin_prefix').'/admin_descriptions')) ? 'is-active' : '' }}"
                    href="{{url(config('app.admin_prefix').'/admin_descriptions')}}">
                    <i class="fa fa-info u-mr-xsmall"></i>Описание товара
                </a>
            </li>

           <li class="c-sidebar__item">
                <a class="c-sidebar__link act {{ (request()->is(config('app.admin_prefix').'/template_details')) ? 'is-active' : '' }}"
                    href="{{route('template_details.index')}} ">
                    <i class="fa fa-align-center u-mr-xsmall"></i>Шаблони деталей
                </a>
            </li>

            <li class="c-sidebar__item">
                <a class="c-sidebar__link act {{ (request()->is(config('app.admin_prefix').'/news')) ? 'is-active' : '' }}"
                    href="{{url(config('app.admin_prefix').'/news')}} ">
                    <i class="fa fa-picture-o u-mr-xsmall"></i>Новости
                </a>
            </li>

            <li class="c-sidebar__item">
                <a class="c-sidebar__link act {{ (request()->is(config('app.admin_prefix').'/banners')) ? 'is-active' : '' }}"
                    href="{{url(config('app.admin_prefix').'/banners')}} ">
                    <i class="fa fa-picture-o u-mr-xsmall"></i>Банеры
                </a>
            </li>
            
            <li class="c-sidebar__item">
                <a class="c-sidebar__link act {{ (request()->is(config('app.admin_prefix').'/brands')) ? 'is-active' : '' }}"
                    href="{{url(config('app.admin_prefix').'/brands')}} ">
                    <i class="fa fa-file-image-o u-mr-xsmall"></i>Топ бренды
                </a>
            </li>

            <li class="c-sidebar__item">
                <a class="c-sidebar__link act {{ (request()->is(config('app.admin_prefix').'/feedbacks')) ? 'is-active' : '' }}"
                    href="{{url(config('app.admin_prefix').'/feedbacks')}} ">
                    <i class="fa fa-reply u-mr-xsmall"></i>Обратная связь
                </a>
            </li>

            <li class="c-sidebar__item">
                <a class="c-sidebar__link act {{ (request()->is(config('app.admin_prefix').'/newsletters')) ? 'is-active' : '' }}"
                    href="{{url(config('app.admin_prefix').'/newsletters')}} ">
                    <i class="fa fa-rss u-mr-xsmall"></i>Рассылки
                </a>
            </li>

            <li class="c-sidebar__item">
                <a class="c-sidebar__link act {{ (request()->is(config('app.admin_prefix').'/orders')) ? 'is-active' : '' }}"
                    href="{{url(config('app.admin_prefix').'/orders')}}">
                    <i class="fa fa-shopping-cart u-mr-xsmall"></i>Заказы
                </a>
            </li>

            <li class="c-sidebar__item">
                <a class="c-sidebar__link act {{ (request()->is(config('app.admin_prefix').'/reports')) ? 'is-active' : '' }}"
                    href="{{url(config('app.admin_prefix').'/reports')}}">
                    <i class="fa fa-database u-mr-xsmall"></i>Отчеты
                </a>
            </li>

            
        </ul>

    </div><!-- // .c-sidebar -->
</div><!-- // .o-page__sidebar -->
