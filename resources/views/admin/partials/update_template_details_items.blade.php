@extends('admin.partials.layout')
@section('content')
<main class="o-page__content">
    @if( Session::has('message'))
        <div class="alert alert-success" role="alert">
            <p style="font-size:2rem;">{{$success_message=Session::get('message')}}</p>
        </div>
    @endif
    <div class="col-3">
        <form action="{{route('template_details_items.update',$data->id)}}" method="post">
        {{ csrf_field() }}
        {{ method_field('PATCH') }}
        <div class="container">
            <label for="template_key">Название Детали</label>
            <input type="text" name="template_key" class="form-control mt-1" id="template_key" value="{{$data->key}}" required>
            <label for="template_vlaue">Значение Детали</label>
            <input type="text" name="template_value" class="form-control mt-1" id="template_value" value="{{$data->value}}">
            <label for="template">Категории</label>
            <input type="text" name="template" class="form-control mt-1" id="template" disabled
                value="{{App\Template_details::find($data->template_details_id)->name}}">
            <input type="submit" class="btn btn-success mt-3" value="Изменить">    
        @if (count($errors)) 
        <div class="form-group">
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach    
                </ul>
            </div>
        </div>
        @endif
        </div>    
        </form>        
    </div>
</main>
@endsection