/**
 * Constants
 */
var client_phone_number =       '#client_phone_number';
var client_name_p =             '.client_name_p';

var status_of_client =          '#status_of_client';

var status_of_client_loader =   undefined;



function Helper(){
    this.client_base_id      = undefined;
    this.phone               = undefined;
    this.base                = undefined;
    this.fullname            = undefined;

    this.active              = false;
}

Helper.prototype.init = function(client_base_id, phone, base, fullname, status_modal, callback){
    if(!this.active){

        if(!this.clearConstants()){
            return;
        }

        if(!this.setConstants(client_base_id, phone, base, fullname)){
            return;
        }

        if(status_modal){
            this.showModal();
        }

        this.setHeaderView()
        callback(this);
    }
    else{

    }
}
Helper.prototype.setHeaderView = function(){
    $(client_phone_number).text(' ').text('('+this.phone+')');
    $(client_name_p).text(' ').text(this.fullname);
}

Helper.prototype.requestByMethod = function (url, method, loader_info, data, callback_success) {
    return $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: url,
        method: method,
        dataType: 'json',
        cache: false,
        data: data,
        timeout: 600000,
        beforeSend: function () {
            Common.prototype.mainLoaderOpen.call(this,loader_info);
            Common.prototype.statusLoaderOpen.call(this);
        }.bind(this),
        success: function (response) {
            Common.prototype.mainLoaderClose.call(this);
            Common.prototype.statusLoaderClose.call(this);
            callback_success(response);
        }.bind(this),
        error: function (xhr) {
            console.log(xhr);
            Common.prototype.mainLoaderClose.call(this);
            Common.prototype.statusLoaderClose.call(this);

            if(xhr.hasOwnProperty('responseJSON')){

                if(xhr.hasOwnProperty('responseJSON')){
                    if(xhr.responseJSON.hasOwnProperty('message')){
                        if(xhr.responseJSON.message.hasOwnProperty('message')){
                            Helper.prototype.notifyError.call(this, xhr.responseJSON.message.message);
                        }
                    }
                }

                if(xhr.hasOwnProperty('responseJSON')){
                    if(xhr.responseJSON.hasOwnProperty('message')){
                        if(xhr.responseJSON.message.hasOwnProperty('code')){
                            if(xhr.responseJSON.message.code=="wrongpassword"){
                                $.removeCookie('password_diler_sunduk', { path: '/base' });
                                $(sunduk_password_ui).show();
                                Helper.prototype.notifyError.call(this, 'Неверный пароль от Sunduk.tv. Введите корректный пароль от Sunduk.tv');
                            }
                        }
                    }
                }

                Helper.prototype.notifyError.call(this, xhr.responseJSON.message);

                return;
            }

            else if(typeof xhr.status != 'undefined' && typeof xhr.status == 200){
                Helper.prototype.notifySuccess.call(this);
                return;
            }

        }.bind(this)
    });
};

Helper.prototype.requestPut = function (url, data, var_url, callback) {
    return $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: url + var_url,
        type: 'PUT',
        data: data,
        beforeSend: function () {
            Common.prototype.statusLoaderOpen.call(this);
        }.bind(this),
        success: function(data) {
            Common.prototype.statusLoaderClose.call(this);
            callback(data);
        },
        error:function(xhr){
            console.error(xhr);
            Common.prototype.statusLoaderClose.call(this);
            if(xhr.hasOwnProperty('responseJSON')){
                Helper.prototype.notifyError.call(this, xhr.responseJSON.message);

                if(xhr.hasOwnProperty('responseJSON')){
                    if(xhr.responseJSON.hasOwnProperty('message')){
                        if(xhr.responseJSON.message.hasOwnProperty('message')){
                            Helper.prototype.notifyError.call(this, xhr.responseJSON.message.message);
                        }
                    }
                }

                if(xhr.hasOwnProperty('responseJSON')){
                    if(xhr.responseJSON.hasOwnProperty('message')){
                        if(xhr.responseJSON.message.hasOwnProperty('code')){
                            if(xhr.responseJSON.message.code=="wrongpassword"){
                                $.removeCookie('password_diler_sunduk', { path: '/base' });
                                $(sunduk_password_ui).show();
                                Helper.prototype.notifyError.call(this, 'Неверный пароль от Sunduk.tv. Введите корректный пароль от Sunduk.tv');
                            }
                        }
                    }
                }

                return;
            }

            if(typeof xhr.status != 'undefined' && typeof xhr.status == 200){
                Helper.prototype.notifySuccess.call(this);
                return;
            }
        }
    });
};

Helper.prototype.showModal = function(){
    $('#modal-call').modal('show');
}

Helper.prototype.clearConstants = function(){
    this.client_base_id =     undefined;
    this.phone =              undefined;
    this.base  =              undefined
    this.fullname =           undefined;

    return true;
}

Helper.prototype.setConstants = function(client_base_id, phone, base, fullname){

    this.client_base_id =     client_base_id;
    this.phone =              phone;
    this.base =               base;
    this.fullname=            fullname;

    if(typeof this.client_base_id == 'undefined' || typeof this.phone == 'undefined' || typeof this.base == 'undefined' || typeof this.fullname == 'undefined'){
        return false;
    }

    status_of_client = '#status_of_client_'+ client_base_id;

    status_of_client_loader = '#status_of_client_loader_'+ client_base_id;

    return true;
}

Helper.prototype.getConstants = function(){
    return this;
}

function tick () {
    /*diff_sec++;
    sec++;
    if (sec < 60) {
        $('.seconds').empty().append(' ' + sec);
    } else {
        min++;
        if (min < 60) {
            sec = 0;
            $('.seconds').empty().append(' ' + sec);
            $('.minutes').empty().append(' ' + min + ' ');
        } else {
            hour++;
            sec = 0;
            min = 0;
            $('.seconds').empty().append(' ' + sec);
            $('.minutes').empty().append(' ' + min + ' ').empty().append(hour + ' ');
        }
    }*/
};

Helper.prototype.notifySuccess = function(message) {
    $.jGrowl(message, {
        header: '<img src="/images/icon-success.png"><span>Успешно!</span>',
        theme: 'bg-success',
        life: 8000,
        position: 'bottom-right'
    });
}

Helper.prototype.notifyError = function(message, exception) {
    $.jGrowl(message, {
        header: '<img src="/images/icon-error.png"><span>Ошибка!<span>',
        theme: 'bg-danger',
        life: 8000,
        position: 'bottom-right'
    });
}

var helper  = new Helper();
