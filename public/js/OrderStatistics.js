/**
 * @author muhammad.yakubov91@gmail.com
 * @required DataTimePicker, ChartJS, DataTable
 */

/**
 * Varibale for call filter button
 */
var order_filter_btn = '.order-filter-btn';

/**
 * Varibale for call filter button
 */
var statistics_table = '#statistics-table';

var salary_table = '#salary-table';


/**
 * Varibale for call filter button
 */
var statistics_order_item = '.statistics-order-item';

var salary_item = '.salary-item';

/**
 * Varibale for call filter button
 */
var data_table = '.data_table';

/**
 * Variable for select base from history/index view
 */
var client_base = '.client_base';

/**
 * Variable for select status from history/index view
 */
var status_select = '#status_select';

var users_select = '#users_select';

// var line_chart              = 'line-chart';
// var donught_chart           = 'donught-chart';

/**
 * Variable for checkbox from history/index view
 */
var count_days = '#count_days';

/**
 * Variable for Line chart
 */
var lineChart = undefined;

/**
 * Variable for Donught chart
 */
var donughtChart = undefined;

/**
 * @description The class OrderStatistics() by default set properties and call init()
 *              function while creating new instance of this Class
 */
function OrderStatistics() {

    this.selected_base = undefined;
    this.start_date = undefined;
    this.end_date = undefined;
    this.status = undefined;
    this.users = undefined;
    this.count_by_days = undefined;

    this.chartShowed = false;
    this.chartShowedDonught = false;


    this.loadUsers(function () {
        this.init();
    });
}

OrderStatistics.prototype.init = function () {
    $(client_base).prop($(client_base + ' option:first').val());
    $(statistics_table).hide();
    // $(salary_table).hide();
}


OrderStatistics.prototype.loadUsers = function (callback) {
    /*Helper.prototype.requestByMethod.call(this, '/statistic/order-statistic-users', 'GET', 'Загрузка...', {
     }, function(response) {
     if (!response.code == "200") {
     return;
     }

     if (!response.hasOwnProperty('data')) {
     // $(users_select).append($("<option></option>").attr("value",response.data.id).text(response.data.name));
     return
     }
     var users = [{
     id : 0,
     title : "Все"
     }];
     if (!response.data.length) {
     users.push({
     id: response.data.id,
     title:response.data.name
     });
     users.splice(0, 1);
     }else{
     for(var i = 0; i < response.data.length; i++){
     users.push({
     id: response.data[i].id,
     title:response.data[i].name
     })
     }
     }
     comboTree = $('#multi_users').comboTree({
     source : users,
     isMultiple: true
     });

     $('#form_multi_users').on("submit",function(e){
     e.preventDefault();
     });

     }.bind(this));*/
}

/**
 * @description The function getCheckboxVal() get checked value or unchecked value from count by days checkbox
 */
OrderStatistics.prototype.getCheckboxVal = function () {
    if ($(count_days).is(":checked")) {
        $(count_days).val('on');
    } else {
        $(count_days).val('off');
    }
    return $(count_days).val();
}

/**
 * @description The function getSelectedData() get data from view forms
 */
OrderStatistics.prototype.getSelectedData = function () {

    this.start_date = $('#datetimepicker_date_start input').val();

    this.end_date = $('#datetimepicker_date_end input').val();

    this.status = $(status_select).val();

    if(typeof this.start_date == "undefined"){
        this.start_date = $('#datetimepicker_date_start').text();
    }

    if(typeof this.end_date == "undefined"){
        this.end_date = $('#datetimepicker_date_end').text();
    }

    if (this.start_date == undefined || this.start_date == "") {
        Helper.prototype.notifyError.call(this, 'Поля start date не должен быть пустым!')
        return false;
    }

    if (this.end_date == undefined || this.end_date == "") {
        Helper.prototype.notifyError.call(this, 'Поля end date не должен быть пустым!')
        return false;
    }

    if (this.status == undefined || this.status == "") {
        Helper.prototype.notifyError.call(this, 'Поля статус не должен быть пустым')
        return false;
    }

    return true;
}

/**
 * @description The function loadData() load data from ajax
 */
OrderStatistics.prototype.loadData = function () {

    if (!this.getSelectedData()) {
        return;
    }

    $(statistics_table).DataTable().destroy();
    //$(salary_table).DataTable().destroy();

    $(statistics_order_item).remove();
    $(salary_item).remove();
    //$(salary_table).hide();
    $(data_table).hide();
    $(statistics_table).hide();

    Helper.prototype.requestByMethod.call(this, 'report-lists', 'GET', 'Загрузка...', {
        'type': 'statistics',
        'start_date': this.start_date,
        'end_date': this.end_date,
        'status': this.status,
    }, function (response) {
        if (!response.code == "200") {
            return;
        }

        if (!response.hasOwnProperty('data') || !response.data.length) {
            Helper.prototype.notifySuccess.call(this, response.message);
            return
        }

        for (var i = 0; i < response.data.length; i++) {
            $(statistics_table + ' tbody').append(this.boxData(
                response.data[i].id,
                response.data[i].name,
                response.data[i].surname,
                response.data[i].address,
                response.data[i].email,
                response.data[i].phone,
                response.data[i].otherPhone,
                response.data[i].city,
                response.data[i].delivery,
                response.data[i].status,
                response.data[i].korti_milli,
                response.data[i].price,
                response.data[i].product_name,
                response.data[i].created_at));

        }

        $(statistics_table).show();
        //$(salary_table).show();
        $(data_table).show();

        this.setDataTables();
        this.generateLineChart(response.graph.labels, response.graph.counts);
        this.generateDonughtChart(response.status);

    }.bind(this));
}

OrderStatistics.prototype.setDataTables = function () {
    $(statistics_table).DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
}

/**
 *
 * @description The function generate Line chart
 * @param label
 * @param data
 */
OrderStatistics.prototype.generateLineChart = function (label, data) {

    var ctx = document.getElementById('line-chart').getContext("2d");


    lineChart = new Chart(ctx).Line(this.genereteObjectChart(label, data), {
        responsive: true
    });

    this.chartShowed = true;
}

/**
 * @description The function generate Donught chart
 * @param data
 */
OrderStatistics.prototype.generateDonughtChart = function (data) {

    var ctx = document.getElementById("donught-chart").getContext("2d");

    if (this.chartShowedught) {
        donughtChart.destroy();
    }

    donughtChart = new Chart(ctx).Doughnut(this.genereteObjectDonughtChart(data), {
        segmentShowStroke: true,
        segmentStrokeColor: "#fff",
        segmentStrokeWidth: 2,
        percentageInnerCutout: 50,
        animationSteps: 100,
        animationEasing: "easeOutBounce",
        animateRotate: true,
        animateScale: false,
        responsive: true,
        maintainAspectRatio: false,
        showScale: true,
        animateScale: true,
        labelFontFamily: "Arial",
        labelFontStyle: "normal",
        labelFontSize: 24,
        labelFontColor: "#666"
    });

    this.chartShowedught = true;

}

/**
 * @description The function set data in Line chart
 * @param label
 * @param data
 */
OrderStatistics.prototype.genereteObjectChart = function (label, data) {

    var lineChartData = {
        labels: label,
        datasets: [{
            label: "My First dataset",
            fillColor: "rgba(220,220,220,0.2)",
            strokeColor: "rgba(220,220,220,1)",
            pointColor: "rgba(220,220,220,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(220,220,220,1)",
            data: data
        }]
    }

    return lineChartData;
}

/**
 *
 * @description The function get random color for Donught chart
 */
OrderStatistics.prototype.getRandomColor = function () {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

/**
 *
 * @description The function set data in Donught chart
 * @param data
 */
OrderStatistics.prototype.genereteObjectDonughtChart = function (data) {
    var doughnutData = [];
    var result = [];

    for (var i in data) {
        result.push([i, data[i]]);
    }

    for (var j = 0; j < result.length; j++) {
        doughnutData.push({
            value: result[j][1],
            color: this.getRandomColor(),
            highlight: this.getRandomColor(),
            label: result[j][0]
        })
    }

    return doughnutData;
}

/**
 * @description The function set data in datatable
 * @param id
 * @param client_base_id
 * @param fullname
 * @param phone
 * @param base
 * @param user_id
 * @param amount
 * @param username
 * @param reseller_id
 * @param status
 * @param created
 * @param updated
 */
OrderStatistics.prototype.boxData = function (id, name, surname, address, email, phone, otherPhone, city, delivery, status, korti_milli, price, product_name, created_at) {
    return '<tr class="statistics-order-item">' +
        '<td>' + id + '</td>' +
        '<td>' + name + '</td>' +
        '<td>' + surname + '</td>' +
        '<td>' + address + '</td>' +
        '<td>' + email + '</td>' +
        '<td>' + phone + '</td>' +
        '<td>' + otherPhone + '</td>' +
        '<td>' + city + '</td>' +
        '<td>' + delivery + '</td>' +
        '<td>' + status + '</td>' +
        '<td>' + korti_milli + '</td>' +
        '<td>' + price + '</td>' +
        '<td>' + product_name + '</td>' +
        '<td>' + created_at + '</td>' +
        '</tr>'
}

OrderStatistics.prototype.boxDataSalary = function (id, username, salary_operator, salary_moderator) {
    return '<tr class="salary-item">' +
        '<td>' + id + '</td>' +
        '<td>' + parseFloat(salary_operator).toFixed(2) + ' EUR</td>' +
        '<td>' + parseFloat(salary_moderator).toFixed(2) + ' EUR</td>' +
        '</tr>'
}


$(function () {

    var order_statistics = new OrderStatistics();

    /**
     * Event click for CallFilterButton
     */
    $(document).on('click', order_filter_btn, function (e) {
        e.preventDefault();
        order_statistics.loadData();
    })
})