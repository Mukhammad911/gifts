var main_loader =               '.main_loader';
var main_loader_p =             '.main_loader_p';
var statistics_loader        =  '.statistics-loader'

function Common() {}

Common.prototype.changeStatusCall = function(status){
    Helper.prototype.requestPut.call(this,'/api/calls/','status='+status, call_id, function(){})
}

Common.prototype.setClientDataFront = function (client_id, real_phone, fullname, base, type) {

    Helper.prototype.requestByMethod.call(this,'/api/processing/'+ client_id, 'GET', 'Получение токена...',{
        'type': 'client_base_id',
        'base': base,
    },function(response){
        if(!response.token){
            return;
        }

        switch(type){
            case 'delete':
                Helper.prototype.requestPut.call(this,'/api/processing/', 'status='+'deleted', response.token, function(response){
                    if(response.client_id){
                        Common.prototype.changeClientStatusDOM.call(this, 'Удален');
                        Helper.prototype.notifySuccess.call(this, "Статус клиента успешно изменена!");
                    }
                })

                break;

            case 'in_processed':
                Helper.prototype.requestPut.call(this,'/api/processing/', 'status='+'in_processing', response.token, function(response){
                    if(response.client_id){
                        Common.prototype.changeClientStatusDOM.call(this, 'В обработке');
                        Helper.prototype.notifySuccess.call(this, "Статус клиента успешно изменена!");
                    }
                })

                break;

            case 'missed':
                Helper.prototype.requestPut.call(this,'/api/processing/', 'status='+'missed', response.token, function(response){
                    if(response.client_id){
                        Common.prototype.changeClientStatusDOM.call(this, 'Недозвон');
                        Helper.prototype.notifySuccess.call(this, "Статус клиента успешно изменена!");
                    }
                })

            case 'ding_dong':
                Helper.prototype.requestPut.call(this,'/api/processing/', 'status='+'ding_dong', response.token, function(response){
                    if(response.client_id){
                        Common.prototype.changeClientStatusDOM.call(this, 'Перезвон');
                        Helper.prototype.notifySuccess.call(this, "Статус клиента успешно изменена!");
                    }
                })

                break;

            case 'rejected':
                Helper.prototype.requestPut.call(this,'/api/processing/', 'status='+'rejected', response.token, function(response){
                    if(response.client_id){
                        Common.prototype.changeClientStatusDOM.call(this, 'Предложение отклонено');
                        Helper.prototype.notifySuccess.call(this, "Статус клиента успешно изменена!");
                    }
                })

                break;

            case 'failed':
                Helper.prototype.requestPut.call(this,'/api/processing/', 'status='+'failed', response.token, function(response){
                    if(response.client_id){
                        Common.prototype.changeClientStatusDOM.call(this, 'Ошибка обработки');
                        Helper.prototype.notifySuccess.call(this, "Статус клиента успешно изменена!");
                    }
                })

                break;

            case 'completed':
                Helper.prototype.requestPut.call(this,'/api/processing/', 'status='+'completed', response.token, function(response){
                    if(response.client_id){
                        Common.prototype.changeClientStatusDOM.call(this, 'Обработка завершена');
                        Helper.prototype.notifySuccess.call(this, "Статус клиента успешно изменена!");
                    }
                })

                break;

            default:
                console.log("default");
                break;
        }
    }.bind(this));
};

Common.prototype.enableForm = function(el1, el2, el3){
    $(client_navbar).removeClass('active').removeClass('in');
    $(client_top_navbar).removeClass('active');
    $(client_tab).removeClass('active');

    $(client_register).removeClass('active').removeClass('in');
    $(client_top_navbar_register).removeClass('active');
    $(register_client_tab).removeClass('active');

    $(client_top_navbar_test_subs).removeClass('active').removeClass('in');
    $(test_subscriber).removeClass('active');
    $(test_subscriber_tab).removeClass('active');

    $(client_top_navbar_order_subs).removeClass('active').removeClass('in');
    $(order_subscriber).removeClass('active');
    $(order_subscriber_tab).removeClass('active');

    $(client_top_navbar_comment_subs).removeClass('active').removeClass('in');
    $(comment_subscriber).removeClass('active');
    $(comment_subscriber_tab).removeClass('active');

    $(client_top_navbar_calendar_subs).removeClass('active').removeClass('in');
    $(calendar_subscriber).removeClass('active');
    $(calendar_subscriber_tab).removeClass('active');

    $(client_top_navbar_history_test_subs).removeClass('active').removeClass('in');
    $(history_test_subscriber).removeClass('active');
    $(history_test_subscriber_tab).removeClass('active');

    $(client_top_navbar_record_subs).removeClass('active').removeClass('in');
    $(record_subscriber).removeClass('active');
    $(record_subscriber_tab).removeClass('active');

    $(el1).addClass('active').addClass('in');
    $(el2).addClass('active');
    $(el3).addClass('active');
}

Common.prototype.refreshTimer = function () {
    sec = 0;
    min = 0;
    hour = 0;
};


Common.prototype.closeCallingLoader = function () {
    $(calling_loader).hide();
};

Common.prototype.openTimer = function () {
    $(timer_call).css({display: 'inline-block'});
    $('.calling_loader').show();
};

Common.prototype.closeTimer = function () {
    $(timer_call).hide();
    $('.calling_loader').hide();
};

Common.prototype.statusLoaderOpen = function () {
    $(status_of_client).hide();
    $(status_of_client_loader).show();
};

Common.prototype.statusLoaderClose = function () {
    $(status_of_client).show();
    $(status_of_client_loader).hide()
};

Common.prototype.mainLoaderOpen = function (loader_info) {
    $(main_loader).show();
    $(main_loader_p).show().text(' ');
    $(main_loader_p).text(loader_info);
    $(statistics_loader).show();
};

Common.prototype.mainLoaderClose = function () {
    $(main_loader).hide();
    $(main_loader_p).hide().text(' ');
    $(statistics_loader).hide();
};

Common.prototype.topLoaderOpen = function (loader_info) {
    $(calling_top_loader).css({
        display: "inline-block"
    });
    $(calling_top_loader_p).css({
        display: "inline-block"
    }).text(' ');
    $(calling_top_loader_p).text(loader_info);
};

Common.prototype.disableButtons = function(){
    $('.call-action-btn').prop('disabled', true);
}

Common.prototype.enableButtons = function(){
    $('.call-action-btn').prop('disabled', false);
}

Common.prototype.topLoaderClose = function () {
    $(calling_top_loader).hide();
    $(calling_top_loader_p).hide().text(' ');
};

Common.prototype.saveStatusCallData = function(status, description='', time_elapsed, timer = 'enabled'){
    if(typeof(time_elapsed)=='undefined'){
        time_elapsed = 0;
    }

    Helper.prototype.requestByMethod.call(this, '/api/calls-data/create','POST', '', {
        'call_id' : call_id,
        'token' : token,
        'status':status,
        'description_status':description,
        'time_elapsed':time_elapsed
    }, function(data){
        empty_data.push(data)

        if(interval){
            clearInterval(interval);
            if(timer==='disable'){

            }
            else{
                diff_sec = 0;
            }
        }

        if(timer === 'disable'){

            var type =undefined;

            for(var j=0;j<data_check.length;j++){

                if(data_check[j].count == empty_data.length){
                    var checking = 0;
                    for(var p=0;p<data_check[j].count;p++){

                        if(data_check[j].data[p]==empty_data[p]){
                            checking++;
                        }

                        if(checking==data_check[j].count){
                            type = data_check[j].type;
                            break;
                        }
                    }
                }
            }
            if(typeof(type)=='undefined'){
                Common.prototype.changeStatusCall.call(this,'failed');
                Common.prototype.changeClientStatusDOM.call(this, 'Ошибка обработки');
            }
            else{
                switch(type){
                    case 1:
                        //Проверяем оператор больше 15 сек разговаривал
                        if(diff_sec > 10){
                            Common.prototype.changeStatusCall.call(this,'completed');
                            Common.prototype.changeClientStatusDOM.call(this, 'Обработка завершена');
                        }

                        /* else{
                         Common.prototype.changeStatusCall.call(this,'failed');
                         Common.prototype.changeClientStatusDOM.call(this, 'Перезвон');
                         } */
                        break;

                    case 2:
                        //Проверяем оператор больше 15 сек разговаривал
                        /* if(diff_sec > 10){
                         Common.prototype.changeStatusCall.call(this,'completed');
                         Common.prototype.changeClientStatusDOM.call(this, 'Обработка завершена');
                         }else{
                         Common.prototype.changeStatusCall.call(this,'failed');
                         Common.prototype.changeClientStatusDOM.call(this, 'Перезвон');
                         } */
                        break;
                    case 3:
                        Common.prototype.changeStatusCall.call(this,'missed');
                        break;

                    case 5:
                        /*   Common.prototype.changeStatusCall.call(this,'failed');
                         Common.prototype.changeClientStatusDOM.call(this, 'Ошибка обработки');*/
                        break;
                    case 6:
                        /* Common.prototype.changeStatusCall.call(this,'missed');
                         Common.prototype.changeClientStatusDOM.call(this, 'Недозвон');*/
                        break;
                    default:
                        /*  Common.prototype.changeStatusCall.call(this,'failed');
                         Common.prototype.changeClientStatusDOM.call(this, 'Ошибка обработки');*/
                        break;
                }
            }

            empty_data=[];
            diff_sec = 0;
        }
        else{

            interval = setInterval(function(){
                tick();
            }, 1000);

        }

    });
}

Common.prototype.changeClientStatusDOM = function(status){
    for(var i=0; i<status_style.length; i++){
        if(status_style[i].name == status){
            $(status_of_client).removeClass();
            $(status_of_client).addClass('label').addClass(status_style[i].style);
            $(status_of_client).text(status);
        }
    }
}