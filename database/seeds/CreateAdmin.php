<?php

use App\User;
use BntAr\Roles\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class CreateAdmin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $user = User::updateOrCreate([
            'email' => 'administrator@gmail.com',
        ],
            [
                'name' => 'Administrator',
                'email' => 'administrator@gmail.com',
                'password' => Hash::make('Admin#2020'),
            ]);

        $user->attachRole(
            Role::where( 'slug', 'admin' )->first()
        );
    }
}
