<?php

use BntAr\Roles\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'name' => 'Admin',
            'slug' => 'admin',
            'description' => '', // optional
            'level' => 2, // optional, set to 1 by default
        ]);

        Role::create([
            'name' => 'Client',
            'slug' => 'client',
            'level' => 1,
        ]);
    }
}
