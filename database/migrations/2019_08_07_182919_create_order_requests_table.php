<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('surname');
            $table->string('address');
            $table->string('email');
            $table->string('phone');
            $table->string('otherPhone')->nullable();
            $table->string('city')->nullable();
            $table->string('delivery');
            $table->enum('status', ['cart', 'complete', 'boxing', 'canceled', 'paid', 'payment', 'preformed', 'shipping'])->nullable();
            $table->string('korti_milli')->nullable();
            $table->string('signature')->nullable();
            $table->string('status_onlinepay')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_requests');
    }
}
