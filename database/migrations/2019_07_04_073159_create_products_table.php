<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->decimal('price', 10, 2);
            $table->decimal('old_price', 10, 2)->nullable();
            $table->string('brand')->nullable();
            $table->string('discount')->nullable();
            $table->enum('onsale', [0, 1])->default(0);
            $table->enum('feature', [0, 1])->default(0);
            $table->enum('new', [0, 1])->default(0);
            $table->enum('bestseller', [0, 1])->default(0);
            $table->enum('available', [0, 1])->default(1);
            $table->string('img')->nullable();
            $table->string('description', 2000)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
