<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->decimal('price',10,2 );
            $table->enum('currency', ['TJS', 'EUR', 'USD', 'RUB']);
            $table->string('session_order_id');
            $table->enum('status', ['cart', 'complete', 'boxing', 'canceled', 'paid', 'payment', 'preformed', 'shipping']);
            $table->biginteger('products_id')->unsigned()->index()->nullable();
            $table->biginteger('order_requests_id')->unsigned()->index()->nullable();
            $table->foreign('products_id')->references('id')->on('products')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
