<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

Auth::routes();

Route::group([
    'middleware' => ['web', 'cookie_order'],
], function () {

    Route::get('/', 'Client\CategoriesController@read');
//Route::get('/', 'Client\ProductsController@getProducts');


    Route::get('/home', 'HomeController@index')->name('home');


    Route::get('news-more/{id}', 'Client\NewsCommentsController@newsComments');

    Route::post('add-news-comment', 'Client\NewsCommentsController@addComment');

    Route::get('news-comment/delete/{id}', 'Client\NewsCommentsController@remove');

    /**
    *Client NewsController
    */

    Route::get('news', 'Client\NewsController@getNewsPage');
    
    Route::get('category/{id}', 'Client\ProductsController@getProducts');

    Route::post('filter', 'Client\FilterController@getFilter')->name('filter');

    Route::get('product/show/{id}', 'Client\DescriptionsController@getDescription');

    Route::post('product/show/add/comment', 'Client\CommentsController@create');

    Route::post('/add/newsletter/', 'Client\NewslettersController@addEmail');

    Route::post('/send', 'Client\FeedbacksController@feedback' );

    Route::post('product-mark', 'Client\MarksController@setMark');

    Route::get('authentication', 'AuthController@auth');

    Route::get('/search', 'Client\CategoriesController@search');

    Route::get('/comment/delete/{id}', 'Client\CommentsController@remove');

    Route::get('/faqs', 'Client\FaqsController@faqs');

    Route::get('/about', 'Client\AboutController@about');

    Route::get('/delivery', 'Client\DeliveryController@delivery');

    Route::get('/pay', 'Client\PayController@pay');

    Route::get('/contact', 'Client\ContactController@contact');

    Route::get('/discount', 'Client\DiscountController@discount');

    Route::get('/feature', 'Client\FeatureController@feature');

    Route::get('/bestsellers', 'Client\BestsellerController@bestsellers');

    Route::get('/send', 'Client\MailController@send');
    Route::get('time', function () {
        var_dump(session_get_cookie_params());
        exit;
    });

    //OrderController
    Route::post('order/cart-add-product', 'Client\OrderController@cartAddProduct')->name('cart.add.product');
    Route::get('remove/cart-order/{order_id}', 'Client\OrderController@removeCartOrder')->name('remove.cart.order');
    Route::get('page-cart', 'Client\OrderController@getPageCart')->name('get.page.cart');
    Route::get('order-product/plus/{product_id}', 'Client\OrderController@plusProductCart')->name('plus.product.cart');
    Route::get('order-product/minus/{product_id}', 'Client\OrderController@minusProductCart')->name('minus.product.cart');
    Route::get('order-product/remove-all/{product_id}', 'Client\OrderController@removeAllProductCart')->name('remove.product.cart');
    Route::get('order/billing-page', 'Client\OrderController@billingPage')->name('order.billing.page');
    Route::post('add-request-order', 'Client\OrderController@billingAddOrder')->name('add.request.order');

    Route::post('success', 'Client\OrderController@sucessorder');
    Route::post('fail', 'Client\OrderController@falseorder');
    Route::get('result', 'Client\OrderController@result');

});

    /*
     * Ajax templates
     */

    Route::get('get_template/{id}', 'Admin\TemplateDetailsController@ajaxTemplates');

    /*
     * Ajax SubCategories
     */
    Route::get('get_sub_categories/{id}', 'Admin\AdminSubCategoriesController@ajaxSubCategories');


Route::group([
    'prefix' => config('app.admin_prefix'),
    'middleware' => ['auth', 'level:2']
], function () {
    Route::get('admin', 'Admin\AdminCategoriesController@getcategories');
    Route::get('admin_categories', 'Admin\AdminCategoriesController@getcategories');
    Route::get('add_categories', 'Admin\AdminCategoriesController@add');
    Route::get('add_some_products', 'Admin\AdminProductsController@addSome');

    Route::post('/add_categories/save', 'Admin\AdminCategoriesController@create');
    Route::post('/add_some_products/save', 'Admin\AdminProductsController@createSome');

    Route::get('admincategories_update/{id}', 'Admin\AdminCategoriesController@getUpdate');
    Route::post('/update_categories/save', 'Admin\AdminCategoriesController@cat_update');
    Route::get('/admincategories/delete/{id}', 'Admin\AdminCategoriesController@remove');
    Route::get('admin_sub_categories', 'Admin\AdminSubCategoriesController@getsubcategories');
    Route::get('/adminsubcategories/update/{id}', 'Admin\AdminSubCategoriesController@getUpdate');
    Route::get('add_sub_categories', 'Admin\AdminSubCategoriesController@add');
    Route::post('/add_sub_categories/save', 'Admin\AdminSubCategoriesController@create');
    Route::get('/adminsubcategories/delete/{id}', 'Admin\AdminSubCategoriesController@remove');
    Route::post('/update_sub_categories/save', 'Admin\AdminSubCategoriesController@sub_cat_update');
    Route::get('admin_products', 'Admin\AdminProductsController@getProducts');
    Route::get('/adminproducts/delete/{id}', 'Admin\AdminProductsController@remove');
    Route::get('/adminproducts/update/{id}', 'Admin\AdminProductsController@getUpdate');
    Route::get('add_products', 'Admin\AdminProductsController@add');
    Route::get('/categories/change/{id}' , 'Admin\AdminProductsController@changeSubCategories');
    Route::post('/add_products/save', 'Admin\AdminProductsController@create');
    Route::post('/update_products/save', 'Admin\AdminProductsController@products_update');
    Route::get('admin_descriptions', 'Admin\AdminDescriptionsController@getDescriptions');
    Route::get('/admindescriptions/update/{id}', 'Admin\AdminDescriptionsController@getUpdate');
    Route::post('/update_descriptions/save', 'Admin\AdminDescriptionsController@descriptions_update');
    Route::get('/admindescriptions/delete/{id}', 'Admin\AdminDescriptionsController@remove');
    Route::get('/img/delete/{id}', 'Admin\AdminProductsController@img_delete');
    Route::post('add-details', 'Admin\AdminDetailsController@addDetails');
    Route::post('/update_detail/save', 'Admin\AdminDetailsController@detail_update');

    Route::get('product-details/{id}', 'Admin\AdminDetailsController@getDetails');
    Route::get('admin_details', 'Admin\AdminDetailsController@getDetails');
    Route::get('/product-details/update/{id}', 'Admin\AdminDetailsController@getUpdate');
    Route::get('/product-details/delete/{id}', 'Admin\AdminDetailsController@remove');
    Route::get('/banners', 'Admin\BannersController@get_banners');
    Route::get('/add_banners', 'Admin\BannersController@add_banners');
    Route::post('/add_banners/save', 'Admin\BannersController@create');
    Route::get('/banners/delete/{id}', 'Admin\BannersController@remove');
    Route::get('/brands', 'Admin\BrandsController@get_brands');
    Route::get('/add_brands', 'Admin\BrandsController@add_brands');
    Route::post('/add_brands/save', 'Admin\BrandsController@create');
    Route::get('/brands/delete/{id}', 'Admin\BrandsController@remove');
    Route::get('/feedbacks', 'Admin\AdminFeedbacksController@feedbacks');

    /**
     * AdminNewslettersController
     */
    Route::get('/feedbacks/delete/{id}', 'Admin\AdminFeedbacksController@remove');
    Route::get('/feedback/change_status/{id}/{status}', 'Admin\AdminFeedbacksController@changeStatus');
    Route::get('/newsletters', 'Admin\AdminNewslettersController@newsletters');
    Route::get('/newsletters/delete/{id}', 'Admin\AdminNewslettersController@remove');

    /**
     *Admin OrderRequestController
     */
    Route::get('/orders', ['uses' => 'Admin\AdminOrderRequestsController@get_orders', 'as'=>'order.requested.list']);
    Route::get('/info/order/{id}', 'Admin\AdminOrdersController@get_orders');
    Route::get('/orders/delete/{id}', 'Admin\AdminOrderRequestsController@remove');

    /**
     * AdminOrdersController
     */


    /**
     * ImageController
     */
    Route::get('/product_images/{id}', 'Admin\ImagesController@get_images');
    Route::post('/add_product_images', 'Admin\ImagesController@add_product_images');
    Route::get('/product_images/delete/{id}', 'Admin\ImagesController@remove');


    /**
     * ReportsController
     */
    Route::get('/reports', 'Admin\ReportsController@reports');
    Route::get('reports/download', 'Admin\ReportsController@download');

    Route::get('/orders/change_status/{id}/{status}', 'Admin\AdminOrderRequestsController@changeStatus');

    /*Template Detailes Route */
    Route::resource('template_details', 'Admin\TemplateDetailsController', [
        'names' => [
            'as' => config('app.admin_prefix')
        ]
    ]);
    /*Template Detailes Items Route */
    Route::resource('template_details_items', 'Admin\TemplateDetailsItemsController',[
        'names' => [
            'as' => config('app.admin_prefix')
        ]
    ]);
    /*get templates details*/
    Route::get('/get_templates/{id}/{product_id}', [
        'uses' =>'Admin\GetTemplatesController@getTemplate',
        'as'   =>'template.get'
    ]);
    Route::post('/save_template_details', [
        'uses' =>'Admin\GetTemplatesController@saveTemplate',
        'as'   =>'template.save'
    ]);


    Route::get('report-lists', 'Admin\ReportsController@lists');

    /**
     *Admin NewsController
     */

    Route::get('news', 'Admin\AdminNewsController@getNewsPage');
    Route::get('add_news', 'Admin\AdminNewsController@addNews');
    Route::post('add_news/save', 'Admin\AdminNewsController@create');
    Route::get('news/update/{id}', 'Admin\AdminNewsController@getUpdateNews');
    Route::post('/update_news/save', 'Admin\AdminNewsController@newsUpdate');
    Route::get('news/delete/{id}', 'Admin\AdminNewsController@remove');


});

