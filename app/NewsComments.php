<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsComments extends Model
{
    protected $table = 'news_comments';
    
    protected $fillable     =   [
        'user_id',
        'name',
        'news_id',
    ];
    
    public function news()
    {
      return $this->belongsTo(News::class);
    }
}
