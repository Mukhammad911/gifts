<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use BntAr\Roles\Traits\HasRoleAndPermission as UltrawareHasRoleAndPermission;
use BntAr\Roles\Contracts\HasRoleAndPermission;

class User extends Authenticatable implements HasRoleAndPermission
{
    use Notifiable;
    use UltrawareHasRoleAndPermission;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function product_comments()
    {
        return $this->hasMany(Comments::class, 'user_id', 'id');
    }

    public function product_marks()
    {
        return $this->hasMany(Marks::class, 'user_id', 'id');
    }
}
