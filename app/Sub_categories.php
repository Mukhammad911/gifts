<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sub_categories extends Model
{
    protected $table = 'sub_categories';

    protected $fillable     =   [
      //'id',
      'categories_id',
      'name'
  ];

    public function categories()
    {
      return $this->belongsTo(Categories::class);
    }

    public function products()
    {
      //return $this->hasMany(Products::class, 'sub_categories_id', 'id');
        return $this->belongsToMany('App\Products', 'sub_category_products',  'sub_category_id', 'product_id');
    }
}
