<?php

namespace App\Http\Middleware;

use App\CustomClasses\Helper;
use Closure;

class CookieOrder
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Helper::getCookieId($request);

        return $next($request);

    }
}
