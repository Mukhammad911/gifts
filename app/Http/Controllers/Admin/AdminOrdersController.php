<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Categories;
use App\Sub_categories;
use App\Products;
use App\Banners;
use App\Newsletters;
use App\Orders;
use App\OrderRequests;

class AdminOrdersController extends Controller
{
    public function get_orders(Request $request, $id)
    {
        //$order_request = OrderRequests::find($id);
        //dd($id);
        $orders = Orders::where('order_requests_id',  $id)->get();
        
        
        return view('admin.partials.orders', compact('orders'));
    }

    public function paid($id)
    {
        $order_request = OrderRequests::where('id', $id)->first();
        $order_request->status = 'paid';
        $order_request->save();

        $order = Orders::find($id);
        $order->status = 'paid';
        $order->save();

        return back()->with('message', 'Статус успешно изменен');
    }

    public function cart($id)
    {
        $order_request = OrderRequests::where('id', $id)->first();
        $order_request->status = 'paid';
        $order_request->save();
        
        $order = Orders::find($id);
        $order->status = 'cart';
        $order->save();

        return back()->with('message', 'Статус успешно изменен');
    }


    public function complete($id)
    {
        $order = Orders::find($id);
        $order->status = 'complete';
        $order->save();

        return back()->with('message', 'Статус успешно изменен');
    }

    public function boxing($id)
    {
        $order = Orders::find($id);
        $order->status = 'boxing';
        $order->save();

        return back()->with('message', 'Статус успешно изменен');
    }

    public function canceled($id)
    {
        $order = Orders::find($id);
        $order->status = 'canceled';
        $order->save();

        return back()->with('message', 'Статус успешно изменен');
    }

    public function payment($id)
    {
        $order = Orders::find($id);
        $order->status = 'payment';
        $order->save();

        return back()->with('message', 'Статус успешно изменен');
    }

    public function preformed($id)
    {
        $order = Orders::find($id);
        $order->status = 'preformed';
        $order->save();

        return back()->with('message', 'Статус успешно изменен');
    }

    public function shipping($id)
    {
        $order = Orders::find($id);
        $order->status = 'shipping';
        $order->save();

        return back()->with('message', 'Статус успешно изменен');
    }
}
