<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Categories;
use App\Sub_categories;
use App\Products;
use App\Descriptions;
use App\Details;
use App\Template_details;
use Illuminate\Support\Facades\Log;

class AdminDetailsController extends Controller
{
    public function getDetails($id)
    {
        if(!isset($id) || $id == "")
        {
            Log::warning('The param id is not defined');
            return false;
        }

        $products = Products::find($id);
        $template_details = Template_details::all();
        $details = Details::where('products_id', $id)->get();

        return view('admin.partials.get_details', compact('details', 'products','template_details'));
    }

    public function addDetails(Request $request)
    {
        $request->validate([
            'detail_name' => 'required',
            'detail_value' => 'required',
            'product_id' => 'required',
        ]);

        $detail = Details::where('name',$request->input('detail_name'))->where('products_id', $request->input('product_id'))->get();
        if(0 != $detail->count()) 
        {
            $products = Products::find($request->input('product_id'));

            $details = Details::where('products_id', $request->input('product_id'))->get();

            return back()->with('message', 'Деталь существует!!!');    
        }

        $detail = new Details();
        $detail->name = $request->input('detail_name');
        $detail->value = $request->input('detail_value');
        $detail->products_id = $request->input('product_id');
        $detail->created_at = Carbon::now();
        $detail->updated_at = Carbon::now();

        $detail->save();

        $products = Products::find($request->input('product_id'));

        $details = Details::where('products_id', $request->input('product_id'))->get();

        return back()->with('message', 'Деталь успешно добавлен');
    }
    public function getUpdate($id)
    {
        
        $data_det = Details::find($id);
        
        return view('admin.partials.update_details', compact('data', 'data_det'));     
    }

    public function detail_update(Request $request) {
        $this->validate(request(), [
           'detail'  => 'required|string',
           'value'  => 'required|string',
        ]);

        $detail = $request->input('detail');
        $value =  $request->input('value');
        $id   = $request->input('id');
        
        $det = Details::find($id);
        $det->name = $detail;
        $det->value = $value;
        $det->save();

        return redirect()->back()->with('message', 'Данные успешно обновлены' );
    }


    public function remove($id) 
    {
        $details = Details::find($id);
        $details->delete($id);
        return back()->withInput(); 
        
    }

}
