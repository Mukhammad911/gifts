<?php

namespace App\Http\Controllers\Admin;

use App\Template_details_items;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Template_details;
use DB;

class TemplateDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Template_details::all();
        return view('admin.partials.template_details_index')->with('data',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.partials.add_template_details');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validate(request(), [
            'name'  => 'required|unique:template_details|string|max:200',
        ] );
        $template_name  = $request->input('name');
        
        Template_details::insert([
            'name'       => $template_name,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        return back()->with('message', "Название шаблона детали успешно добавлен");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = DB::table('template_details_items')->where('template_details_id','=',$id)->get();
        
        return view('admin.partials.show_template_details')->with(['data'=> $data,'id_row'=>$id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Template_details::find($id);
        return view('admin.partials.update_template_details')->with('data', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validate(request(), [
            'name'  => 'required|unique:template_details|string|max:200',
        ] );
        $template_name  = $request->input('name');
        $row = [
            'name'       => $template_name
        ];
        Template_details::whereId($id)->update($row);
        return back()->with('message', "Название шаблона детали успешно изменен");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $template = Template_details::find($id);
        $template->delete($id);
        return back()->with('message', "Шаблон детали успешно удален");
    }

    public function ajaxTemplates($id)
    {

        $templates  =    Template_details_items::where('template_details_id', $id)->get();

        if($templates->count() < 1)
        {
            $response = [
                //'success' => false,
                'data'    => [],
                //'message' => 'Данные не найдены'
            ];

            return response()->json($response, 200);

        }

        $response = [
            //'success' => true,
            'data'    => $templates,
            //'message' => 'Данные найдены'
        ];

        return response()->json($response, 200);

    }
}
