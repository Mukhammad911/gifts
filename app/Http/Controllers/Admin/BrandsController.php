<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Categories;
use App\Sub_categories;
use App\Products;
use App\Brands;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;


class BrandsController extends Controller
{
    public static function brands_list()
    {
        $brands =  Brands::all();
        
        return $brands;
    }

    public static function get_brands()
    {
        $brands =  Brands::all();
        
        return view('admin.partials.brands_image', compact('brands'));
    }

    public function add_brands()
    {
        return view('admin.partials.add_brands');
    }

    public function create(Request $request)
    {
        $this->validate(request(), [
            'name'  => 'required|unique:products|string|max:100',
            'img' => 'mimes:jpg,png,jpeg|required|max:500|dimensions:min_width=144,min_height=35'
        ]);
        
        $img   = $request->file('img');
        //dd($img);
        $name  = $request->input('name');
        
        $new_date = now();
        $hash = hash('sha256', $new_date). '.' . $img->getClientOriginalExtension();
        $img = Image::make(Input::file('img'))->resize('144', '35')->save(public_path('images/brands/').$hash);
        
        Brands::insert([
            'name' => $name,
            'img' => $hash,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        return back()->with('message', 'Бренд успешно добавлен');
    }

    public function remove($id) 
    {
        $brands = Brands::find($id);
        if(\File::exists(public_path('images/brands/'.$brands->img)))
        {
            \File::delete(public_path('images/brands/'.$brands->img));
        }
        
        $brands->delete($id);
        
        return back()->with('message', 'Бренд успешно удален'); 
    }

}
