<?php

namespace App\Http\Controllers\Admin;

use App\SubCategoryProducts;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Categories;
use App\Sub_categories;
use App\Products;
use App\Descriptions;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use App\Details;
use App\Images;
use App\Template_details;
use Illuminate\Support\Str;


class AdminProductsController extends Controller
{
    public function getProducts()
    {

        $products = Products::all();

        return view('admin.partials.products', compact( 'products'));
    }

    public function changeSubCategories($id)
    {

        $sc = Sub_categories::where('categories_id', $id)->get();
        return back()->with('categories', $id);

    }

    public function add() {

        $categories     =   Categories::all();

        $sub_categories = Sub_categories::all();

        $template_details = Template_details::all();

        return view('admin.partials.add_products',
            compact('sub_categories', 'template_details', 'categories'));
    }


    public function create(Request $request) {


        $this->validate(request(), [
            'name'  => 'required|string|max:100',
            'sub_category' => 'required',
            'price' => 'required|numeric',
            'img' => 'mimes:jpg,png,jpeg|required|max:2000|dimensions:min_width=433,min_height=325',

        ]);

        $details = $request->input('detail');
        $values = $request->input('value');

        if(isset($details) || isset($values))
        {
            $results = array_combine($details, $values);
        }

        /*if(isset($details) || isset($value))
        {
            if(in_array(null,$value) || in_array(null, $details))
            {
                return redirect()->back()->withErrors(['errors' => 'Поле деталь или значение не должны быть пустыми']);
            }
        }*/

        $pictures       = $request->file('pictures');
        $img            = $request->file('img');
        $name           = $request->input('name');
        $price          = $request->input('price');
        $sub_category   = $request->input('sub_category');
        $old_price      = $request->input('old_price');
        $brand          = $request->input('brand');
        $available      = $request->input('available');
        $discount       = $request->input('discount');
        $onsale         = $request->input('onsale');
        $new            = $request->input('new');
        $bestseller     = $request->input('bestseller');
        $feature        = $request->input('feature');
        $description    = $request->input('description');

        $new_date = now();
        $hash = hash('sha256', $new_date). '.' . $img->getClientOriginalExtension();
        if(\File::exists(public_path('images/products/')) == false){
            \File::makeDirectory(public_path('images/products/'), 0777, true, true);
        }
        Image::make(Input::file('img'))->save(public_path('images/products/').$hash);


        try{
            DB::beginTransaction();

            $product = new Products();

            $product->name          =   $name;
            $product->price         =   $price;
            $product->old_price     =   $old_price;
            $product->brand         =   $brand;
            $product->discount      =   $discount;
            $product->onsale        =   $onsale;
            $product->new           =   $new;
            $product->bestseller    =   $bestseller;
            $product->available     =   $available;
            $product->feature       =   $feature;
            $product->img           =   $hash;
            $product->description   =   $description;
            $product->save();

            foreach ($sub_category as $sub_cat)
            {
                $sub_cat_products                   =   new SubCategoryProducts();
                $sub_cat_products->sub_category_id  =   $sub_cat;
                $sub_cat_products->product_id       =   $product->id;
                $sub_cat_products->save();
            }

            /*if(isset($details) && isset($value))
            {
                //Add details for product
                foreach ($details as $key => $detail)
                {
                    $add_detail                 =   new Details();
                    $add_detail->name           =   $detail;
                    $add_detail->value          =   $value[$key];
                    $add_detail->products_id    =   $product->id;
                    $add_detail->save();
                }
            }*/

            if(isset($results))
            {
                //Add details for product
                foreach ($results as $detail => $value)
                {
                    if(!empty($detail) && !is_null($value))
                    {
                        $add_detail                 =   new Details();
                        $add_detail->name           =   $detail;
                        $add_detail->value          =   $value;
                        $add_detail->products_id    =   $product->id;
                        $add_detail->save();
                    }
                }
            }


            //Add pictures for product
            if(isset($pictures))
            {
                $this->validate($request, [
                    'pictures'      => 'array',
                    'pictures.*'    => 'mimes:jpg,png,jpeg|required|max:2000',
                ]);

                foreach ($pictures as $picture)
                {
                    $random = Str::random(32);
                    $pic = hash('sha256', $random). '.' . $picture->getClientOriginalExtension();
                    if(\File::exists(public_path('images/products/')) == false){
                        \File::makeDirectory(public_path('images/products/'), 0777, true, true);
                    }

                    Image::make($picture)->save(public_path('images/products/').$pic);

                    $add_pictures = new Images();
                    $add_pictures->name = $random;
                    $add_pictures->img  = $pic;
                    $add_pictures->products_id    =   $product->id;
                    $add_pictures->save();
                }
            }

            DB::commit();
        } catch(\Exception $e){

            DB::rollback();
            return redirect()->back()->withErrors(['errors' => $e->getMessage()]);
        }

        return back()->with('message', 'Товар успешно добавлен');
    }

    public function remove($id) 
    {
        $products = Products::find($id);

        if(\File::exists(public_path('images/products/'.$products->img)))
        {
            \File::delete(public_path('images/products/'.$products->img));
        }

        $products->delete($id);

        return back()->with('message', 'Товар успешно удален');
    }

    public function getUpdate($id)
    {
        $data = Products::find($id);

        if(is_null($data))
        {
            return redirect()->back();
        }

        $template_details = Template_details::all();

        $sub_category   =   Sub_categories::all();

        $categories     =   Categories::all();

        return view('admin.partials.update_products', compact('data', 'sub_category', 'template_details', 'categories'));
    }


    public function products_update(Request $request) 
    {
        $product_id = Products::find($request->id);

        $product_image = $product_id->img;

        $this->validate(request(), [
            'name'  => 'required|string',
            'price' => 'required|numeric',
            'sub_category' => 'required',
        ]);


        $sub_category = $request->input('sub_category');
        $name = $request->input('name');
        $price = $request->input('price');
        $id   = $request->input('id');
        $img = $request->file('img');
        $old_price = $request->input('old_price');
        $brand = $request->input('brand');
        $available = $request->input('available');
        $discount = $request->input('discount');
        $onsale = $request->input('onsale');
        $new = $request->input('new');
        $bestseller = $request->input('bestseller');
        $feature = $request->input('feature');
        $description = $request->input('description');
        $details = $request->input('detail');
        $values = $request->input('value');
        $pictures = $request->file('pictures');

        //dd($request->all());

        if(isset($details) || isset($values))
        {
            $results = array_combine($details, $values);
        }
        
        if(empty($product_image))
        {
            $this->validate(request(), [
                'img' => 'mimes:jpg,png,jpeg|required|max:2000|dimensions:min_width=433,min_height=325',
            ]);

            $new_date = now();
            $hash = hash('sha256', $new_date) . '.' . $img->getClientOriginalExtension();

            Image::make(Input::file('img'))->save(public_path('images/products/') . $hash);
        }

        try{
            DB::beginTransaction();

            $product = Products::find($id);
            $product->name = $name;
            $product->price = $price;
            $product->img = $hash ?? $product_image;
            $product->old_price = $old_price;
            $product->brand = $brand;
            $product->available = $available;
            $product->discount = $discount;
            $product->onsale = $onsale;
            $product->new = $new;
            $product->bestseller = $bestseller;
            $product->feature = $feature;
            $product->description = $description;
            $product->save();

            if(isset($sub_category))
            {
                //Получаем все невыбранные подкатегории и удаляем их
                SubCategoryProducts::where('product_id', $id)->whereNotIn('sub_category_id', $sub_category)->delete();

                foreach ($sub_category as $sub_cat)
                {
                    SubCategoryProducts::updateOrCreate(
                        [
                            'sub_category_id'   =>  $sub_cat,
                            'product_id'        =>  $id,
                        ],
                        [
                            'sub_category_id'       =>  $sub_cat,
                            'product_id'            =>  $id,
                        ]);
                }
            }


            if(isset($results))
            {
                //Удаляем все невыбранные детали
                $get_details    =   Details::where('products_id', $id)->whereNotIn('name', $details);
                $get_values     =   Details::where('products_id', $id)->whereNotIn('value', $values);

                if($get_details->count() > 0)
                {
                    $get_details->delete();
                }

                if($get_values->count() > 0)
                {
                    $get_values->delete();
                }

                //Обновляем или создаем новые детали товара
                foreach ($results as $detail => $value)
                {
                    if(!empty($detail) && !is_null($value))
                    {
                        Details::updateOrCreate(
                            [
                                'products_id'   =>  $id,
                                'name'          =>  $detail,
                                'value'         =>  $value,
                            ],
                            [
                                'products_id'   =>  $id,
                                'name'          =>  $detail,
                                'value'         =>  $value,
                            ]);
                    }

                }

            }

            //Если не выбран не один деталь то удаляем все детали товара
            else
            {
                Details::where('products_id', $id)->delete();
            }


            //Add pictures for product
            if(isset($pictures))
            {
                $this->validate($request, [
                    'pictures'      => 'array',
                    'pictures.*'    => 'mimes:jpg,png,jpeg|required|max:2000|dimensions:min_width=433,min_height=325',
                ]);

                foreach ($pictures as $key => $picture)
                {

                    $random = Str::random(32);
                    $pic = hash('sha256', $random). '.' . $picture->getClientOriginalExtension();
                    if(\File::exists(public_path('images/products/')) == false){
                        \File::makeDirectory(public_path('images/products/'), 0777, true, true);
                    }
                    Image::make($picture)->save(public_path('images/products/').$pic);

                    $add_pictures                   =   new Images();
                    $add_pictures->name             =   $random;
                    $add_pictures->img              =   $pic;
                    $add_pictures->products_id      =   $id;
                    $add_pictures->save();
                }
            }

            DB::commit();
        } catch(\Exception $e){

            DB::rollback();
            return redirect()->back()->withErrors(['errors' => 'Возникла ошибка при добавлении']);
        }

        return back()->with('message', 'Данные товара успешно обновленны');
        
    }

    public function img_delete($id) 
    {
        $image = Products::find($id);

        if(\File::exists(public_path('images/products/'.$image->img)))
        {
            \File::delete(public_path('images/products/'.$image->img));
        }
        else {
            dd('not found');
        }
        //dd($image->img);
        
        $image->img = null;
        $image->save();
        
        
        //return redirect('controlshop/admin_products');
        return back()->with('imgMessage', 'Картинка успешно удалена');
    }
    
    public function addSome() 
    {
        return view('admin.partials.add_some_products');
    }


    public function createSome(Request $request)
    
    {
        
        //$excel  = $request->file('excel');
        //dd($excel);
        //dd($excel->getClientOriginalname());
        //dd($excel->getClientOriginalExtension());

        // Excel::import(new UsersImport, request()->file('excel'));
        
        $products = Excel::toArray(new UsersImport, request()->file('excel'));
        //dd($products);
        try{
            DB::beginTransaction();
            foreach($products as $cat)
            {
                foreach($cat as $c)
                {  
                    if($c[0] != null){

                        Products::updateOrCreate(
                        ['name' => $c[0],],
                        [
                        'name'       => $c[0],
                        'price'      => $c[1],
                        'old_price' => $c[3],
                        'brand' => $c[4],
                        'discount' => $c[5],
                        'onsale' => $c[6],
                        'new' => $c[7],
                        'bestseller' => $c[8],
                        'available' => $c[9],
                        'feature' => $c[10],                    
                        'created_at' => now(),
                        'updated_at' => now()
                        ]);
                     }
                }
                
            }
            DB::commit();
        } catch(\Exception $e){
                        
            DB::rollback();
            return redirect()->back()->with('error', 'Возникла ошибка при добавлении Excel файла');
        }
        
        

        return redirect(config('app.admin_prefix').'/admin_products');
    }
    
        
}
