<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Categories;
use App\Sub_categories;
use App\Products;
use App\Banners;
use App\Newsletters;
use App\Orders;
use App\OrderRequests;
use Yajra\DataTables\Facades\DataTables;


class AdminOrderRequestsController extends Controller
{
    public function get_orders(Request $request)
    {
        $order_requests = OrderRequests::orderBy('id', 'desc')->get();
        foreach($order_requests as $order){
            if($order->delivery == "own"){
                $order->delivery = "Самовывоз";
            }
            
        } 
        foreach($order_requests as $order){
            if($order->korti_milli == "off"){
                $order->korti_milli = "Наличными";
            }
            
        } 

        foreach($order_requests as $order){
            if($order->korti_milli == "on"){
                $order->korti_milli = "Корти милли";
            }
            
        }

        return view('admin.partials.order_requests', compact('order_requests'));
    }

    public function changeStatus($id, $status)
    {

        $order_request = OrderRequests::find($id);
        $order_request->status = $status;
        $order_request->save();

        $order = Orders::where('order_requests_id', $id)->get();
        if($order->count()>0)
        {
            foreach($order as $or)
            {
                $or->status = $status;
                $or->save();
            }
        }
        return back()->with('message', 'Статус успешно изменен');
    }
}
