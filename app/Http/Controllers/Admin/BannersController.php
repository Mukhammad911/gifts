<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Categories;
use App\Sub_categories;
use App\Products;
use App\Banners;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;

class BannersController extends Controller
{
    public static function banners_list()
    {
        $banners =  Banners::all();
        
        return $banners;
    }


    public function get_banners()
    {
        $banners =  Banners::all();
        
        return view('admin.partials.banners', compact('banners'));
    }

    public function add_banners()
    {
        return view('admin.partials.add_banners');
    }

    public function create(Request $request)
    {
        $this->validate(request(), [
            'name'  => 'required|string|max:100',
            'img' => 'mimes:jpg,png,jpeg|required|max:2000|dimensions:min_width=898,min_height=525'
        ]);
        
        $img   = $request->file('img');
        
        $name  = $request->input('name');
        
        $new_date = now();
        $hash = hash('sha256', $new_date). '.' . $img->getClientOriginalExtension();

        if(\File::exists(public_path('images/banners/')) == false){
            \File::makeDirectory(public_path('images/banners/'), 0777, true, true);
        }
        $img = Image::make(Input::file('img'))->save(public_path('images/banners/').$hash);
        
        Banners::insert([
            'name'       => $name,
            'img' => $hash,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        return back()->with('message', 'Баннер успешно добавлен');
    }

    public function remove($id) 
    {
        $banners = Banners::find($id);
        if(\File::exists(public_path('images/banners/'.$banners->img)))
        {
            \File::delete(public_path('images/banners/'.$banners->img));
        }
        
        $banners->delete($id);
        
        return back()->with('message', 'Баннер успено удален'); 
    }
}
