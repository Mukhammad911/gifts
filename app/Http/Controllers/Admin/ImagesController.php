<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Categories;
use App\Sub_categories;
use App\Products;
use App\Brands;
use App\Images;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;

class ImagesController extends Controller
{
    public function get_images($id)
    {
        if(!isset($id) || $id == "")
        {
            Log::warning('The param id is not defined');
            return false;
        }

        $products = Products::find($id);

        $images = Images::where('products_id', $id)->get();

        return view('admin.partials.product_images', compact('images', 'products'));
    }

    // public function get_images()
    // {
        
    //     $images = Images::all();
        
    //     return view('admin.partials.product_images', compact('images'));
    // }

    public function add_product_images(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'img' => 'mimes:jpg,png,jpeg|required|max:500|dimensions:min_width=433,min_height=325',
            'product_id' => 'required',
        ]);

        $img   = $request->file('img');
        $name  = $request->input('name');
        $product_id = $request->input('product_id');
        
        $new_date = now();
        $hash = hash('sha256', $new_date). '.' . $img->getClientOriginalExtension();
        $img = Image::make(Input::file('img'))->resize('433', '325')->save(public_path('images/products/').$hash);
        
        Images::insert([
            'name'       => $name,
            'products_id' => $product_id,
            'img' => $hash,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        

        return back()->with('message', 'Картинка успешно добавлена');
    }

    public function remove($id) 
    {
        $image = Images::find($id);

        if(\File::exists(public_path('images/products/'.$image->img)))
        {
            \File::delete(public_path('images/products/'.$image->img));
        }
        else {
            dd('not found');
        }

        $image->delete($id);

        return back()->with('delete', 'Картинка успешно удалена'); 
        
    }

}
