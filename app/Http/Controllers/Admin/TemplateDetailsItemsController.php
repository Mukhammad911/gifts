<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Template_details_items;
use App\Template_details;

class TemplateDetailsItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Template_details_items::all();
        return view('admin.partials.template_details_items_index')->with('data',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = Template_details::all();
        
        return view('admin.partials.add_template_details_items')->with(['data'=>$data]);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(request(), [
            'template_key'  => 'required|string|max:100',
            'template' => 'required',
        ] );
        $key = $request->input('template_key');
        $value  = $request->input('template_value');
        $template = $request->input('template');
        
        $id_tem = Template_details_items::where('template_details_id',$template)->where('key',$key)->get('key');
        if ($id_tem->count()>0) {
            return back()->with('message', "Такая деталь в этом шаблоне существует!!");
        }
        Template_details_items::insert([
            'key'       => $key,
            'value'     =>$value,
            'template_details_id' => $template,
            'created_at' => now(),
            'updated_at' => now()
        ]);
        return back()->with('message', "Составляющие шаблона детали успешно добавлены");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Template_details::all();
        
        return view('admin.partials.add_template_details_items')->with(['data'=>$data,'id_row'=>$id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Template_details_items::find($id);
        return view('admin.partials.update_template_details_items')->with('data', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate(request(), [
            'template_key'  => 'required|string',
         ]);
 
         $key = $request->input('template_key');
         $value = $request->input('template_value');
         
         $row = [
             'key' =>$key,
             'value' =>$value
         ];
         Template_details_items::whereId($id)->update($row);
        return back()->with('message', "Детали шаблона успешно изменены");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $template_items = Template_details_items::find($id);
        $template_items->delete($id);
        return back()->with('message', "Шаблон детали успешно удален");
    }
}
