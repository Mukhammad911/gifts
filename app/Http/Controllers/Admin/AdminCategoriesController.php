<?php

namespace App\Http\Controllers\Admin;

use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Categories;
use App\Sub_categories;
use App\Products;

class AdminCategoriesController extends Controller
{
    public function getcategories()
    {
        $categories = Categories::all();
        return view('admin.partials.categories', compact('categories'));
    }

    public function add() {
        return view('admin.partials.add_categories');
    }

    public function create(Request $request) {
         
        $validator = $this->validate(request(), [
            'name'  => 'required|unique:categories|string|max:100',
        ] );


        $name  = $request->input('name');
        
        Categories::insert([
            'name'       => $name,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        return back()->with('message', 'Категория успешно добавлена');
    }
    /*
    ** Добавить категорию товара
    */
    
    public function remove($id) 
    {
        $categories = Categories::find($id);
        $categories->delete($id);
        return back()->with('message', 'Категория успешно удалена'); 
    }

    public function getUpdate($id)
    {
        $data = Categories::find($id);
        return view('admin.partials.update_categories', ['data' => $data]);
    }


    public function cat_update(Request $request) {
        $this->validate(request(), [
           'name'  => 'required|string',
        ]);

        $name = $request->input('name');
        $id   = $request->input('id');
        
        $cat = Categories::find($id);
        $cat->name = $name;
        $cat->save();

        return back()->with('message', 'Категория успешно обновлена');
    }

    
}
