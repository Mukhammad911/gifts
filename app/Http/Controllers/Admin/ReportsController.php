<?php

namespace App\Http\Controllers\Admin;

use App\CustomClasses\Helper;
use App\Imports\UsersImport;
use App\Exports\UsersExport;
use App\OrderRequests;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Categories;
use App\Sub_categories;
use App\Products;
use App\Reports;
use App\Orders;

class ReportsController extends Controller
{
    public function reports()
    {
        //$reports = Reports::all();

        return view('admin.partials.reports');
    }

    public function download()
    {
        $newDate = now();

        $hash = hash('sha256', $newDate);


        //$z = Excel::download($data, $hash.'.xlsx');
        //dd($z);
        return Excel::download(new UsersExport, $hash.'.xlsx');
    }

    public function lists(Request $request)
    {


        $count_by_days = $request->input('count_by_days');

        $start_date = $request->input('start_date');
        $end_date = $request->input('end_date');

        $status = $request->input('status');

        if($status == "all")
        {
            $status = array('cart', 'complete', 'boxing', 'canceled', 'paid', 'payment', 'preformed', 'shipping');

            $datas = OrderRequests::
            where('created_at', '>=', $start_date)
                ->where('created_at', '<=', $end_date)
                ->whereIn('status', $status)
                ->get();
            if($datas->count() == 0)
            {
                return response()->json([
                    'success' => false,
                    'data'=> $data,
                    'message'=> "The data is not found",
                ]);
            }
            $response = [];

            foreach($datas as $key => $data){
                $orders = Orders::where('order_requests_id', $data->id)->get();
                if($orders->count() == 0)
                {
                    continue;
                }

                foreach ($orders as $order)
                {
                    $order_push = [];
                    $order_push['price'] = $order->price;
                    $order_push['product_name'] = Products::where('id', $order->products_id)->first()->name;
                    $response[] = array_merge($data->toArray(), $order_push);
                }
            }
        }
        else{
            $datas = OrderRequests::where('created_at', '>=', $start_date)
                ->where('created_at', '<=', $end_date)
                ->where('status', $status)
                ->get();

            if($datas->count() == 0)
            {
                return response()->json([
                    'success' => false,
                    'data'=> $datas,
                    'message'=> "The data is not found",
                ]);
            }
            $response = [];

            foreach($datas as $key => $data){
                $orders = Orders::where('order_requests_id', $data->id)->get();
                if($orders->count() == 0)
                {
                    continue;
                }

                foreach ($orders as $order)
                {
                    $order_push = [];
                    $order_push['price'] = $order->price;
                    $order_push['product_name'] = Products::where('id', $order->products_id)->first()->name;
                    $response[] = array_merge($data->toArray(), $order_push);
                }
            }
        }
        // Get all clients statues
        $statues = [];

        foreach($response as $key => $val)
        {
            $statues[] = $val["status"];
        }

        return response()->json([
            'code'      => '200',
            'message'   => 'Заказы найдены',
            'data'      => $response,
            'graph'     => ($count_by_days == 'off') ? Helper::getLabelsAndCount($data) : Helper::getLabelsAndCountByDays($data),
            'status'    => array_count_values($statues),
        ]);
    }
}
