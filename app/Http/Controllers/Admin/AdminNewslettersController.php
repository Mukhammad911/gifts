<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Categories;
use App\Sub_categories;
use App\Products;
use App\Banners;
use App\Newsletters;


class AdminNewslettersController extends Controller
{
    public function newsletters()
    {
        $newsletters = Newsletters::all();
        
        return view('admin.partials.newsletter', compact('newsletters'));
    }

    public function remove($id) 
    {
        $newsletters = Newsletters::find($id);
        
        $newsletters->delete($id);
        
        return redirect(url(config('app.admin_prefix').'/newsletters')); 
    }

}
