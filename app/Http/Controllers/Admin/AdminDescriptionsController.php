<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Categories;
use App\Sub_categories;
use App\Products;
use App\Descriptions;

class AdminDescriptionsController extends Controller
{
    public function getDescriptions()
    {
        $products = Products::all();

        return view('admin.partials.descriptions', compact('descriptions','products'));
    }

    public function getUpdate($id)
    {
        $data = Products::find($id);
        $data_desc = Descriptions::where('products_id', '=',$id)->first();
        
        return view('admin.partials.update_descriptions', compact('data','data_desc')); //['data' => $data],['data_desc' => $data_desc]);
         
    }

    public function descriptions_update(Request $request) {
         
        $this->validate(request(), [
            'id'  => 'integer',
            'description'  => 'required',
        ] );

        $id = $request->input('id');
        $description = $request->input('description');
        
        Descriptions::updateOrCreate(
            ['products_id' => $id],
            [
            'name'       => $description,
            'products_id' => $id,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        return back()->with('message', 'Описание успешно добавлена');
    }

    public function remove($id) 
    {
        $descriptions = Descriptions::find($id);
        $descriptions->delete($id);
        return redirect('controlshop/admin_descriptions'); 
    }

}
