<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Template_details_items;
use App\Details;

class GetTemplatesController extends Controller
{
    public function getTemplate($id,$product_id)
    {
        $data = Template_details_items::where('template_details_id',$id)->get();
        return view('admin.partials.get_templates',compact('data'))->with('product_id',$product_id);
    }
    public function saveTemplate(Request $request)
    {
        /*validation problem */
        for ($i=0; $i < count($request->key); $i++) { 
            $data = Details::where('name',$request->key[$i])->where('products_id',$request->product_id);
            if(0 != $data->count()) 
            {
                return back()->with('message','Детали существуют!!!');    
            }
        }
        
        for ($i=0; $i < count($request->key); $i++) { 
            $detail = new Details();
            $detail->name = $request->key[$i];
            $detail->value = $request->value[$i];
            $detail->products_id = $request->product_id;
            $detail->created_at = Carbon::now();
            $detail->updated_at = Carbon::now();

            $detail->save();
        }
        return back()->with('message',"Детали из шаблона успешно сохранены!");
    }
}
