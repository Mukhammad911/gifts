<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Categories;
use App\Sub_categories;
use App\Products;
use App\Banners;
use App\Feedbacks;

class AdminFeedbacksController extends Controller
{
    public function feedbacks()
    {
        $feedbacks = Feedbacks::orderBy('id', 'DESC')->get();
        
        return view('admin.partials.feedback', compact('feedbacks'));
    }

    public function remove($id) 
    {
        $feedbacks = Feedbacks::find($id);
        
        $feedbacks->delete($id);
        
        return redirect(url(config('app.admin_prefix').'/feedbacks')); 
    }

    public function changeStatus($id, $status)
    {
        $feedback = Feedbacks::find($id);
        $feedback->status = $status;
        $feedback->save();

        return back()->with('message', 'Статус успешно изменен');    
    }

}
