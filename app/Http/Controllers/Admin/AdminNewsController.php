<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\News;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;

class AdminNewsController extends Controller
{
    public function getNewsPage()
    {
        $news = News::all();
        return view('admin.partials.news', compact('news'));
    }

    public function addNews()
    {
        return view('admin.partials.add_news');
    }

    public function create(Request $request)
    {
        $this->validate(request(), [
            'theme'  => 'required|string|max:100',
            'news'  => 'required|string|max:3000',
            'img' => 'mimes:jpg,png,jpeg|max:2000|dimensions:min_width=600,min_height=500'
        ]);
        
        $img   = $request->file('img');
        
        $theme  = $request->input('theme');

        $news  = $request->input('news');
        
        $new_date = now();
        $hash = hash('sha256', $new_date). '.' . $img->getClientOriginalExtension();
        $img = Image::make(Input::file('img'))->resize('600', '500')->save(public_path('images/news/').$hash);
        
        News::insert([
            'theme'       => $theme,
            'news'       => $news,
            'img' => $hash,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        return back()->with('message', 'Новость успешно добавлена');
    }

    public function remove($id) 
    {
        $news = News::find($id);
        if(\File::exists(public_path('images/news/'.$news->img)))
        {
            \File::delete(public_path('images/news/'.$news->img));
        }
        
        $news->delete($id);
        
        return back()->with('message', 'Новость успено удалена'); 
    }

    public function getUpdateNews($id)
    {
        $data = News::find($id);
        return view('admin.partials.update_news', compact('data'));
    }

    public function newsUpdate(Request $request) {
        $this->validate(request(), [
           'theme'  => 'required|string|max:100',
           'news'  => 'required|string|max:3000',
        ]);

        $theme = $request->input('theme');
        $news =  $request->input('news');
        $id   = $request->input('id');
        
        
        $new = News::find($id);
        $new->theme = $theme;
        $new->news = $news;
        $new->save();

        return back()->with('message', 'Данные успешно изменены');
    }
}
