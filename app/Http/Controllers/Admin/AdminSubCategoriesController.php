<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Categories;
use App\Sub_categories;
use App\Products;

class AdminSubCategoriesController extends Controller
{
    public function getsubcategories()
    {
        $sub_categories = Sub_categories::all();
        $categories = Categories::all();
        return view('admin.partials.sub_categories', compact('sub_categories', 'categories'));
    }

    public function add() {
        $categories = Categories::all();
        
        return view('admin.partials.add_sub_categories', compact('categories', 'sub_categories'));
    }

    public function create(Request $request) {
         
        $this->validate(request(), [
            'name'  => 'required|unique:sub_categories|string|max:100',
            'category' => 'required',
        ] );
        $name  = $request->input('name');
        $category = $request->input('category');

        Sub_categories::insert([
            'name'       => $name,
            'categories_id' => $category,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        return back()->with('message', 'Подкатегория успешно добавлена');
    }

    /*
    ** Добавить подкатегорию товара
    */
    
    public function remove($id) 
    {
        $sub_categories = Sub_categories::find($id);
        $sub_categories->delete($id);
        
        return back()->with('message', 'Подкатегория успешно удалена'); 
    }

    /*
    ** Удалить подкатегорию товара
    */
    
    public function getUpdate($id)
    {
        $data = Sub_categories::find($id);
        return view('admin.partials.update_sub_categories', ['data' => $data]);
    }


    public function sub_cat_update(Request $request) {
        $this->validate(request(), [
           'name'  => 'required|string',
        ]);

        $name = $request->input('name');
        $id   = $request->input('id');
        
        $sub_cat = Sub_categories::find($id);
        $sub_cat->name = $name;
        $sub_cat->save();

        return back()->with('message', 'Подкатегория успешно обновлена');
    }

    public function ajaxSubCategories($id)
    {
        $sub_categories  =    Sub_categories::where('categories_id', $id)->get();


        if($sub_categories->count() < 1)
        {
            $response = [
                //'success' => false,
                'data'    => [],
                //'message' => 'Данные не найдены'
            ];

            return response()->json($response, 200);

        }

        $response = [
            //'success' => true,
            'data'    => $sub_categories,
            //'message' => 'Данные найдены'
        ];

        return response()->json($response, 200);

    }

}
