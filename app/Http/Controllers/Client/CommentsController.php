<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Categories;
use App\Sub_categories;
use App\Products;
use App\Comments;
use App\Marks;
use Illuminate\Support\Facades\Auth;


class CommentsController extends Controller
{
    public function create(Request $request) 
    {
        $product_id = $request->input('product_id');
        $score = $request->input('score');
        $comment = $request->input('comment');
        $user_id = $request->input('user_id');
         
        if($comment)
        {   
            $this->validate(request(), [
                
                'comment'  => 'string',
            ] );

            Comments::insert([
                'name'       => $comment,
                'products_id' => $product_id,
                'user_id'   => $user_id,
                'created_at' => now(),
                'updated_at' => now()
            ]);
        }
            
        
        if($score)
        {
            Marks::updateOrCreate(
                ['products_id' => $product_id, 'user_id' => $user_id],
                [
                'rate' => $score,
                'products_id' => $product_id,
                'user_id' => $user_id,
                'created_at' => now(),
                'updated_at' => now()
            ]);
    
        }
        


        return back()->with('class', 'active');
    }

    public function remove($id) 
    {
        $comment = Comments::find($id);
        $comment->delete($id);
        return back()->with('class', 'active'); 
    }
}
