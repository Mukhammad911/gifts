<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Categories;
use App\Sub_categories;
use App\Products;
use App\Descriptions;
use App\Details;
use App\Comments;
use App\Feedbacks;
use Illuminate\Support\Facades\Auth;

class FeedbacksController extends Controller
{
    public function feedback(Request $request)
        {
        // $count_item = OrderController::countItem($request);

        // $sum_of_items = OrderController::sumOfItems($request);

        // $listOrders = OrderController::listOrders($request);
        
        $this->validate(request(), [
            'name'   => 'required|string|max:100',
            'email'  => 'required|email|max:100',
            'theme'  => 'required|string|max:100',
            'message'  => 'required|string|max:300',
         ]);

        $name = $request->input('name');
        $email = $request->input('email');
        $theme = $request->input('theme');
        $message = $request->input('message');

        Feedbacks::insert([
            'name'       => $name,
            'email'       => $email,
            'theme'       => $theme,
            'message'       => $message,
            'status'      => 'panding',
            'created_at' => now(),
            'updated_at' => now()
        ]);

        return redirect()->back()->with(['message' => 'Ваше сообщение отправлено!']); 
        }
}
