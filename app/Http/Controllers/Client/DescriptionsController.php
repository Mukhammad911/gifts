<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Categories;
use App\Sub_categories;
use App\Products;
use App\Descriptions;
use App\Details;
use App\Comments;
use App\Images;
use App\Marks;
use Illuminate\Support\Facades\Auth;
use Cookie;

class DescriptionsController extends Controller
{
    private function existsRecent($request, $id)
    {
        $ids = $request->session()->get('recent');
        if(is_null($ids))
        {
            return false;
        }
        if(count($ids) == 0 || empty($ids))
        {
            return false;
        }
        if(count($ids) > 10 )
        {
            $poped = array_pop($ids);

            if(in_array($id, $ids))
            {
                return true;
            }
            $request->session()->flush();
            $request->session()->push('recent', $id);
            foreach($ids as $i)
            {
                $request->session()->push('recent', $i);
            }
            return true;
        }
        if(in_array($id, $ids))
        {
            return true;
        }

        return false;
    }
    public function getDescription($id, Request $request)
    {
        if(!$this->existsRecent($request, $id))
        {
            $request->session()->push('recent', $id);
        }
        $categories = Categories::all();
        
        $user = Auth::user();
        $sub_categories = Sub_categories::find($id);
        $products = Products::find($id);
        $images = Images::where('products_id', $id)->get();

        $details = Details::where('products_id', $id)->get();
        $comments = Comments::where('products_id', $id)->get();
        //$get_cat_id = Products::find($id)->sub_categories->categories_id;

        /*$bc_categories = Categories::where('id', '=', $get_cat_id)->first();
        $bc_sub_categories_list = Sub_categories::where('categories_id', '=', $get_cat_id)->get();
        $get_sub_cat_id = Products::find($id)->sub_categories->id;
        $bc_products_list = Products::where('sub_categories_id', '=', $get_sub_cat_id)->get();
        $bc_sub_categories = Products::find($products->id)->sub_categories;
        $bc_products = Products::find($products->id);
        */

        $count_item = OrderController::countItem($request);

        $sum_of_items = OrderController::sumOfItems($request);

        $listOrders = OrderController::listOrders($request);

        $mark = Marks::where('products_id', $id)->get();
        
        $marks = Marks::where('products_id',$id)->pluck('rate')->toArray();
        
        
        if(!empty($marks))
        {
            $sum_marks = array_sum($marks)/count($marks);
            $round = round($sum_marks);
            $rate = intval($round);
            
                        
        }
        else
        {
            $rate = 0;
        }
        
        return view('client.partials.description', ['categories' => $categories, 'sub_categories' => $sub_categories,
            'products' => $products, 'details' => $details, 'comments' => $comments, 'user' => $user,
            'count_item' => $count_item, 'sum_of_items' => $sum_of_items, 'listOrders' => $listOrders,
            'images' => $images, 'marks' => $marks, 'rate' => $rate,
            'mark' => $mark,
        ])->withCookie(cookie()->forever('name', $id));

    }
}
