<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Categories;
use App\Sub_categories;
use App\Products;
use App\Descriptions;
use App\Details;
use App\Comments;
use Illuminate\Support\Facades\Auth;


class BestsellerController extends Controller
{
    public function bestsellers(Request $request)
    {
        $count_item = OrderController::countItem($request);

        $sum_of_items = OrderController::sumOfItems($request);

        $listOrders = OrderController::listOrders($request);

        $products = Products::where('bestseller', '!=', 1)->paginate(15);
        //dd($products);
        return view('client.partials.bestsellers', compact('products','listOrders','sum_of_items','count_item'));
    }
}
