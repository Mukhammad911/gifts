<?php

namespace App\Http\Controllers\Client;


use App\Categories;
use App\CustomClasses\Helper;
use App\Http\Controllers\Controller;
use App\Products;
use App\Details;
use App\Sub_categories;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class FilterController extends Controller
{
    public function getFilter(Request $request)
    {
        
        $categories = Categories::all();

        $sub_id = (int)$request->sub_id;

        $sub_categories = Sub_categories::find($sub_id);

        $products = [];

        $prod = $sub_categories->products;

        /**
         * Filter by brand
         */
        $brands = $request->input('brand');

        if(isset($brands) && count($brands) > 0)
        {
            $product_brands = [];
            $product_id_brands = [];

            foreach ($brands as $brand)
            {
                $product_brands[] = $prod->where('brand', $brand);
                    //Products::where('sub_categories_id','=', $request->input('sub_category_id'))->where('brand', $brand)->get();
            }

            foreach ($product_brands as $product_brand)
            {
                foreach ($product_brand as $pro_b)
                {
                    $product_id_brands[] = $pro_b->id;

                }
            }

            if(count($product_id_brands) > 0)
            {
                $products = $prod->whereIn('id', $product_id_brands);
                    //Products::where('sub_categories_id','=', $request->input('sub_category_id'))->whereIn('id', $product_id_brands)->get();

            }else{
                $products = [];
            }
        }
        /**
         * Filter by price
         */
        $min_price = $request->input('min_set_value');
        $max_price = $request->input('max_set_value');

        if(!empty($min_price) && !empty($max_price))
        {
            $result_product= [];

            foreach($products as $product)
            {

                if($product->price >= $min_price && $product->price <= $max_price)
                {
                    $result_product[] = $product->id;
                }
            }

            if(count($result_product) > 0)
            {
                $products = $prod->whereIn('id', $result_product);
                    //Products::where('sub_categories_id','=', $request->input('sub_category_id'))->whereIn('id', $result_product)->get();
            }else{
                $products = [];
            }
        }
        
        $filter = Helper::filter($request->input('sub_category_id'));

        $count_item = OrderController::countItem($request);

        $sum_of_items = OrderController::sumOfItems($request);

        $listOrders = OrderController::listOrders($request);

        $max_min = Helper::getMinMaxPrice($request->input('sub_category_id'));

        if(!empty($filter))
        {
            $result_filter = [];

            foreach ($filter as $key => $f)
            {
                $result_filter[$key] = $f;
                $result_filter[$key]['checked'] = $this->checkExistValue(array_values($f)[0]->brand, $brands);
            }

            if(!empty($result_filter))
            {
                $filter = $result_filter;
            }

        }

        /**
         * get all id from product_details
         */
        $all_products_from_details = [];

        if (isset($request->key)) {
            if ($request->value) 
            {
                $count_request=count($request->value);
                $check = $request->value;
                $all_product = Helper::getAllid($check);
                foreach ($all_product as $id_key) 
                {
                    foreach ($id_key as $id_value) 
                    {
                        $all_produts_id[] = $id_value['products_id'];
                    }
                }
                $all_products_id = array_count_values($all_produts_id);
                $all_products_from_details = Helper::getAllProductsFilter($all_products_id, $sub_id, $count_request);
            }
        }

        $filterDetail = Helper::filterDetails($sub_id);
        $filterDetails = $filterDetail[0];
        $count_filter = $filterDetail[1];

        $min_price = $request->input('min_set_value');
        $max_price = $request->input('max_set_value');

        $price_filter = [$min_price, $max_price];
        //dd($price_filter);

        if (!empty($all_products_from_details)) 
        {
            $all_products = Helper::getProductsPriceFilter($all_products_from_details, $max_price, $min_price);
        }
        


        if(empty($all_products_from_details))
        {
           
            $all_products[] = $prod->where('price', '>=', $min_price)->where('price', '<=', $max_price)->toArray();
                //Products::where('sub_categories_id',$sub_id)->where('price', '>=', $min_price)->where('price', '<=', $max_price)->get()->toArray();
        }
        
        return view('client.partials.filter', compact('categories','sub_categories','all_products', 'filter', 'count_item', 'sum_of_items', 'listOrders', 'max_min','filterDetails','check','count_filter','price_filter'));
    }

    protected function checkExistValue($brand, $brands)
    {

        if(empty($brands))
        {
            return 'not_checked';
        }

        $exist = false;

        foreach ($brands as $b)
        {
            if($b == $brand)
            {
                $exist = true;
            }
        }

        if($exist){
            return 'checked';
        }
        else{
            return 'not_checked';
        }
    }
}
