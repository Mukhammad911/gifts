<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Categories;
use App\Sub_categories;
use App\Products;
use App\Descriptions;
use App\Details;
use App\Comments;
use Illuminate\Support\Facades\Auth;

class FeatureController extends Controller
{
    public function feature(Request $request)
    {
        $count_item = OrderController::countItem($request);

        $sum_of_items = OrderController::sumOfItems($request);

        $listOrders = OrderController::listOrders($request);

        $products = Products::where('feature', '!=', 1)->paginate(15);
        //dd($products);
        return view('client.partials.feature', compact('products','listOrders','sum_of_items','count_item'));
    }
}
