<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Categories;
use App\Sub_categories;
use App\Products;
use App\Descriptions;
use App\Details;
use App\Comments;
use App\Newsletters;
use Illuminate\Support\Facades\Auth;


class NewslettersController extends Controller
{
    public function addEmail(Request $request)
    {
        if (!empty($request->input('email')) && $request->input('email') != null)
        {
            $news_latter = Newsletters::where('email', $request->input('email'))->first();

            if($news_latter === null)
            {
                $latter = new Newsletters();
                $latter->email = $request->input('email');
                $latter->created_at = Carbon::now();
                $latter->updated_at = Carbon::now();

                $latter->save();

                return redirect()->back()->with(['success' => 'Вы успешно подписались на нашу рассылку!']);
            }
            else{
                return redirect()->back()->withErrors(['errors' => 'The email already taken. custom']);
            }
        }
        else{
            return redirect()->back()->withErrors(['errors' => 'The email is empty']);
        }
    }
}
