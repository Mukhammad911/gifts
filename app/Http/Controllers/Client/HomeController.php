<?php

namespace App\Http\Controllers\Client;

 
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function getHome(Request $request)
    {
        $count_item = OrderController::countItem($request);

        $sum_of_items = OrderController::sumOfItems($request);

        $listOrders = OrderController::listOrders($request);


        return view('client.partials.home', compact('count_item','sum_of_items','listOrders'));
    }

}
