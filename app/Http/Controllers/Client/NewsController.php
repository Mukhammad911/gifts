<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\News;

class NewsController extends Controller
{
    function getNewsPage()
    {
        $news = News::paginate(6);
        return view('client.partials.news', compact('news'));
    }
}
