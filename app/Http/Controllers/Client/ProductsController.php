<?php

namespace App\Http\Controllers\Client;

use App\CustomClasses\Helper;
use App\Http\Controllers\Controller;
use App\Orders;
use Illuminate\Http\Request;
use App\Categories;
use App\Sub_categories;
use App\Products;
use App\Details;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;


class ProductsController extends Controller
{
    public function getProducts($id, Request $request)
    {

        $categories = Categories::all();

        $sub_categories = Sub_categories::find($id);

        $data = Sub_categories::find($id)->products;//Products::where('sub_categories_id', '=', $id)->paginate(9);

        $filter = Helper::filter($id);

        $count_item = OrderController::countItem($request);

        $sum_of_items = OrderController::sumOfItems($request);

        $listOrders = OrderController::listOrders($request);

        $max_min = Helper::getMinMaxPrice($id);

        if(!empty($filter))
        {
            $result_filter = [];

            foreach ($filter as $key => $f)
            {
                $result_filter[$key] = $f;
                $result_filter[$key]['checked'] = 'checked';
            }

            if(!empty($result_filter))
            {
                $filter = $result_filter;
            }

        }
        /*filter for details */
        $filterDetail = Helper::filterDetails((int)$id);
        $filterDetails = $filterDetail[0];
        $count_filter = $filterDetail[1];

        //$myCollectionObj = collect($myArray);


        $products = $this->paginate($data);
        $products->setPath($sub_categories->id);



        return view('client.partials.products', compact('categories','sub_categories','products', 'filter', 'count_item',
            'sum_of_items', 'listOrders', 'max_min','filterDetails','count_filter'));
    }


    public function paginate($items, $perPage = 9, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);

        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }
}
