<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Categories;
use App\Sub_categories;
use App\Products;
use App\Descriptions;
use App\Details;
use App\Comments;
use Illuminate\Support\Facades\Auth;

class DeliveryController extends Controller
{
    public function delivery(Request $request) 
    {
        $count_item = OrderController::countItem($request);

        $sum_of_items = OrderController::sumOfItems($request);

        $listOrders = OrderController::listOrders($request);

        return view('client.partials.delivery', compact('listOrders','sum_of_items','count_item'));
    }
}
