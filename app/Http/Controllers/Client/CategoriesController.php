<?php

namespace App\Http\Controllers\Client;

use App\CustomClasses\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Categories;
use App\Sub_categories;
use App\Products;

class CategoriesController extends Controller
{
    public function __construct(Request $request)
    {

    }

    public function read(Request $request)
    {
        $data = $request->session()->get('recent');
        //dd($data);
        $categories = Categories::all();
        $news = Products::where('new', '!=', 1)->inRandomOrder()->get();
        $tops = Products::where('bestseller', '!=', 1)->inRandomOrder()->get();
        $discount = Products::whereNotNull('discount')->inRandomOrder()->get();
        
        $features = Products::where('feature', '!=', 1)->paginate(4);

        $onsales = Products::where('onsale', '!=', 1)->inRandomOrder()->get();

        $count_item = OrderController::countItem($request);

        $sum_of_items = OrderController::sumOfItems($request);

        $listOrders = OrderController::listOrders($request);

        $brands_list = \App\Http\Controllers\Admin\BrandsController::brands_list();
        $banners_list = \App\Http\Controllers\Admin\BannersController::banners_list();
        

        return view('client.partials.main', compact('categories', 'onsales', 
            'tops','news','discount','discount','features', 'count_item', 
            'sum_of_items', 'listOrders', 'brands_list', 'banners_list', 'data'));
        
    }

    public function search(Request $request)
    {   
        $this->validate(request(), [
            //'search'  => 'string|max:100|required',
         ]);
        
        $search = $request->input('search');
        if($search){
            
        }
        //dd($search);

        $products = Products::where('name', 'like', '%' .$search. '%')->get();
            
        $sub_categories =  Sub_categories::where('name', 'like', '%' .$search. '%')->get();
        
        $count_item = OrderController::countItem($request);

        $sum_of_items = OrderController::sumOfItems($request);

        $listOrders = OrderController::listOrders($request);

         

        //$products = Products::all();
        //dd($sub_categories);
        return view('client.partials.search', compact('sub_categories', 'products','results','listOrders','sum_of_items','count_item'));
    }

    
}
