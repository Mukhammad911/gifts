<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\News;
use App\NewsComments;
use Illuminate\Support\Facades\Auth;


class NewsCommentsController extends Controller
{
    public function newsComments($id)
    {
        $new = News::find($id);
        //dd($new);
        $user = Auth::user();
        $news_more = NewsComments::where('news_id',$id)->get();
        //dd($news_more->count());
        return view('client.partials.news-more', compact('new', 'news_more', 'user'));
    }

    public function addComment(Request $request)
    {
        $this->validate(request(), [
                
            'comment'  => 'required',
            'name'  => 'required',
            'email'  => 'required',
        ] );

        
        $name = $request->input('name');
        $email = $request->input('email');
        $comment = $request->input('comment');
        $user_id = $request->input('user_id');
        $news_id = $request->input('news_id');
        
        NewsComments::insert([
            'name'       => $name,
            'email'     => $email,
            'comment'  => $comment,
            'news_id' => $news_id,
            'user_id'   => $user_id,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        return back()->with('message', 'Ваш комментарий успешно добавлен');
    }

    public function remove($id) 
    {
        $comment = NewsComments::find($id);
        $comment->delete($id);
        return back()->with('delete', 'Комментарий удален'); 
    }
}
