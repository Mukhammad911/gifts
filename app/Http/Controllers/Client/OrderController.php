<?php

namespace App\Http\Controllers\Client;

use App\Categories;
use App\CustomClasses\Helper;
use App\Http\Controllers\Controller;
use App\Http\Mail\OrderCreated;
use App\Http\Mail\OrderCreatedSuccess;
use App\OrderRequests;
use App\Orders;
use App\Products;
use App\Sub_categories;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class OrderController extends Controller
{
    private $client_code = 'a601f951';
    private $client_api_key = 'acd7664826476363e5277af2a730e83f1ae837fc8216da9118ecb74d46f0e446';
    private $root_api_key = 'f3ea8387d94553655552a1cc21a06280d4fee998df41f44b4b6ccab12c2b0464';

    /**
     * Get Count of card item by cookie Id
     */
    public static function countItem(Request $request)
    {
        $count = Orders::where('session_order_id', Helper::getCookieId($request))->where('status', 'cart')->count();

        return $count;
    }

    /**
     * Get Sum of price of selected Item
     */
     public static function sumOfItems(Request $request)
    {
        $count = Orders::where('session_order_id', Helper::getCookieId($request))->where('status', 'cart')->count();

        if($count != 0)
        {
            return Orders::where('session_order_id', Helper::getCookieId($request))->where('status', 'cart')->sum('price');
        }

        return 0;
    }

    /**
     * Cart add Product
     */
    public function cartAddProduct(Request $request)
    {
        $request->validate([
           'product_id' => 'required'
        ]);

        $product_id = $request->input('product_id');

        $product = Products::where('id', $product_id)->first();
        if($product->count() == 0)
        {
            Log::error("The product ID is not Found");

            return redirect()->back()->withErrors(['message' => 'The Item is not found']);
        }

        $order = new Orders();
        $order->status = 'cart';
        $order->price = $product->price;
        $order->currency = 'TJS';
        $order->session_order_id = Helper::getCookieId($request);
        $order->products_id = $product->id;

        $order->save();

        return redirect()->back()->with('success', ['Товар успешно добавлен в корзину']);
    }

    /**
     * ListOrders
     */
    public static function listOrders(Request $request)
    {
        return Orders::where('session_order_id', Helper::getCookieId($request))->where('status', 'cart')->get();
    }

    /**
     * Remove Cart order by order ID
     */
    public function removeCartOrder($id, Request $request)
    {
        $order = Orders::destroy($id);

        if($order)
        {
            return redirect()->back();
        }
    }

    /**
     * Get CartPage
     */
    public function getPageCart(Request $request)
    {
        $categories = Categories::all();

        $count_item = OrderController::countItem($request);

        $sum_of_items = OrderController::sumOfItems($request);

        $listOrders = OrderController::listOrders($request);

        $listOrdersDetail = DB::table('orders')
            ->where('session_order_id', Helper::getCookieId($request))
            ->where('status', 'cart')
            ->select('products_id', 'session_order_id')
            ->distinct('products_id')->get();

        return view('client.partials.cart', compact('categories','news','tops','discount','features', 'count_item', 'sum_of_items', 'listOrders', 'listOrdersDetail'));
    }

    /**
     * OrderProduct Plus
     */
    public function plusProductCart($id, Request $request)
    {
        $product = Products::find($id);

        $order = new Orders();
        $order->status = 'cart';
        $order->price = $product->price;
        $order->currency = 'TJS';
        $order->session_order_id = Helper::getCookieId($request);
        $order->products_id = $product->id;

        $order->save();

        return redirect()->back()->with('success', ['Товар успешно добавлен в корзину']);

    }

    /**
     * OrderProduct Minus
     */
    public function minusProductCart($id, Request $request)
    {
        $order_id = Orders::where('products_id', $id)->where('session_order_id', Helper::getCookieId($request))->first();
        Orders::destroy($order_id->id);

        return redirect()->back()->with('success', ['Товар успешно добавлен в корзину']);

    }

    /**
     * Remove All product
     */
    public function removeAllProductCart($id, Request $request)
    {
        $orders = Orders::where('products_id', $id)->where('session_order_id',Helper::getCookieId($request))->get();

        if($orders->count() > 0)
        {
            foreach ($orders as $order)
            {
                Orders::destroy($order->id);
            }
        }

        return redirect()->back();
    }

    /**
     * Billing Page
     */
    public function billingPage(Request $request)
    {
        $categories = Categories::all();

        $count_item = OrderController::countItem($request);

        $sum_of_items = OrderController::sumOfItems($request);

        $listOrders = OrderController::listOrders($request);

        $listOrdersDetail = DB::table('orders')
            ->where('session_order_id', Helper::getCookieId($request))
            ->select('products_id', 'session_order_id')
            ->where('status', 'cart')
            ->distinct('products_id')->get();

        return view('client.partials.billing', compact('categories','news','tops','discount','features', 'count_item', 'sum_of_items', 'listOrders', 'listOrdersDetail'));
    }

    /**
     * Add Order
     */
    public function billingAddOrder(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'surname' => 'required',
            'address' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'group1' => 'required',
            'group2' => 'required',
        ]);

        if(!$validatedData)
        {
            return;
        }

        if(0 == OrderController::countItem($request))
        {
            return redirect()->back()->withErrors(['error' => 'Your cart is empty. Please add product on your cart']);
        }

        $order_requests = new OrderRequests();
        $order_requests->name = $request->input('name');
        $order_requests->surname = $request->input('surname');
        $order_requests->address = $request->input('address');
        $order_requests->email = $request->input('email');
        $order_requests->city = empty($request->input('city')) ? null : $request->input('city');
        $order_requests->phone = $request->input('phone');
        $order_requests->otherPhone = $request->input('otherPhone');
        $order_requests->delivery = $request->input('group1');
        $order_requests->status = 'payment';
        $order_requests->korti_milli = $request->input('group2');
        //dd($order_requests->korti_milli);
        $order_requests->status = 'payment';

        $order_requests->save();

        $list_orders = self::listOrders($request);

        $sum_amount = self::sumOfItems($request);

        if(0 < $list_orders->count())
        {
            foreach ($list_orders as $order)
            {
                $update_order = Orders::find($order->id);

                $update_order->status = 'payment';

                $update_order->order_requests_id = $order_requests->id;

                $update_order->save();
            }
        }


        if(!empty($request->input('group2')) && $request->input('group2') == 'on')
        {

            $redirect = $this->createDusoftQuoteForOrder($request, $order_requests->id, $sum_amount);

            Helper::setNewCookie($request);

            return redirect($redirect);


        }else{

            $order_requests = OrderRequests::find($order_requests->id);

            try
            {
                Mail::to($request->input('email'))->send(new OrderCreated($order_requests));
            }
            catch(\Exception $e)
            {
                Log::error('User Mailing error');
            }

            try
            {
                Mail::to(config('app.email_administrator'))->send(new OrderCreated($order_requests));
            }

            catch(\Exception $e)
            {
                Log::error('User Mailing error');
            }

            Helper::setNewCookie($request);
            return redirect()->back()->with(['success' => 'Ваш заказ успешно оформлен. Дождитесь звонка оператора.']);
        }
    }

    public function createDusoftQuoteForOrder($request, $order_requests_id, $sum_amount)
    {

        $product_amount =  round($sum_amount, 2);
        Log::info('Product Amount'. $product_amount);

        //$item_url = 'http://upwork.tj/images/logo_farad.png';

        $hash = $this->sha256Hash($order_requests_id.$product_amount.$this->client_api_key);

        /** @var \GuzzleHttp\Psr7\Response $response */
        $response = new \GuzzleHttp\Psr7\Response();

        $client = new Client();

        $requestMethod = 'POST';
        $url = 'https://onlinepay.tj/api/pay/createQuote';

        try {
            $response = $client->request($requestMethod, $url, [
                'form_params' => [
                    'client_code' => $this->client_code,
                    'hash' => $hash,
                    'order_id' => $order_requests_id,
                    'amount' => $product_amount,
                    'item_url' => 'farad',
                ]
            ]);
        } catch (ClientException $ex) {
            echo 'hey client';
            $resEx = $ex->getMessage();

            return $resEx;
        } catch (ConnectException $ex) {
            $resEx = $ex->getMessage();

            return $resEx;
        } catch (RequestException $ex) {
            echo 'hey request';
            $resEx = $ex->getMessage();

            return $resEx;
        }
        $response_data = json_decode($response->getBody()->getContents());

        if ($response_data->success) {

            $updated_requests_order = OrderRequests::find($order_requests_id);
            $updated_requests_order->signature = $response_data->signature;
            $updated_requests_order->save();

            return 'https://onlinepay.tj/api/pay/gateway/'.$response_data->signature.'/'.$order_requests_id.'/'.$this->client_code;
        }
    }

    public function result(Request $request){
        Log::info('result');
        Log::info('app.requests', ['request' => $request->all()]);

    }

    public function sucessorder(Request $request){

        Log::info('sucess');
        Log::info('app.requests', ['request' => $request->all()]);

        $order = $request->input('order_id');

        $update = OrderRequests::find($order);
        $remote_signature = $request->input('signature');

        if(!$remote_signature == $update->signature)
        {
            Log::info('Signature not equal');
            Log::info($remote_signature);
            Log::info($update->signature);
            return;
        }

        $update->status_onlinepay='success';

        $update->status = 'paid';
        $update->save();

        $products_order_requests = OrderRequests::find($order);

        $products_order_requests->status = 'paid';
        
        $products_order_requests->save();

        $products_orders = Orders::where('order_requests_id', $order)->where('status', 'payment')->get();

        if(0 < $products_orders->count())
        {
            foreach ($products_orders as $product)
            {
                $new_changed_status = Orders::find($product->id);

                $new_changed_status->status = 'paid';

                $new_changed_status->save();
            }

        }

        $order_requests = OrderRequests::find($request->input('order_id'));
        try
        {
            Mail::to($order_requests->email)->send(new OrderCreatedSuccess($order_requests));
        }
        catch(\Exception $e)
        {
            Log::error('User Mailing success error');
        }

        try
        {
            Mail::to(config('app.email_administrator'))->send(new OrderCreatedSuccess($order_requests));
        }
        catch(\Exception $e)
        {
            Log::error('User Mailing success error');
        }

    }

    public function falseorder(Request $request){
        Log::info('falsee');
        Log::info('app.requests', ['request' => $request->all()]);

    }

    public function sha256Hash($str)
    {
        return hash('sha256', $str);
    }

}
