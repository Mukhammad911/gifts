<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Categories;
use App\Sub_categories;
use App\Products;
use App\Descriptions;
use App\Details;
use App\Comments;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public function send()
    {
        
        
        
        Mail::send(
            ['text' => 'client.partials.mail'], 
            ['name' => 'ikrom'],
            function($message)
            {
                $message->to('ikrom251124@gmail.com', 'to web dev blog');
                $message->from('ikrom251124@gmail.com', 'web dev');
            }
        );
    }
}
