<?php

namespace App\Http\Mail;

use App\OrderRequests;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderCreatedSuccess extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The order instance.
     *
     * @var Order
     */
    public $order;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(OrderRequests $order)
    {
        $this->order = $order;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from([
            'address' => config('mail.from.address'), 'name' => 'Farad'
        ])->subject('Оформлен заказ на сайте gifts.tj')->view('emails.order_success');
    }
}