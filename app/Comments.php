<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comments extends Model
{
    protected $table = 'product_comments';
    
    protected $fillable     =   [
        //'id',
        'products_id',
        'user_id',
        'name'
    ];
    
    public function products()
    {
      return $this->belongsTo(Products::class);
    }

    public function user()
    {
      return $this->belongsTo(User::class);
    }
}
