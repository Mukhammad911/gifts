<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Template_details extends Model
{
    protected $fillable = [
        'name'
    ];
}
