<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategoryProducts extends Model
{
    protected $table = 'sub_category_products';

    protected $fillable     =   [
        //'id',
        'sub_category_id',
        'product_id'
    ];


    public function products()
    {
        return $this->hasMany(Products::class, 'id', 'product_id');
    }
}
