<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $table = 'products';
    
    protected $fillable     =   [
        //'id',
        'sub_categories_id',
        'name',
        'price',
        'old_price',
        'brand',
        'discount',
        'onsale',
        'new',
        'bestseller',
        'available',
        'feature',
        'img'
    ];
    
    public function sub_categories()
    {
        return $this->belongsToMany('App\Sub_categories', 'sub_category_products',  'product_id', 'sub_category_id');
    }

    public function descriptions()
    {
      return $this->hasMany(Descriptions::class, 'products_id', 'id');
    }

    public function details()
    {
      return $this->hasMany(Details::class, 'products_id', 'id');
    }

    public function comments()
    {
      return $this->hasMany(Comments::class, 'products_id', 'id');
    }

    public function images()
    {
      return $this->hasMany(Images::class, 'products_id', 'id');
    }

    public function marks()
    {
      return $this->hasMany(Marks::class, 'products_id', 'id');
    }

    public function categories()
    {
      return $this->belongsTo(Categories::class);
    }

}
