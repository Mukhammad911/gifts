<?php

namespace App\Exports;

use App\Reports;
use Maatwebsite\Excel\Concerns\FromCollection;
use App\OrderRequests;

class UsersExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        
        return OrderRequests::all();
        
    }

    public function model(array $row)
    {
        // if (!isset($row[0])) {
        //     return null;
        // }
    
        
        return new Products([
            'name'     => $row[0],
            'price'      => $row[1],
            'sub_categories_id' => $row[2],
            'old_price' => $row[3],
            'brand' => $row[4],
            'discount' => $row[5],
            'onsale' => $row[6],
            'new' => $row[7],
            'bestseller' => $row[8],
            'available' => $row[9],
            'feature' => $row[10],
        ]);
        //dd($row[0]);
    }
}
