<?php
namespace App\CustomClasses;
use App\Orders;
use App\Products;
use App\Details;
use App\Sub_categories;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Session;

class Helper{

    public static function getMinMaxPrice($sub_category_id)
    {
        $response = ['min' => 0, 'max' => 0];

        $products = Sub_categories::find($sub_category_id)->products;

        $max = $products->max('price');

        $min = $products->min('price');
        if(!empty($max))
        {
            $response['max'] = $max;
        }

        if(!empty($min))
        {
            $response['min'] = $min;
        }

        return $response;
    }

    public static function filter($sub_category_id)
    {
        $filter = [];
        $products = Sub_categories::find($sub_category_id)->products;

        $brands =   $products->where('brand', '!=', null)->unique('brand');

        //$brands = Products::where('brand', '!=', 1)->where('sub_categories_id', $sub_category_id)->distinct()->get('brand');
        if($brands->count() != 0)
        {
            foreach($brands  as $key => $brand){
                if(!empty($brand->brand)){
                    $filter[] =[
                        'Бренд' => $brand,
                        'count' => $products->where('brand', '=', $brand->brand)->count(),
                        //Products::where('brand', '=', $brand->brand)->where('sub_categories_id', $sub_category_id)->count(),
                    ];
                    continue;
                }
                $filter[] = [
                    'Бренд' => $brand,
                    'count' =>0,
                ];
            }
        }
        /**
         * Получаем bestseller;
         */
       /* $bestsellers = Products::where('bestseller', '!=', 1)->where('sub_categories_id', $sub_category_id)->distinct()->get('bestseller');
        if($bestsellers->count() != 0)
        {
            foreach($bestsellers  as $key => $bestseller){
                if(!empty($bestseller->bestseller)){
                    $filter[] =[
                        'Топ продаж' => $bestseller,
                        'count' => Products::where('bestseller', '=', $bestseller->bestseller)->where('sub_categories_id', $sub_category_id)->count(),
                    ];
                    continue;
                }
                $filter[] = [
                    'Топ продаж' => $bestseller,
                    'count' =>0,
                ];
            }
        }*/
        /**
         * Получаем new;
         */

        /*$news = Products::where('new', '!=', 1)->where('sub_categories_id', $sub_category_id)->distinct()->get('new');
        if($news->count() != 0)
        {
            foreach($news  as $key => $new){
                if(!empty($new->new)){
                    $filter[] =[
                        'Новинка' => $new,
                        'count' => Products::where('new', '=', $new->new)->where('sub_categories_id', $sub_category_id)->count(),
                    ];
                    continue;
                }
                $filter[] = [
                    'Новинка' => $new,
                    'count' =>0,
                ];
            }
        }
*/
        /**
         * Получаем onsale;
         */

        /*$onsales = Products::where('onsale', '!=', 1)->where('sub_categories_id', $sub_category_id)->distinct()->get('onsale');
        if($onsales->count() != 0)
        {
            foreach($onsales  as $key => $onsale){
                if(!empty($onsale->onsale)){
                    $filter[] =[
                        'Распродажа' => $onsale,
                        'count' => Products::where('onsale', '=', $onsale->onsale)->where('sub_categories_id', $sub_category_id)->count(),
                    ];
                    continue;
                }
                $filter[] = [
                    'Распродажа' => $onsale,
                    'count' =>0,
                ];
            }
        }*/

        /**
         * Получаем available;
         */

        /*$availables = Products::where('available', '!=', 1)->where('sub_categories_id', $sub_category_id)->distinct()->get('available');
        if($availables->count() != 0)
        {
            foreach($availables  as $key => $available){
                if(!empty($available->available)){
                    $filter[] =[
                        'В наличии' => $available,
                        'count' => Products::where('available', '=', $available->available)->where('sub_categories_id', $sub_category_id)->count(),
                    ];
                    continue;
                }
                $filter[] = [
                    'В наличии' => $available,
                    'count' =>0,
                ];
            }
        }*/
        return $filter;
    }

    /**
     * @param Request $request
     * Initialize session cart for unique order
     */
    public static function setNewCookie(Request $request)
    {

        $time = Carbon::now();
        $cookie_id = hash('sha256', $time);
        Cookie::queue(Cookie::make('cart', $cookie_id, 6000000));

        return  $request->cookie('cart');
    }

    /**
     * @param Request $request
     * Initialize session cart for unique order
     */
    public static function initCookie(Request $request)
    {

        $cookie = $request->cookie('cart');
        if(is_null($cookie) || $cookie == "")
        {
            $time = Carbon::now();
            $cookie_id = hash('sha256', $time);
            Cookie::queue(Cookie::make('cart', $cookie_id, 6000000));

        }
        return  $request->cookie('cart');
    }

    /**
     * @param Request $request
     * Get SessionId
     */
    public static function getCookieId(Request $request)
    {
        return  self::initCookie($request);
    }


    public static function filterDetails($sub_id)
    {
        $b = [];

        $products = Sub_categories::find($sub_id)->products;

        $id = $products->pluck('id');

        //$id = Products::where('sub_categories_id',$sub_id)->get('id');

        $id_for_count = $products->pluck('id')->toArray();
        //Products::where('sub_categories_id',$sub_id)->get('id')->toArray();

        $name_details = [];

        foreach ($id as $key) {
            $name_detail = Details::where('products_id',$key)->get();
            if(!$name_detail->count() > 0)
            {
                continue;
            }

            foreach ($name_detail as $value)
            {
                $name_details[] = $value->name;
            }
        }

        /**filter name_details with value >=2 */
        if(!empty($name_details))
        {
            $name_details = array_count_values($name_details);
            foreach ($name_details as $key => $value)
            {
                if($value >= 1)
                {
                    foreach ($id as $key_id)
                    {
                        $a=Details::where('name',$key)
                            ->where('products_id',$key_id)
                            ->distinct()->get()->toArray();

                        if (!empty($a[0]['value']))
                        {
                            if (in_array($a[0]['value'], $b))
                            {

                            }
                            else
                            {
                                $name_detailes[$key][] = Details::where('name',$key)
                                    ->where('products_id',$key_id)
                                    ->distinct()->get('value');
                                $count[] = Details::where('name',$key)
                                    ->where('products_id',$key_id)
                                    ->where('name',$a[0]['name'])
                                    ->distinct()
                                    ->get();
                                $b[] = $a[0]['value'];
                            }
                        }

                    }
                }
            }

            foreach ($count as $keys) {
                foreach ($keys as $key => $value) {
                    $counts[] = $value->value;
                    foreach ($id_for_count as $count_id) {
                        $c_exam = Details::where('name', $value->name)
                            ->where('value',$value->value)
                            ->where('products_id', $count_id)
                            ->count();
                        if ($c_exam!=0) {
                            $countes[$value->value][] = Details::where('name', $value->name)
                                ->where('value',$value->value)
                                ->where('products_id', $count_id)
                                ->count();
                        }

                    }

                }
            }

        }else {
            $name_detailes = [];
            $countes = [];
        }

        return [$name_detailes,$countes];
    }

    public static function getAllid($data)
    {
        global $response_data_details_value;
        if(count($data) == 0)
            return;

        foreach ($data as $key => $value)
        {
            $detail_name = $key;
            foreach ($value as $row)
            {
                $detail_value = $row;
                break;
            }
            break;
        }

        $res = $response_data_details_value[] = Helper::getDetailValue($detail_name, $detail_value);
        if ($res)
        {
            array_splice($data[$detail_name], 0, 1);
            if (count($data[$detail_name]) == 0)
            {
                array_splice($data, 0, 1);
            }
            Helper::getAllid($data);
        }
        return $response_data_details_value;
    }

    private static function getDetailValue($detail_name, $detail_value)
    {
        $detail = Details::where('name',$detail_name)
                                            ->where('value',$detail_value)
                                            ->get()
                                            ->toArray();
        return $detail;
    }


    public static function getAllProductsFilter($data, $sub_categories_id, $count_request)
    {
        global $response_data;

        $products = Sub_categories::find($sub_categories_id)->products;

        if(count($data) == 0)
            return;
        foreach ($data as $key => $value)
        {
            if ($value >= $count_request)
            {
                $id = $key;
                break;
            }
        }
        if (isset($id))
        {

            $res = $response_data[] = $products->where( 'id', $id)
                //Products::whereId($id)
                //->where('sub_categories_id',$sub_categories_id)
                //->get()
                ->toArray();
            if (empty($res))
                return;;
            if ($response_data)
            {
                unset($data[$id]);

                Helper::getAllProductsFilter($data, $sub_categories_id, $count_request);
            }
        }
        return $response_data;
    }

    public static function getProductsPriceFilter($all_products, $max, $min)
    {
        foreach ($all_products as $key => $value) 
        {
            if (!($min <= $value[0]['price'] && $value[0]['price'] <= $max)) 
            {
                unset($all_products[$key]);
            }
        }
        return $all_products;
    }
    
    /**
     * The function divide by month for charts
     * @param $datas
     * @return $response_data
     */
    public static function getLabelsAndCount($datas){
        $it = 0;
        $arr_date_month = [];
        foreach ($datas as $data)
        {
            $date_and_year = $data['created_at'];
            $month = date("m", strtotime($date_and_year));
            $year = date("Y", strtotime($date_and_year));
            if (!empty($arr_date_month))
            {
                $g = 0;
                for ($i = 0; $i < count($arr_date_month); $i++)
                {
                    if ((!empty($arr_date_month[$i]['month'])) && ((int)$arr_date_month[$i]['month'] == $month)
                        && (!empty($arr_date_month[$i]['year']))
                        && ((int)$arr_date_month[$i]['year'] == $year))
                    {
                        $arr_date_month[$i]['count'] = $arr_date_month[$i]['count'] + 1;
                        $g++;
                    }
                }
                if ($g == 0)
                {
                    $it++;
                    $arr_date_month[$it]['month'] = $month;
                    $arr_date_month[$it]['year'] = $year;
                    $arr_date_month[$it]['count'] = 1;
                }
            } else {
                $arr_date_month[0]['month'] = $month;
                $arr_date_month[0]['year'] = $year;
                $arr_date_month[0]['count'] = 1;
            }
        }
        $response_data = [];

        foreach($arr_date_month as $key => $value)
        {
            $month = DateTime::createFromFormat('!m', $value['month']);
            $monthFullName = $month->format('F');
            $monthShortName = date('M', strtotime($monthFullName));

            $response_data['labels'][] = $monthShortName.'/'.$value['year'];
            $response_data['counts'][]  = $value['count'];

        }
        return $response_data;
    }

    /**
     * The function divide by days for charts
     * @param $datas
     * @return $response_data
     */
    public static function getLabelsAndCountByDays($datas){
        $arr_date = [];
        $it = 0;
        foreach ($datas as $data)
        {
            $date_and_year = $data['created_at'];
            $month = date("m", strtotime($date_and_year));
            $year = date("Y", strtotime($date_and_year));
            $day= date("d", strtotime($date_and_year));
            if (!empty($arr_date))
            {
                $g = 0;
                for ($i = 0; $i < count($arr_date); $i++)
                {
                    if((!empty($arr_date[$i]['month'])) && ((int)$arr_date[$i]['month'] == $month)
                        && (!empty($arr_date[$i]['year']))
                        &&((int)$arr_date[$i]['year'] == $year)
                        &&(!empty($arr_date[$i]['day']))
                        &&((int)$arr_date[$i]['day'] == $day))
                    {
                        $arr_date[$i]['count'] = $arr_date[$i]['count'] + 1;
                        $g++;
                    }
                }
                if ($g == 0)
                {
                    $it++;
                    $arr_date[$it]['month'] = $month;
                    $arr_date[$it]['year'] = $year;
                    $arr_date[$it]['day'] = $day;
                    $arr_date[$it]['count'] = 1;
                }
            }else{
                $arr_date[0]['month'] = $month;
                $arr_date[0]['year'] = $year;
                $arr_date[0]['day'] = $day;
                $arr_date[0]['count'] = 1;
            }
        }
        $response_data = [];
        foreach($arr_date as $key => $value)
        {
            $month = DateTime::createFromFormat('!m', $value['month']);
            $monthFullName = $month->format('F');
            $monthShortName = date('M', strtotime($monthFullName));
            $response_data['labels'][] = $monthShortName.'/'.$value['day'].'/'.$value['year'];
            $response_data['counts'][]  = $value['count'];
        }
        return $response_data;
    }
}