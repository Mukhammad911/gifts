<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Marks extends Model
{
    protected $table = 'product_marks';
    
    protected $fillable     =   [
        //'id',
        'products_id',
        'rate',
        'user_id'
    ];
    
    public function products()
    {
      return $this->belongsTo(Products::class);
    }

    public function user()
    {
      return $this->belongsTo(User::class);
    }
}
