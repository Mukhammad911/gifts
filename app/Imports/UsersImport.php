<?php

namespace App\Imports;


use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Categories;
use App\Products;

class UsersImport implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        //
    }

    public function model(array $row)
    {
        // if (!isset($row[0])) {
        //     return null;
        // }
    
        
        return new Products([
            'name'     => $row[0],
            'price'      => $row[1],
            'sub_categories_id' => $row[2],
            'old_price' => $row[3],
            'brand' => $row[4],
            'discount' => $row[5],
            'onsale' => $row[6],
            'new' => $row[7],
            'bestseller' => $row[8],
            'available' => $row[9],
            'feature' => $row[10],
        ]);
        //dd($row[0]);
    }
}
