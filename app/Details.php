<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Details extends Model
{
    protected $table = 'product_details';

    protected $fillable     =   [
        //'id',
        'products_id',
        'name',
        'value'
    ];

    public function products()
    {
      return $this->belongsTo('App\Products', 'id');
    }

}
