<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $table = 'categories';


    protected $fillable     =   [
      //'id',
      'name'
  ];

    public function sub_categories()
    {
      return $this->hasMany(Sub_categories::class, 'categories_id', 'id');
    }

    public function products()
    {
      return $this->hasMany(Products::class, 'sub_categories_id', 'id');
    }
}
