<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Descriptions extends Model
{
    protected $table = 'product_descriptions';

    protected $fillable     =   [
        //'id',
        'products_id',
        'name'
    ];
    
    public function products()
    {
      return $this->belongsTo(Products::class);
    }

}
