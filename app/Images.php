<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Images extends Model
{
    protected $table = 'images';
    
    protected $fillable     =   [
        //'id',
        'products_id',
        'name',
        'img'
    ];
    
    public function products()
    {
      return $this->belongsTo(Products::class);
    }
}
