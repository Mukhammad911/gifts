<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Template_details_items extends Model
{
    protected $fillable = [
        'key','value','template_details_id'
    ];
}
